﻿using Sirenix.Serialization;

namespace Framework.Data
{
    using UnityEngine;
    using Sirenix.OdinInspector;

    [DataInfo(DataInfoGroup.Audio)]
    public class AudioClipData : Data
    {
        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public AudioClip Clip { get; private set; }

        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public bool Looped { get; private set; } = false;
        
        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public bool UseAttackPoint { get; private set; } = false;

        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public bool UseReleasePoint { get; private set; } = false;

        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public float Pitch { get; private set; } = 1f;
        
        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public float Volume { get; private set; } = 1f;
        
        [OdinSerialize, FoldoutGroup(nameof(AudioClipData))]
        public bool PlayOneShot { get; private set; } = false;
    }
}
