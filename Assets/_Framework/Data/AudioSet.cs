﻿using Sirenix.Serialization;

namespace Framework.Data
{
    using UnityEngine;
    using Sirenix.OdinInspector;
    using Framework;
    using System;

    [DataInfo(DataInfoGroup.Audio)]
    public class AudioSet : Data
    {
        [OdinSerialize, FoldoutGroup(nameof(AudioSet)), HideReferenceObjectPicker]
        public SerializedReadOnlyListInlineEditor<AudioClipData> Set { get; private set; } = new SerializedReadOnlyListInlineEditor<AudioClipData>();

        public AudioClipData Random() => Set.GetInternalList().Random();
    }
}
