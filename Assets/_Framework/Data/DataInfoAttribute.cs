﻿namespace Framework.Data
{
    using System;

    public enum DataInfoGroup
    {
        Audio,
        Graphics,
        Gameplay,
        UI,
        Animation,
        FX,
        Editor
    }

    public class DataInfoAttribute : Attribute
    {
        public DataInfoGroup Group { get; private set; }

        public DataInfoAttribute(DataInfoGroup group)
        {
            this.Group = group;
        }
    }

    public class DefInfoAttribute : Attribute
    {
        public string Group { get; private set; }

        public DefInfoAttribute(string group)
        {
            this.Group = group;
        }
    }
}


namespace Framework.Internal
{
    using System;

    /// <summary>
    /// Apply to a single static class with const strings
    /// </summary>
    public class DefInfoGroupSource : Attribute
    {
    }
}