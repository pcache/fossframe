using Sirenix.Serialization;
namespace Framework.Data
{
    using Sirenix.OdinInspector;

    public enum DefUsage
    {
        Internal = 0,
        Normal = 1,
    }

    public abstract class Def : FrameworkScriptable
    {
        [OdinSerialize, FoldoutGroup("Def")]
        public DefUsage Usage { get; private set; } = DefUsage.Internal;

        [OdinSerialize, FoldoutGroup("Def")]
        public string NiceName { get; private set; }

        [OdinSerialize, FoldoutGroup("Def"), MultiLineProperty(5)]
        public string Description { get; private set; }

        [OdinSerialize, FoldoutGroup("Def"), MultiLineProperty(5)]
        public string DescriptionFluff { get; private set; }

    }
}