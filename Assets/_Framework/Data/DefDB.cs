namespace Framework.Internal
{
    using Framework.Data;
    using System;
    using System.Collections.Generic;

    public interface IDefDBInternal
    {
        void SetScriptables(List<FrameworkScriptable> list);
        void SetGuidScriptableMap(Dictionary<Guid, FrameworkScriptable> map);
        void CleanupNulls();
        bool Contains(FrameworkScriptable cs);
    }
}

namespace Framework
{
    using Framework.Data;
    using Sirenix.OdinInspector;
    using Sirenix.Serialization;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Framework.Internal;
#if UNITY_EDITOR
    using UnityEditor; 

    [InitializeOnLoad]
#endif
    [CreateAssetMenu(fileName = "DefDB", menuName = "Framework/Internal/DefDB")]
    public class DefDB : SerializedScriptableObject, IDefDBInternal 
    {
        public static DefDB Instance { get; private set; }

#if UNITY_EDITOR
        static DefDB()
        {
            EditorApplication.delayCall += () =>
            {
                GetAsset().RuntimeInitialize();
            };
        }

        public static DefDB GetAsset()
        {
            return UnityExt.FindScriptableEditor<DefDB>();
        }
#endif

        [OdinSerialize, ReadOnly]
        public SerializedReadOnlyList<FrameworkScriptable> Scriptables { get; private set; } = new SerializedReadOnlyList<FrameworkScriptable>();
        public ReadOnlyDictionary<Type, ReadOnlyList<FrameworkScriptable>> ScriptablesByType { get; private set; }

        public ReadOnlyList<Def> Defs { get; private set; }
        public ReadOnlyDictionary<string, Def> DefsByName { get; private set; }
        public ReadOnlyDictionary<Type, ReadOnlyDictionary<string, Def>> DefsByType  { get; private set; }

        public ReadOnlyDictionary<Guid, FrameworkScriptable> GuidScriptableMap { get; private set; } = new ReadOnlyDictionary<Guid, FrameworkScriptable>();

        public static bool initialized { get; private set; }
        private static List<Def> validFilteredBuffer = new List<Def>();

        [Button("TestRuntime")]
        public void RuntimeInitialize()
        {
            MapIDs();

            List<Def> defs = new List<Def>();

            Dictionary<Type, ReadOnlyList<FrameworkScriptable>> scriptablesByType = new Dictionary<Type, ReadOnlyList<FrameworkScriptable>>();

            foreach (var item in Scriptables)
            {
                if (item.GetType().IsSubclassOf(typeof(Def)))
                {
                    defs.Add(item as Def);
                }

                if(!scriptablesByType.TryGetValue(item.GetType(), out var list))
                {
                    list = new ReadOnlyList<FrameworkScriptable>(new List<FrameworkScriptable>());
                    scriptablesByType.Add(item.GetType(),list);
                }

                ((IReadOnlyListInternal<FrameworkScriptable>)list).InternalList.Add(item);
            }

            ScriptablesByType = new ReadOnlyDictionary<Type, ReadOnlyList<FrameworkScriptable>>(scriptablesByType);

            Defs = new ReadOnlyList<Def>(defs);
            var defsByName = new Dictionary<string, Def>();
            var defsByType = new Dictionary<Type, ReadOnlyDictionary<string, Def>>();

            foreach (var item in Defs)
            {
                defsByName.Add(item.name, item);
                var type = item.GetType();
                
                if (!defsByType.TryGetValue(type, out var dict))
                {
                    dict = new ReadOnlyDictionary<string, Def>(new Dictionary<string, Def>());
                    defsByType.Add(type, dict);
                }

                ((IReadOnlyDictionaryInternal<string, Def>)dict).InternalDictionary.Add(item.name, item);
            }

            DefsByName = new ReadOnlyDictionary<string, Def>(defsByName);
            DefsByType = new ReadOnlyDictionary<Type, ReadOnlyDictionary<string, Def>>(defsByType);

            initialized = true;
            Instance = this;
        }

        public T GetFirstDef<T>(Func<T, bool> predicate = null) where T : Def
        {
            if (!initialized) return null;

            T val = default;

            if (DefsByType.TryGetValue(typeof(T), out var dict))
            {
                foreach (var pair in dict)
                {
                    if (pair.Value != null)
                    {
                        if (predicate != null)
                        {
                            if (predicate(pair.Value as T))
                            {
                                val = pair.Value as T;
                                break;
                            }
                        }
                        else
                            val = pair.Value as T;
                    }
                }
            }

            return val;
        }

        public T GetFirst<T>(Func<T, bool> predicate = null) where T : FrameworkScriptable
        {
            if (!initialized) return null;

            T val = default;

            if (ScriptablesByType.TryGetValue(typeof(T), out var dict))
            {
                foreach (var scriptable in dict)
                {
                    if (scriptable != null)
                    {
                        if (predicate != null)
                        {
                            if (predicate(scriptable as T))
                            {
                                val = scriptable as T;
                                break;
                            }
                        }
                        else
                            val = scriptable as T;
                    }
                }
            }

            return val;
        }

        public List<T> GetAll<T>(Func<T, bool> predicate = null) where T : FrameworkScriptable
        {
            if (!initialized) return null;

            if (DefsByType.TryGetValue(typeof(T), out var dict))
            {
                List<T> list = new List<T>();
                foreach (var item in dict.Values)
                {
                    if(predicate != null )
                    {
                        if(predicate(item as T))
                            list.Add(item as T);
                    }
                    else
                    {
                        list.Add(item as T);
                    }
                }
                return list;
            }

            return null;
        }

        public Def GetRandomDef<T>(Func<T, bool> predicate = null) where T : Def
        {
            if (!initialized) return null;

            validFilteredBuffer.Clear();
            var dict = DefsByType[typeof(T)];

            if (predicate != null)
            {
                foreach (var item in dict.Values)
                {
                    if (predicate(item as T))
                    {
                        validFilteredBuffer.Add(item);
                    }
                }

                return validFilteredBuffer[UnityEngine.Random.Range(0, validFilteredBuffer.Count)];
            }
            else
            {
                var count = validFilteredBuffer.Count;
                var index = UnityEngine.Random.Range(0, count);
                var i = 0;
                foreach (var item in dict)
                {
                    if (i == index)
                        return item.Value;
                    i++;
                }
            }
            return null;
        }

        public List<T> GetRandomDefCount<T>(int count) where T : Def
        {
            if (!initialized) return null;

            List<T> list = new List<T>(count);

            while (list.Count < count)
            {
                var d = GetRandomDef<T>();
                if (list.Contains(d))
                    continue;

                list.Add(d as T);
            }

            return list;
        }

        private void MapIDs()
        {
            Dictionary<Guid, FrameworkScriptable> map = new Dictionary<Guid, FrameworkScriptable>();
            foreach (var s in Scriptables)
            {
                if(!s)
                {
                    FLog.LogError(FLog.CategoryArrow(nameof(DefDB)), "CRITICAL:  SCRIPTABLE IS NULL!", FLogCategory.Framework, s);
                }

                if (s.GUID == Guid.Empty)
                {
                    FLog.LogError(FLog.CategoryArrow(nameof(DefDB)), "CRITICAL:  NO GUID ON SCRIPTABLE!", FLogCategory.Framework, s);
                }

                //CLog.Log("Mapping FS: " + s.name);
                if (map.ContainsKey(s.GUID))
                {
                    FLog.LogError(nameof(DefDB), "CRITICAL:  A Scriptable a duplicate ID found.", FLogCategory.Editor, s);
                    continue;
                }
                map.Add(s.GUID, s);
            }
            GuidScriptableMap = new ReadOnlyDictionary<Guid, FrameworkScriptable>(map);
        }

        void IDefDBInternal.SetScriptables(List<FrameworkScriptable> list)
        {
            List<FrameworkScriptable> scriptables = new List<FrameworkScriptable>();
            for (int i = 0; i < list.Count; i++)
            {
            }

            Scriptables = new SerializedReadOnlyList<FrameworkScriptable>(list);
        }

        void IDefDBInternal.SetGuidScriptableMap(Dictionary<Guid, FrameworkScriptable> map)
        {
            GuidScriptableMap = new ReadOnlyDictionary<Guid, FrameworkScriptable>(map);
        }

        void IDefDBInternal.CleanupNulls()
        {
            var list = Scriptables.GetInternalList();
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] == null)
                    list.RemoveAt(i);
            }
            Scriptables = new SerializedReadOnlyList<FrameworkScriptable>(list);
        }

        bool IDefDBInternal.Contains(FrameworkScriptable cs)
        {
            return Scriptables.Contains(cs);
        }

    }
}
