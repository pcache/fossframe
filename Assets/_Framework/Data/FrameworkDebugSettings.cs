﻿namespace Framework.Internal
{
    using Framework.Data;
    using Sirenix.Serialization;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = nameof(FrameworkDebugSettings), menuName = "Framework/" + nameof(FrameworkDebugSettings))]
    public class FrameworkDebugSettings : FrameworkScriptable
    {
        private static FrameworkDebugSettings instance;

        public static FrameworkDebugSettings Instance
        {
            get
            {
                if(!instance)
                {
                    instance =  Resources.Load("Framework/" + nameof(FrameworkDebugSettings), typeof(FrameworkDebugSettings)) as FrameworkDebugSettings;
                }
                return instance;
            }
        }

        [OdinSerialize] public bool DebugSystemInit { get; set; }
        [OdinSerialize] public bool DebugSystemLoops { get; set; }
        [OdinSerialize] public bool DebugUI { get; set; }
    }

}