﻿using Sirenix.Serialization;

namespace Framework.Data
{
    using System;
    using Sirenix.OdinInspector;

    public interface IFrameworkScriptableEditorInternal
    {
        void GenerateNewGUID();
    }

    public abstract class FrameworkScriptable : SerializedScriptableObject , IFrameworkScriptableEditorInternal
    {
        public static FrameworkScriptable scriptableToScan;

        [OdinSerialize, ReadOnly]
        public Guid GUID { get; private set; }

        void IFrameworkScriptableEditorInternal.GenerateNewGUID()
        {
            GUID = Guid.NewGuid();
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this); 
#endif
        }

#if UNITY_EDITOR
        [UnityEngine.ContextMenu("Gererate a new GUID")]
        private void NewID()
        {
            if(UnityEditor.EditorUtility.DisplayDialog("Gererate a new GUID", "This will erase current GUID and generate a new one. This can potentially lead to disasterous results. Please notify programmers if you do this.", "I Consent To Donating My Kidneys", "I want to live." ))
            {
                ((IFrameworkScriptableEditorInternal)this).GenerateNewGUID();
            }
        }
#endif
    }
}
