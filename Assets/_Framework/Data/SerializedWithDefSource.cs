﻿using Framework.Data;
using System;

namespace Framework
{
    public interface 
        ISerializedDataWithOptionalDefSource<TData, TDef>
        : IRuntimeInstanceContainer<TData>

        where TDef : Def, IDefDataSource<TData>
    {
        bool LoadFromDef { get; set; }
        TDef Def { get; }
    }

    public interface IDefDataSource<TData>
    {
        TData GetDataRuntimeInstance();
    }

/*    public class SerializedWithDefSource<TData, TDef> : ISerializedDataWithOptionalDefSource<TData, TDef>
        where TData : IRuntimeInstanceContainer<TData>
        where TDef : Def, IDefDataSource<TData>
    {

    }*/
}
