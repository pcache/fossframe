﻿namespace Framework.Editor
{
    using UnityEditor;
    using System.Collections.Generic;
    using UnityEngine;

    internal static class AppEditorUtils
    {
        [InitializeOnLoadMethod]
        private static void EditorInit()
        {
            AppLoader.DevMode = EditorConsts.KEY_DEVMODE.GetEditorPrefsBool();
        }

        [MenuItem("Framework/Danger/ReserializeData")]
        public static void ReserializeData()
        {
            Reserialize(AssetDatabase.FindAssets("t:Def"));
            Reserialize(AssetDatabase.FindAssets("t:Data"));
        }

        private static void Reserialize(string[] guids )
        {
            List<string> paths = new List<string>();
            foreach (var g in guids)
            {
                paths.Add(AssetDatabase.GUIDToAssetPath(g));
            }

            AssetDatabase.ForceReserializeAssets(paths, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        }

        [MenuItem("Framework/ToggleDevMode")]
        public static void ToggleDevMode()
        {
            EditorConsts.KEY_DEVMODE.SetEditorPrefsBool(!EditorConsts.KEY_DEVMODE.GetEditorPrefsBool());
            AppLoader.DevMode = EditorConsts.KEY_DEVMODE.GetEditorPrefsBool();
            FLog.Log("DevMode is: ", AppLoader.DevMode.ToString(), FLogCategory.Editor);
        }
    }
}
