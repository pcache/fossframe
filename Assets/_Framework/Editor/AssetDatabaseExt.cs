﻿namespace Framework.Editor
{
    using Framework;
    using System;
    using System.IO;
    using UnityEditor;

    public static class AssetDatabaseExt
    {
        public static T FindObject<T>() where T : UnityEngine.Object
        {
            var guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
            if(guids.Length > 0)
            {
                var path = AssetDatabase.GUIDToAssetPath(guids[0]);
                if(!string.IsNullOrEmpty(path))
                {
                    return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
                }
            }
            return null;
        }

        public static string GetAssetNameWithExt(this UnityEngine.Object obj)
        {
            var path = AssetDatabase.GetAssetPath(obj);
            var name = Path.GetFileName(path);
            return name;
        }

        public static string GetAssetPath(this UnityEngine.Object obj)
        {
            return AssetDatabase.GetAssetPath(obj);
        }

        public static void SplitIntoPathNameExt(this string source, out string path, out string name, out string ext)
        {
            ext = Path.GetExtension(source);
            path = source.GetParentFolder();
            name = Path.GetFileNameWithoutExtension(source);
        }

        public static string CombineIntoPathNameExt(string dir, string name, string ext)
        {
            return dir + name + ext;
        }

        public static string GetParentFolder(this string source)
        {
            return Directory.GetParent(source).ToString().ToUnityPath();
        }

        public static void MoveAsset(this UnityEngine.Object obj, string newDirectory)
        {
            AssetDatabase.MoveAsset(obj.GetAssetPath(), NewAssetPathInDirectory(newDirectory, obj.GetAssetNameWithExt()));
        }

        public static UnityEngine.Object CloneAsset(this UnityEngine.Object obj, Type type)
        {
            var path = obj.GetAssetPath();
            path.SplitIntoPathNameExt(out string dir, out string name, out string ext);
            var newPath = CombineIntoPathNameExt(dir, name + "_clone", ext);

            if (AssetDatabase.CopyAsset(path, newPath))
            {
                return AssetDatabase.LoadAssetAtPath(newPath, type);
            }

            return null;
        }

        public static string NewAssetPathInDirectory(string directory, string assetName)
        {
            directory = EnsureDirectoryTerminator(directory);
            return directory + assetName;
        }

        public static string EnsureDirectoryTerminator(string directory)
        {
            if (directory[directory.Length - 1] != '/')
                directory += '/';

            return directory;
        }
    }
}
