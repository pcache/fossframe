﻿namespace Framework.Editor
{
    using Framework;
    using Framework.Data;
    using System;
    using UnityEditor;

    public static class CopyDataGuid
    {
        [MenuItem("Assets/Copy Data GUID")]
        public static void CopyGUID()
        {
            if(Selection.activeObject)
            {
                FrameworkScriptable fs = Selection.activeObject as FrameworkScriptable;
                if(fs)
                {
                    EditorGUIUtility.systemCopyBuffer = fs.GUID.ToString();
                }
            }
        }
    }
}
