using Framework;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class DataTransferUtil : EditorWindow
{
    [MenuItem("Framework/Utilities/DataTransfer")]
    public static void Create()
    {
        // Show the selection window.
        var window = EditorWindow.GetWindow<DataTransferUtil>(true);
        window.ShowPopup();
    }

    private UnityEngine.Object from, to;


    private void OnGUI()
    {
        GUILayout.Label("Transfer From");
        from = EditorGUILayout.ObjectField(from, typeof(UnityEngine.Object), false);
        GUILayout.Label("Transfer To");
        to = EditorGUILayout.ObjectField(to, typeof(UnityEngine.Object), false);

        if(from != null && to != null)
        {
            if (GUILayout.Button("Transfer"))
            {
                if (UnityEditor.EditorUtility.DisplayDialog("Transfer data",
                    "This will copy all the matching data from one UnityEngine.Object to another.",
                    "Yes do it", "Nope"))
                {
                    Undo.RecordObject(to, "Data Transfer to");

                    ReflectionUtil.ObjectInfo fromInfo = new ReflectionUtil.ObjectInfo(from.GetType(), true);
                    ReflectionUtil.ObjectInfo toInfo = new ReflectionUtil.ObjectInfo(to.GetType(), true);

                    foreach (var prop in fromInfo.Properties)
                    {
                        if(toInfo.PropertiesMap.TryGetValue(prop.Name, out var toprop))
                        {
                            toprop.SetValue(to, prop.GetValue(from));
                        }
                    }

                    foreach (var field in fromInfo.Fields)
                    {
                        if(toInfo.FieldsMap.TryGetValue(field.Name, out var tofield))
                        {
                            tofield.SetValue(to, field.GetValue(from));
                        }
                    }

                    EditorUtility.SetDirty(to);
                }
            }
        }
    }
}
