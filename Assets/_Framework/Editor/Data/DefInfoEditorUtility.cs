﻿namespace Framework.Editor
{
    using UnityEditor;
    using UnityEngine;
    using Framework;
    using Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Framework.Internal;

    public static class DefInfoEditorUtility
    {
        public static string[] DefGroups { get; private set; }

        [InitializeOnLoadMethod]
        static void onLoad()
        {
            ReflectedTypeStore.TryInit();

            var types = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user);
            var source = ReflectionUtil.GetFirstTypeWithAttribute(types, typeof(DefInfoGroupSource), true);
            if (source == null)
            {
                FLog.LogError(FLog.CategoryArrow("DEF INFO"), " You see this message because you dont have a DefInfoGroup class. To fix this" +
                    " you have to create a new static class and slap a DefInfoGroupSource attribute on it. Then you can declare static strings in it" +
                    " and use those strings in conjunction with DefInfo attribute on Defs.");
                return;
            }

            var fields = source.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            List<string> list = new List<string>();
            foreach (var f in fields)
            {
                list.Add(f.Name);
            }

            DefGroups = list.ToArray();
        }
    }
}
