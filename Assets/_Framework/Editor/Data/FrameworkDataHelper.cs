﻿namespace Framework.Editor
{
#if UNITY_EDITOR
    using UnityEditor;
#endif

    using System;
    using UnityEngine;
    using System.Reflection;
    using System.Linq;
    using System.IO;
    using Framework;
    using Framework.Data;
    using System.Collections.Generic;

    internal class FrameworkDataHelper : EditorWindow
    {
        private Type[] Defs;
        private Type[] Data;
        private FrameworkDataHelperColors colors;

        private List<DirectoryTemplate> directoryTemplates = new List<DirectoryTemplate>();

        private class DirectoryTemplate
        {
            public string Name;
            public string[] directories;

            public DirectoryTemplate(string Name, params string[] directories)
            {
                this.Name = Name;
                this.directories = directories;
            }
        }

        [MenuItem("Framework/DataHelper")]
        static void Init()
        {
            if (EditorWindow.HasOpenInstances<FrameworkDataHelper>())
                return;

            // Get existing open window or if none, make a new one:
            FrameworkDataHelper window = (FrameworkDataHelper)EditorWindow.GetWindow(typeof(FrameworkDataHelper));
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 620, 900);
            window.Show();

            window.colors = UnityExt.FindScriptableEditor<FrameworkDataHelperColors>();
            window.colors.Init();
        }

        private void OnEnable()
        {
            FrameworkDataHelper window = (FrameworkDataHelper)EditorWindow.GetWindow(typeof(FrameworkDataHelper));
            window.Defs = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user, typeof(Def), true).ToArray();
            window.Data = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user, typeof(Data), true).OrderBy(x => x.Name).ToArray();

            window.Defs = window.Defs.OrderBy(x =>
            {
                var att = x.GetCustomAttribute(typeof(DefInfoAttribute)) as DefInfoAttribute;
                if (att != null)
                    return att.Group;
                return default;
            }).ThenBy(x => x.Name).ToArray();

            window.Data = window.Data.OrderBy(x =>
            {
                var att = x.GetCustomAttribute(typeof(DataInfoAttribute)) as DataInfoAttribute;
                if (att != null)
                    return att.Group;
                return default;
            }).ThenBy(x => x.Name).ToArray();

            directoryTemplates.Add(new DirectoryTemplate("All Def Assets", 
                "art", "animations", "audio", "prefabs", "fx", "materials", "cinematics"));

            directoryTemplates.Add(new DirectoryTemplate("Art", "art"));
            directoryTemplates.Add(new DirectoryTemplate("Prefabs", "prefabs"));
            directoryTemplates.Add(new DirectoryTemplate("Audio", "audio"));
            directoryTemplates.Add(new DirectoryTemplate("Animations", "animations"));
            directoryTemplates.Add(new DirectoryTemplate("FX", "fx"));
            directoryTemplates.Add(new DirectoryTemplate("Materials", "materials"));
            directoryTemplates.Add(new DirectoryTemplate("Cinematics", "cinematics"));


        }

        private void Update()
        {
            Repaint();
        }

        void OnGUI()
        {
            var e = Event.current;
            var selection = Selection.activeObject;

            if(!selection)
            {
                GUI.color = Color.red;
                GUILayout.Label("Select an asset, and its folder will be used to create new assets.");
                GUI.color = Color.white;
                return;
            }

            string path = AssetDatabase.GetAssetPath(selection);
            if(string.IsNullOrEmpty(path))
            {
                FLog.LogError(FLog.CategoryArrow("DataHelper"), "Not a valid asset or folder.");
                Close();
                return;
            }

            if (AssetDatabase.IsValidFolder(path))
                path = path.ToUnityPath();
            else
                path = path.GetParentFolder();
            
            GUILayout.Label(path);

            if (e.keyCode == KeyCode.Escape)
            {
                this.Close();
            }

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical();
                {

                    GUILayout.Label("-------DEFS-------");

                    string defGroup = null;

                    foreach (var item in Defs)
                    {
                        var attr = item.GetCustomAttribute(typeof(DefInfoAttribute)) as DefInfoAttribute;
                        if (attr != null && defGroup != attr.Group)
                        {
                            GUILayout.Label(attr.Group.ToString());
                            defGroup = attr.Group;
                        }

                        if (!string.IsNullOrEmpty(defGroup) && colors.DefInfo.TryGetValue(defGroup, out var info))
                            GUI.color = info.color;
                        else
                            GUI.color = Color.white;


                        if (GUILayout.Button(item.Name, GUILayout.Width(200f)))
                        {
                            CreateSO(item, path, item.Name, "Def");
                            Close();
                            return;
                        }

                        GUI.color = Color.white;
                    }

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("-------DATA-------");
                    DataInfoGroup dataGroup = (DataInfoGroup)int.MaxValue;

                    foreach (var item in Data)
                    {
                        var attr = item.GetCustomAttribute(typeof(DataInfoAttribute)) as DataInfoAttribute;
                        if (attr != null && dataGroup != attr.Group)
                        {
                            GUILayout.Label(attr.Group.ToString());
                            dataGroup = attr.Group;
                        }

                        if (colors.DataInfo.TryGetValue(dataGroup, out var info))
                            GUI.color = info.color;
                        else
                            GUI.color = Color.white;

                        if (GUILayout.Button(item.Name, GUILayout.Width(200f)))
                        {
                            CreateSO(item, path, item.Name, "Data");
                            Close();
                            return;
                        }

                        GUI.color = Color.white;

                    }

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("-------FOLDERS-------");

                    foreach (var item in directoryTemplates)
                    {
                        if (GUILayout.Button(item.Name, GUILayout.Width(200f)))
                        {
                            CreateDirectoryTemplate(path, item);
                            Close();
                            return;
                        }
                    }

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.EndHorizontal();
            }
        }

        private void CreateDirectoryTemplate(string path, DirectoryTemplate template)
        {
            if(template.directories.Length > 1)
            {
                foreach (var dir in template.directories)
                {
                    var dirPath = path + "/" + dir;
                    if (!AssetDatabase.IsValidFolder(dirPath))
                        AssetDatabase.CreateFolder(path, dir);
                }
            }
            else
            {
                var dir = template.directories[0];
                var dirPath = path + "/" + dir;
                if (!AssetDatabase.IsValidFolder(dirPath))
                    AssetDatabase.CreateFolder(path, dir);

                foreach (var item in Selection.objects)
                {
                    if (item.IsFolder())
                        continue;

                    item.MoveAsset(dirPath);
                }
            }
        }

        private void CreateSO(Type type, string path, string name, string prefix)
        {
            var so = ScriptableObject.CreateInstance(type);
            ProjectWindowUtil.CreateAsset(so, path + "/" + name + ".asset");
            ProjectWindowUtil.ShowCreatedAsset(so);
            //AssetDatabase.CreateAsset(so, );
            //Selection.activeObject = so;
        }
    }
}
