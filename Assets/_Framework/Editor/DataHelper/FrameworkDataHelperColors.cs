﻿using Sirenix.Serialization;

namespace Framework.Editor
{
    using Framework;
    using Framework.Data;
    using Sirenix.OdinInspector;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class FrameworkDataHelperColors : SerializedScriptableObject
    {
        [Serializable]
        public class DefGroupData
        {
            [ValueDropdown("$GetValues")]
            public string group;
            public Color color = Color.white;

            private string[] GetValues => DefInfoEditorUtility.DefGroups;
        }

        [SerializeField]
        private List<DefGroupData> DefData = new List<DefGroupData>();

        [Serializable]
        public class DataGroupData
        {
            public DataInfoGroup group;
            public Color color = Color.white;
        }

        [SerializeField]
        private List<DataGroupData> DataData = new List<DataGroupData>();

        private Dictionary<DataInfoGroup, DataGroupData> dataMap = new Dictionary<DataInfoGroup, DataGroupData>();
        private Dictionary<string, DefGroupData> defMap = new Dictionary<string, DefGroupData>();

        public ReadOnlyDictionary<DataInfoGroup, DataGroupData> DataInfo { get; private set; }
        public ReadOnlyDictionary<string, DefGroupData> DefInfo { get; private set; }

        public void Init()
        {
            defMap.Clear();
            dataMap.Clear();

            foreach (var item in DefData)
            {
                defMap.Add(item.group, item);
            }

            foreach (var item in DataData)
            {
                dataMap.Add(item.group, item);
            }

            DataInfo = new ReadOnlyDictionary<DataInfoGroup, DataGroupData>(dataMap);
            DefInfo = new ReadOnlyDictionary<string, DefGroupData>(defMap);
        }
    }
}
