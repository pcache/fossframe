﻿namespace Framework.Editor.Extensions
{

    using System.Collections.Generic;
    using UnityEditor;

    internal static class CollapseProject
    {

        [MenuItem("Edit/Collapse Project ")] // shortcut key F5 to Play (and exit playmode also)
        static void collapseProject()
        {
            string assemblypath = UnityEditorInternal.InternalEditorUtility.GetEditorAssemblyPath();
            var assembly = System.Reflection.Assembly.LoadFrom(assemblypath);
            var type = assembly.GetType("UnityEditor.ProjectBrowser");
            var projectbrowserfield = type.GetField("s_LastInteractedProjectBrowser");
            var projectbrowser = projectbrowserfield.GetValue(type);
            var treestatefield = type.GetField("m_AssetTreeState", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var treestate = treestatefield.GetValue(projectbrowser);
            var expandedIdsField = treestate.GetType().GetProperty("expandedIDs", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);

            var assetsFolder = AssetDatabase.LoadAssetAtPath("Assets", typeof(UnityEngine.Object));
            UnityEditorInternal.InternalEditorUtility.expandedProjectWindowItems = new int[1] { assetsFolder.GetInstanceID() };
            expandedIdsField.SetValue(treestate, new List<int>() { assetsFolder.GetInstanceID() }, null);
            type.InvokeMember("OnProjectChanged", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, projectbrowser, null);
        }

    }
}