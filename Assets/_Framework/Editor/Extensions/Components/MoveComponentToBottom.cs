﻿namespace Framework.Editor.Extensions
{

    using UnityEngine;
    using UnityEditor;

    internal static class MoveComponentToBottom
    {
        [MenuItem("CONTEXT/Component/Move to bottom")]
        private static void MoveToBottom(MenuCommand command)
        {
            for (int i = 0; i < 100; i++)
            {
                UnityEditorInternal.ComponentUtility.MoveComponentDown(command.context as Component);
            }
        }
    }
}