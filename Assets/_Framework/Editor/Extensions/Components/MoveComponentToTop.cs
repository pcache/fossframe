﻿namespace Framework.Editor.Extensions
{
    using UnityEngine;
    using UnityEditor;

    internal static class MoveComponentToTop
    {
        [MenuItem("CONTEXT/Component/Move to top")]
        static void MoveToTop(MenuCommand command)
        {
            for (int i = 0; i < 100; i++)
            {
                UnityEditorInternal.ComponentUtility.MoveComponentUp(command.context as Component);
            }
        }
    }
}