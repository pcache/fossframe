﻿
namespace Framework.Editor
{
    using Framework;
    using System;
    using System.IO;
    using UnityEditor;

    public static class CopyPasteAssets
    {
        private static string copiedAssetPath;
        private static Type copiedAssetType;

        [MenuItem("Assets/Copy Asset")] 
        public static void CopyAsset()
        {
            if (Selection.objects.Length > 1)
                return;

            if (Selection.activeObject.IsFolder())
                return;

            if(Selection.activeObject)
            {
                copiedAssetPath = AssetDatabase.GetAssetPath(Selection.activeObject);
                copiedAssetType = Selection.activeObject.GetType();
            }
        }

        [MenuItem("Assets/Paste Asset")]
        public static void PasteAsset()
        {
            if (Selection.objects.Length == 0 || Selection.objects.Length > 1)
                return;

            string path = AssetDatabase.GetAssetPath(Selection.objects[0]);

            if (AssetDatabase.IsValidFolder(path))
                path = path.ToUnityPath();
            else
                path = Directory.GetParent(path).ToString().ToUnityPath();

            if (string.IsNullOrEmpty(copiedAssetPath))
            {
                return;

            }

            var asset = AssetDatabase.LoadAssetAtPath(copiedAssetPath, copiedAssetType);

            if (!asset)
                return;

            var clone = asset.CloneAsset(copiedAssetType);

            if (!clone)
                return;

            clone.MoveAsset(path);
        }
    }
}
