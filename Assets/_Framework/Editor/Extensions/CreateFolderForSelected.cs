﻿namespace Framework.Editor.Extensions
{

    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using System.IO;

    internal static class CreateFolderForSelected
    {
        static HashSet<Type> ignoreTypes = new HashSet<Type>()
        {
            typeof(UnityEditor.DefaultAsset)
        };

        [MenuItem("Assets/Make folder for")]
        static void createFolderForSelected()
        {
            foreach (var item in Selection.objects)
            {
                TryCreateFolderFor(item);
            } 
        }

        private static void TryCreateFolderFor(UnityEngine.Object obj)
        {
            if (!AssetDatabase.IsMainAsset(obj))
                return;

            if (ignoreTypes.Contains(obj.GetType()))
                return;

            string path = AssetDatabase.GetAssetPath(obj);
            string ext = Path.GetExtension(path);
            string parentDir = path.Remove(path.LastIndexOf('/'));
            string newpath = path.Remove(path.LastIndexOf('.')) + "/" + obj.name + ext;

            AssetDatabase.CreateFolder(parentDir, obj.name);

            if (AssetDatabase.ValidateMoveAsset(path, newpath) == string.Empty)
            {
                AssetDatabase.MoveAsset(path, newpath);
                Debug.Log("Made folder for and moved asset:" + newpath);
            }
        }

    }
}