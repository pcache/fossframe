﻿namespace Framework.Editor.Extensions
{

    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using System.IO;

    public static class CreateMaterialFrom
    {
        public static event Action<Material> OnMaterialCreated;

        static HashSet<Type> ignoreTypes = new HashSet<Type>()
        {
            typeof(UnityEditor.DefaultAsset)
        };

        [MenuItem("Assets/Create Material From")]
        static void createMaterialFrom()
        {

            var target = Selection.activeObject;
            if (target == null)
                return;

            if (!AssetDatabase.IsMainAsset(target))
                return;

            if (ignoreTypes.Contains(target.GetType()))
                return;

            string path = AssetDatabase.GetAssetPath(target);
            string ext = ".mat";
            string parentDir = Directory.GetParent(path).ToString();
            string newpath = parentDir + "/" + target.name + ext;

            Material material = new Material(Shader.Find("Standard"));

            AssetDatabase.CreateAsset(material, newpath);

            if (AssetDatabase.ValidateMoveAsset(path, newpath) == string.Empty)
            {
                AssetDatabase.MoveAsset(path, newpath);
                Debug.Log("Created material at: " + newpath);
            }

        }

    }
}