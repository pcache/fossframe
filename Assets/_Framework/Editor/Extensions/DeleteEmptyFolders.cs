﻿namespace Framework.Editor.Extensions
{
    using UnityEngine;
    using UnityEditor;

    internal static class DeleteEmptyFolders
    {
        [MenuItem("Assets/Delete Empty Folders")]
        static void deleteEmptyFolders()
        {
            foreach (var item in Selection.objects)
            {
                TryDeleteEmptyFolders(item);
            } 
        }

        private static void TryDeleteEmptyFolders(UnityEngine.Object obj)
        {
            if (!AssetDatabase.IsMainAsset(obj))
                return;

            // checks if the asset is a folder
            if (obj.GetType() != typeof(UnityEditor.DefaultAsset))
                return;

            string dir = AssetDatabase.GetAssetPath(obj);

            var subdirs = AssetDatabase.GetSubFolders(dir);

            string[] strBuffer = new string[1];

            foreach (var sub in subdirs)
            {
                strBuffer[0] = sub;
                var assets = AssetDatabase.FindAssets("*", strBuffer);

                if(assets.Length == 0)
                {
                    Debug.Log("Deleteing : " + sub);
                    AssetDatabase.MoveAssetToTrash(sub);
                }
            }
        }
    }
}