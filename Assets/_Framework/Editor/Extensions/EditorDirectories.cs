﻿namespace Framework.Editor.Extensions
{
    using UnityEngine;
    using UnityEditor;
    using System.IO;
    using System.Diagnostics;

    internal static class EditorDirectories
    {
        [MenuItem("ProjectSettings/Directories/Script templates", false, -4)]
        static void openScriptTemplateDir()
        {
            string path = Directory.GetParent(EditorApplication.applicationPath) + "/Data/Resources/ScriptTemplates/";
            //UnityEngine.Debug.Log(path);
            Process.Start(path);
        }

        [MenuItem("ProjectSettings/Directories/Editor root", false, -4)]
        static void openEditorRoot()
        {
            Process.Start(Directory.GetParent(EditorApplication.applicationPath).ToString());
        }

        [MenuItem("ProjectSettings/Directories/Persistent data", false, -4)]
        static void openPersistentData()
        {
            Process.Start(Application.persistentDataPath);
        }
    }
}