﻿namespace Framework.Editor.Extensions
{
    using UnityEngine;
    using UnityEditor;
    using System.Collections.Generic;

    internal class FindMissingScriptsEditor : EditorWindow
    {
        [MenuItem("Framework/Utilities/Find Missing Scripts")]
        public static void FindMissingScripts()
        {
            EditorWindow.GetWindow(typeof(FindMissingScriptsEditor));
        }

        static int missingCount = -1;

        List<GameObject> missing = new List<GameObject>();

        void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Missing Scripts:");
                EditorGUILayout.LabelField("" + (missingCount == -1 ? "---" : missingCount.ToString()));
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Find missing scripts"))
            {
                var go = Selection.activeGameObject;
                if (!go)
                {
                    Debug.LogWarning("Select a root GameObject to scan");
                    return;
                }

                missing = new List<GameObject>();
                FindMissingRecursive(go, missing);

            }

            for (int i = missing.Count - 1; i >= 0; i--)
            {
                var gameobject = missing[i];
                if (gameobject == null || !HasMissing(gameobject))
                {
                    missing.RemoveAt(i);
                    continue;
                }

                if (GUILayout.Button(gameobject.name))
                {
                    Selection.activeGameObject = gameobject;
                }
            }
        }

        private static List<Component> buffer = new List<Component>();

        private static void FindMissingRecursive(GameObject gameObject, List<GameObject> list)
        {
            if (HasMissing(gameObject))
                list.Add(gameObject);

            foreach (Transform child in gameObject.transform)
            {
                FindMissingRecursive(child.gameObject, list);
            }
        }

        private static bool HasMissing(GameObject gameObject)
        {
            gameObject.GetComponents<Component>(buffer);
            for (int i = 0; i < buffer.Count; i++)
            {
                if (buffer[i] == null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}