﻿namespace Framework.Editor.Extensions
{

    using UnityEngine;
    using UnityEditor;

    internal static class Unparent
    {
        [MenuItem("GameObject/Unparent", false, -4)]
        static void unparent(MenuCommand command)
        {
            var go = command.context as GameObject;
            Transform parent = go.transform.parent;
            if (parent != null)
            {
                Undo.SetTransformParent(go.transform, null, "Unparented");
            }
        }
    }
}