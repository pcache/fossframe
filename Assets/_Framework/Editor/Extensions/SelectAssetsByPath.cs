﻿
namespace Framework.Editor.Extensions
{
    using UnityEditor;
    using UnityEngine;
    using Unity.EditorCoroutines.Editor;
    using System.Collections;

    internal static class SelectAssetsByPath
    {
        private static object CoroutineOwner;
        private static EditorCoroutine coroutine;
        private static bool PrevFocus;

        [InitializeOnLoadMethod]
        public static void Initialize()
        {
            if (CoroutineOwner == null)
                CoroutineOwner = new object();

            if(coroutine == null)
                coroutine = EditorCoroutineUtility.StartCoroutine(WaitForFocus(), CoroutineOwner);
        }

        static IEnumerator WaitForFocus()
        {
            while(true)
            {
                bool focused = UnityEditorInternal.InternalEditorUtility.isApplicationActive;
                if (PrevFocus != focused)
                {
                    if(focused == true)
                    {
                        ParseClipboard();
                    }
                    PrevFocus = focused;
                }
                yield return null;
            }
        }

        private static void ParseClipboard()
        {
            string path = EditorGUIUtility.systemCopyBuffer;
            path = path.Replace("\\", "/");
            if(path.Contains("Assets/"))
            {
                if(path.StartsWith("Assets/"))
                {

                }
                else
                {
                    path = "Assets" + path.Replace(Application.dataPath, string.Empty);
                }

                var asset = AssetDatabase.LoadAllAssetsAtPath(path);
                if(asset != null && asset.Length > 0)
                {
                    Selection.activeObject = asset[0];
                    Debug.Log("Selected asset on focus: " + path);
                    EditorGUIUtility.systemCopyBuffer = null;
                }
            }
        }
    }
}
