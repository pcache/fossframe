﻿namespace Framework.Editor
{
    using UnityEditor;
    using UnityEngine;
    using UnityEditor.Build.Reporting;
    using System.IO;
    using uDebug = UnityEngine.Debug;
    using Ionic.Zip;
    using System.Diagnostics;

    // Output the build size or a failure depending on BuildPlayer.

    internal static class BuildScript
    {
        private const char BUILD_SEPARATOR_TOKEN = '_';
        private static string ProjectRoot => Directory.GetParent(Application.dataPath).ToString();

        private static BuildOptions bo_Release = BuildOptions.None;
        private static BuildOptions bo_Debug = BuildOptions.AllowDebugging | BuildOptions.Development;

        private static BuildTarget bt_win = BuildTarget.StandaloneWindows64;
        private static BuildTarget bt_mac = BuildTarget.StandaloneOSX ;
        private static BuildTarget bt_linux = BuildTarget.StandaloneLinux64;

        private static string debugBuildPrefix = "dbg_";
        private static string pathSteamContent = @"G:\Repos\crawler_demo\SteamSDK\tools\ContentBuilder\content";
        private static string pathSteamUploader = @"G:\Repos\crawler_demo\SteamSDK\tools\ContentBuilder\run_build.bat";

        private const string PLATFORM_WIN = "Win64";
        private const string PLATFORM_OSX = "OSX";
        private const string PLATFORM_LINUX = "Linux64";

        private static void ClearSteamContentDirectory(string platform)
        {
            var path = pathSteamContent + "/" + platform;
            if (Directory.Exists(path)) 
                Directory.Delete(path, true);
            Directory.CreateDirectory(path);
        }

        private static void CopyBuildToSteamOutputDir(string buildpath, string platform)
        {
            var steampath = pathSteamContent + "/" + platform;
            IOExt.CopyDirRecursive(buildpath, steampath);
        }

        [MenuItem("Framework/Danger/SteamUpload")]
        public static void LaunchSteamBuildUploader()
        {
            if(EditorUtility.DisplayDialog("SteamSDK Content Builder", "Are you sure you want to upload current output to steam?", "Да, блять", "Нет"))
            {
                Process.Start(pathSteamUploader);
            }
        }

        [MenuItem("Framework/Build/All/ForSteam")]
        public static void BuildForSteam()
        {
            // Clear content folders first
            ClearSteamContentDirectory(PLATFORM_LINUX);
            ClearSteamContentDirectory(PLATFORM_WIN);
            ClearSteamContentDirectory(PLATFORM_OSX);

            string windir = Build(bo_Release, bt_win, string.Empty, false);
            CopyBuildToSteamOutputDir(windir, PLATFORM_WIN);

            string linux = Build(bo_Release, bt_linux, string.Empty, false);
            CopyBuildToSteamOutputDir(linux, PLATFORM_LINUX);

            string osx = Build(bo_Release, bt_mac, string.Empty, false);
            CopyBuildToSteamOutputDir(osx, PLATFORM_OSX);
        }

        [MenuItem("Framework/Build/All/Release")]
        public static void Build_All()
        {
            Build_Windows();
            Build_Mac();
            Build_Linux();
        }

        [MenuItem("Framework/Build/All/ReleaseNoZip")]
        public static void Build_AllNoZip()
        {
            Build_WindowsNoZip();
            Build_MacNoZip();
            Build_LinuxNoZip();
        }

        [MenuItem("Framework/Build/All/Debug")]
        public static void Build_All_Debug()
        {
            Build_Windows_Debug();
            Build_Mac_Debug();
            Build_Linux_Debug();
        }

        [MenuItem("Framework/Build/All/DebugNoZip")]
        public static void Build_All_DebugNoZip()
        {
            Build_Windows_DebugNoZip();
            Build_Mac_DebugNoZip();
            Build_Linux_DebugNoZip();
        }


        [MenuItem("Framework/Build/Windows/Release")]
        public static void Build_Windows() => Build(bo_Release, bt_win, string.Empty);

        [MenuItem("Framework/Build/Windows/ReleaseNoZip")]
        public static void Build_WindowsNoZip() => Build(bo_Release, bt_win, string.Empty, false);

        [MenuItem("Framework/Build/Windows/Debug")]
        public static void Build_Windows_Debug() => Build(bo_Debug, bt_win, debugBuildPrefix);

        [MenuItem("Framework/Build/Windows/DebugNoZip")]
        public static void Build_Windows_DebugNoZip() => Build(bo_Debug, bt_win, debugBuildPrefix, false);

        [MenuItem("Framework/Build/Mac/Release")]
        public static void Build_Mac() => Build(bo_Release, bt_mac, string.Empty);

        [MenuItem("Framework/Build/Mac/ReleaseNoZip")]
        public static void Build_MacNoZip() => Build(bo_Release, bt_mac, string.Empty, false);

        [MenuItem("Framework/Build/Mac/Debug")]
        public static void Build_Mac_Debug() => Build(bo_Debug, bt_mac, debugBuildPrefix);

        [MenuItem("Framework/Build/Mac/DebugNoZip")]
        public static void Build_Mac_DebugNoZip() => Build(bo_Debug, bt_mac, debugBuildPrefix, false);

        [MenuItem("Framework/Build/Linux/Release")]
        public static void Build_Linux() => Build(bo_Release, bt_linux, string.Empty);

        [MenuItem("Framework/Build/Linux/ReleaseNoZip")]
        public static void Build_LinuxNoZip() => Build(bo_Release, bt_linux, string.Empty, false);

        [MenuItem("Framework/Build/Linux/Debug")]
        public static void Build_Linux_Debug() => Build(bo_Debug, bt_linux, debugBuildPrefix);

        [MenuItem("Framework/Build/Linux/DebugNoZip")]
        public static void Build_Linux_DebugNoZip() => Build(bo_Debug, bt_linux, debugBuildPrefix, false);

        private class BuildData
        {
            private Stopwatch stopwatch = new Stopwatch();

            private string BuildOutputString;

            public BuildData(BuildOptions options, BuildTarget target, string prefix, bool shouldzip)
            {
                var version = ProjectInfo.Instance.Last;

                BuildOutputString = "Build " + version.Version;

                switch (target)
                {
                    case BuildTarget.StandaloneLinux64:
                        BuildOutputString += " " + PLATFORM_LINUX;
                        break;
                    case BuildTarget.StandaloneOSX:
                        BuildOutputString += " " + PLATFORM_OSX;
                        break;
                    case BuildTarget.StandaloneWindows64:
                        BuildOutputString += " " + PLATFORM_WIN;
                        break;
                }

                if ((options & BuildOptions.Development) == BuildOptions.Development)
                {
                    BuildOutputString += " Development";
                }

                if(shouldzip)
                {
                    BuildOutputString += " with Zipping";
                }

                stopwatch.Restart();
            }

            public void BuildComplete(BuildReport buildReport)
            {
                UnityEngine.Debug.Log(BuildOutputString += " completed in: " + stopwatch.ElapsedMilliseconds / 1000 + " seconds, total size = " + buildReport.summary.totalSize / 1024 / 1024 + " mb");
            }
        }

        private static string Build(BuildOptions options, BuildTarget target, string prefix, bool shouldzip = true)
        {
            BuildData buildData = new BuildData(options, target, prefix, shouldzip);


            FrameworkScriptablePostProcessor.OnBuildCollectScriptables();
            AssetDatabase.SaveAssets(); 

            string buildsDir = "Builds";
            string root = Directory.GetParent(Application.dataPath).ToString() + "/" + buildsDir;
            UnityEngine.Debug.Log(root);

            bool rootexists = Directory.Exists(root);
            if (!rootexists)
            {
                uDebug.Log("Builds dir doesnt exist, creating");
                Directory.CreateDirectory(root);
            }

            var projectInfo = UnityExt.FindScriptableEditor<ProjectInfo>();
            if (!projectInfo)
            {
                uDebug.LogError("Could not find Project Info");
                return string.Empty;
            }

            string last = projectInfo.GetLast().Version.ToString();

            string platformName = null;
            string projectName = Application.productName;
            string exename = null;

            switch (target)
            {
                case BuildTarget.StandaloneLinux64:
                    platformName = PLATFORM_LINUX;
                    exename = projectName + ".x86_64";
                    break;
                case BuildTarget.StandaloneOSX:
                    platformName = PLATFORM_OSX; 
                    exename = projectName;
                    break;
                case BuildTarget.StandaloneWindows64:
                    platformName = PLATFORM_WIN;
                    exename = projectName + ".exe";
                    break;
            }


            string dirName = prefix + projectName + BUILD_SEPARATOR_TOKEN + platformName + BUILD_SEPARATOR_TOKEN + last;
            string localpath = buildsDir + "/" + dirName + "/" + exename;

            switch (target)
            {
                case BuildTarget.StandaloneLinux64:
                    break;
                case BuildTarget.StandaloneOSX:
                    localpath += ".app";
                    break;
                case BuildTarget.StandaloneWindows64:
                    break;
            }

            string buildDir = root + "/" + dirName + "/";



            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
            {
                scenes = new[] { 
                    "Assets/Scenes/Game.unity",
                    "Assets/Scenes/Loader.unity",
                },
                locationPathName = localpath,
                target = target,
                options = options,
            };


            BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildSummary summary = report.summary;

            if (summary.result == BuildResult.Succeeded)
            {

                if (target == bt_win)
                {
                    CopyFiles(ProjectRoot + "/Framework/CopyToBuildUtils_Windows/", buildDir);
                }

                if (shouldzip)
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.SaveProgress += (x, e) =>
                        {
                            var data = buildData;
                            var breport = report;
                            
                            if (e.EventType == ZipProgressEventType.Saving_Completed)
                            {
                                data.BuildComplete(breport);
                            }
                        };

                        zip.AddDirectory(root + "/" + dirName + "/");
                        zip.Save(root + "/" + dirName + ".zip");
                    }
                }
                else
                {
                    buildData.BuildComplete(report);
                }
            }
            else if (summary.result == BuildResult.Failed)
            {
                uDebug.Log("Build failed : " + summary.platform);
            }

            return buildDir;
        }

        private static void CopyFiles(string from, string to)
        {
            uDebug.Log($"Copying files from {from} to {to}");
            var files = Directory.GetFiles(from);
            foreach (var f in files)
            {
                uDebug.Log($"Copying {f}");
                File.Copy(f, to + Path.GetFileName(f), true);
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            Directory.CreateDirectory(destDirName);

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }
    }
}