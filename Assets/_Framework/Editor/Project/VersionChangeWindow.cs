﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.Linq;

namespace Framework.Editor
{
    public class VersionWindow : UnityEditor.EditorWindow
    {
        public ProjectInfo info;
        public ProjectInfo.VersionInfo lastVersion;
        public ProjectInfo.VersionInfo newVersion;
        public ProjectInfo.VersionInfo editingVersion;
        public ProjectInfo.VersionInfo currentlyInspectedVersion;

        private StringBuilder sb = new StringBuilder();

        private enum Mode
        {
            create,
            edit
        }

        private string[] modeNames = new string[]
        {
            "Create new",
            "Edit last"
        };

        private Mode mode;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Framework/Version Window")]
        static void Init()
        {
            if (EditorWindow.HasOpenInstances<VersionWindow>())
                return;

            // Get existing open window or if none, make a new one:
            VersionWindow window = (VersionWindow)EditorWindow.GetWindow(typeof(VersionWindow));
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 420, 720);
            window.Show();

        }

        private void OnEnable()
        {
            info = UnityExt.FindScriptableEditor<ProjectInfo>();
            VersionUpdated();
        }

        private void VersionUpdated()
        {
            lastVersion = info.GetLast();
            newVersion = new ProjectInfo.VersionInfo();
            newVersion.Version = lastVersion.Version;
            editingVersion = lastVersion;
            OnSwitchMode();
        }

        private void OnSwitchMode()
        {
            switch (mode)
            {
                case Mode.create:
                    currentlyInspectedVersion = newVersion;
                    break;
                case Mode.edit:
                    currentlyInspectedVersion = editingVersion;
                    break;
                default:
                    break;
            }
        }

        private void OnGUI()
        {
            // Current info
            GUILayout.BeginVertical(GUI.skin.box);
            {
                GUILayout.Label(Application.companyName + "." + Application.productName);

                Mode prevmode = mode;
                mode = (Mode)GUILayout.Toolbar((int)mode, modeNames);

                if (prevmode != mode)
                {
                    OnSwitchMode();
                }

                GUILayout.BeginHorizontal(GUI.skin.box);
                {
                    GUILayout.Label("Current Version: ");
                    GUILayout.Label(lastVersion.Version.ToString());


                    GUILayout.EndHorizontal();
                }

                GUILayout.EndVertical();
            }
            GUILayout.Space(20);


            // START NEW ENTRY
            GUILayout.BeginVertical(EditorStyles.helpBox);
            {
                GUILayout.Label("New Version Entry", EditorStyles.boldLabel);

                GUILayout.BeginHorizontal();
                {
                    SetColorChanged(lastVersion.Version.Major != currentlyInspectedVersion.Version.Major);
                    EditorGUILayout.IntField(currentlyInspectedVersion.Version.Major);
                    if (GUILayout.Button("+"))
                    {
                        currentlyInspectedVersion.Version.Major++;
                    }
                    if (GUILayout.Button("-"))
                    {
                        currentlyInspectedVersion.Version.Major = Mathf.Clamp(--currentlyInspectedVersion.Version.Major, lastVersion.Version.Major, 999);
                    }

                    SetColorChanged(lastVersion.Version.Minor != currentlyInspectedVersion.Version.Minor);
                    EditorGUILayout.IntField(currentlyInspectedVersion.Version.Minor);
                    if (GUILayout.Button("+"))
                    {
                        currentlyInspectedVersion.Version.Minor++;
                    }
                    if (GUILayout.Button("-"))
                    {
                        currentlyInspectedVersion.Version.Minor = Mathf.Clamp(--currentlyInspectedVersion.Version.Minor, lastVersion.Version.Minor, 999);
                    }

                    SetColorChanged(lastVersion.Version.Sub != currentlyInspectedVersion.Version.Sub);
                    EditorGUILayout.IntField(currentlyInspectedVersion.Version.Sub);
                    if (GUILayout.Button("+"))
                    {
                        currentlyInspectedVersion.Version.Sub++;
                    }
                    if (GUILayout.Button("-"))
                    {
                        currentlyInspectedVersion.Version.Sub = Mathf.Clamp(--currentlyInspectedVersion.Version.Sub, lastVersion.Version.Sub, 999);
                    }

                    ResetColor();
                    GUILayout.EndHorizontal();
                }

                GUILayout.BeginHorizontal();
                {
                    SetColorChanged(lastVersion.Version != currentlyInspectedVersion.Version);
                    GUILayout.Label("Version change : " + lastVersion.Version.ToString() + "    ►    " + currentlyInspectedVersion.Version.ToString());
                    ResetColor();
                    GUILayout.EndHorizontal();
                }

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Name", GUILayout.Width(100));
                    currentlyInspectedVersion.Name = EditorGUILayout.TextArea(currentlyInspectedVersion.Name);
                    GUILayout.EndHorizontal();
                }

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Description", GUILayout.Width(100));
                    currentlyInspectedVersion.Description = EditorGUILayout.TextArea(currentlyInspectedVersion.Description);
                    GUILayout.EndHorizontal();
                }

                if (currentlyInspectedVersion.ChangeLogEntries == null)
                    currentlyInspectedVersion.ChangeLogEntries = new List<ProjectInfo.ChangeLogEntry>();

                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Changelogs:", EditorStyles.boldLabel);

                    if (GUILayout.Button("Export Copy"))
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine($"Changelog version: {currentlyInspectedVersion.Version.ToString()}");

                        for (int i = 0; i < currentlyInspectedVersion.ChangeLogEntries.Count; i++)
                        {
                            var entry = currentlyInspectedVersion.ChangeLogEntries[i];
                            sb.AppendLine($" - {entry.type.ToString()} : {entry.Text}");
                        }

                        EditorGUIUtility.systemCopyBuffer = sb.ToString();
                    }

                    if (mode == Mode.edit)
                    {
                        if (GUILayout.Button("Sort"))
                        {
                            currentlyInspectedVersion.ChangeLogEntries = currentlyInspectedVersion.ChangeLogEntries.OrderBy(x => x.type).ToList();
                        }
                    }

                    if (GUILayout.Button("+"))
                    {
                        currentlyInspectedVersion.ChangeLogEntries.Add(new ProjectInfo.ChangeLogEntry());
                    }
                    GUILayout.EndHorizontal();
                }

                for (int i = 0; i < currentlyInspectedVersion.ChangeLogEntries.Count; i++)
                {
                    var entry = currentlyInspectedVersion.ChangeLogEntries[i];

                    GUILayout.BeginVertical(GUI.skin.box);
                    {
                        GUILayout.BeginHorizontal();
                        {
                            entry.type = (ProjectInfo.ChangeLogEntry.EntryType)EditorGUILayout.EnumPopup(entry.type, GUILayout.Width(100));
                            entry.Text = EditorGUILayout.TextArea(entry.Text);
                            if (GUILayout.Button("-", GUILayout.Width(30)))
                            {
                                currentlyInspectedVersion.ChangeLogEntries.RemoveAt(i);
                                break;
                            }
                            GUILayout.EndHorizontal();
                        }
                        GUILayout.EndVertical();
                    }
                }

                if (mode == Mode.create)
                {
                    if (GUILayout.Button("Submit", EditorStyles.toolbarButton))
                    {
                        if (EditorUtility.DisplayDialog("Confirm new version", "Are you sure you want to confirm this version?", "Да блять"))
                        {
                            info.AddVersion(currentlyInspectedVersion.Clone());
                            PlayerSettings.bundleVersion = currentlyInspectedVersion.Version.ToString();
                            VersionUpdated();
                        }
                    }
                    if (GUILayout.Button("Update in ProjectSettings", EditorStyles.toolbarButton))
                    {
                        PlayerSettings.bundleVersion = info.GetLast().Version.ToString();
                    }
                }

                GUILayout.EndVertical();
            }// END NEW ENTRY

            GUILayout.Space(20);

            // START PREVIOUS VERSIONS
            scroll = GUILayout.BeginScrollView(scroll);
            {
                GUILayout.BeginVertical(GUI.skin.box);
                {
                    foreach (var previousEntry in info.Versions)
                    {
                        if (mode == Mode.edit)
                        {
                            if (currentlyInspectedVersion == previousEntry)
                            {
                                GUI.color = Color.red;
                                EditorGUILayout.HelpBox("EDITING", MessageType.None);
                            }
                        }

                        EditorGUILayout.HelpBox(previousEntry.Version.ToString(), MessageType.None);
                        GUILayout.BeginHorizontal();
                        {

                            GUILayout.Label(previousEntry.Name, EditorStyles.miniLabel);
                            GUILayout.Label(previousEntry.Description, EditorStyles.miniLabel);
                            GUILayout.EndHorizontal();
                        }

                        ResetColor();

                        if (previousEntry.ChangeLogEntries == null)
                            continue;

                        foreach (var entry in previousEntry.ChangeLogEntries)
                        {
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Space(50);
                                SetColorEntryType(entry.type);
                                GUILayout.Label(entry.type.ToString(), GUILayout.Width(70));
                                ResetColor();
                                GUILayout.Label(entry.Text);
                                GUILayout.EndHorizontal();
                            }
                        }

                    }

                    GUILayout.EndVertical();
                }
                GUILayout.EndScrollView();
            }// END PREVIOUS VERSIONS

            EditorUtility.SetDirty(info);
        }

        private Vector2 scroll;

        private void SetColorChanged(bool changed)
        {
            GUI.color = changed ? Color.yellow : Color.white;
        }

        private void SetColorEntryType(ProjectInfo.ChangeLogEntry.EntryType type)
        {
            switch (type)
            {
                case ProjectInfo.ChangeLogEntry.EntryType.Added:
                case ProjectInfo.ChangeLogEntry.EntryType.Fixed:
                    GUI.color = Color.green;
                    break;
                case ProjectInfo.ChangeLogEntry.EntryType.Removed:
                    GUI.color = Color.red;
                    break;
                case ProjectInfo.ChangeLogEntry.EntryType.Improved:
                    GUI.color = Color.cyan;
                    break;
                case ProjectInfo.ChangeLogEntry.EntryType.Changed:
                    GUI.color = Color.yellow;
                    break;
                default:
                    break;
            }
        }

        private void ResetColor()
        {
            GUI.color = Color.white;
        }

        //private void DrawVersionReadOnly(ProjectInfo.VersionInfo info)
        //{
        //    GUILayout.Label();
        //}
    }
}
