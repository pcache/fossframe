﻿namespace Game.Utility.ScriptableObjectFactory
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;
    using UnityEditor.Experimental.UIElements;

    /// <summary>
    /// Helper class for instantiating ScriptableObjects.
    /// </summary>
    public class ScriptableObjectFactory
    {
        [MenuItem("Assets/Create/ScriptableObject")]
        public static void Create()
        {
            // Show the selection window.
            var window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
            window.ShowPopup();

            window.TypesUser = GetScriptableObjectTypes(GetAssembly("Assembly-CSharp"));
            window.TypesEditorInternal = GetScriptableObjectTypes(Assembly.GetAssembly(typeof(EditorWindow)));
            window.TypesPlugins = GetScriptableObjectTypes(GetAssembly("Assembly-CSharp-Firstpass"));
        }

        private static Type[] GetScriptableObjectTypes(Assembly asmb)
        {
            var types = asmb.GetTypes();
            return (from t in types
                    where t.IsSubclassOf(typeof(ScriptableObject)) && !t.IsAbstract
                    select t).ToArray();
        }


        /// <summary>
        /// Returns the assembly that contains the script code for this project (currently hard coded)
        /// </summary>
        private static Assembly GetAssembly(string name)
        {
            return Assembly.Load(new AssemblyName(name));
        }
    }
}