using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Framework.Editor
{
    public class GameObjectTool : EditorWindow
    {
        // Add menu named "My Window" to the Window menu
        [MenuItem("Framework/Game Object Tool")]
        static void Init()
        {
            if (EditorWindow.HasOpenInstances<GameObjectTool>())
                return;

            // Get existing open window or if none, make a new one:
            GameObjectTool window = (GameObjectTool)EditorWindow.GetWindow(typeof(GameObjectTool));
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 200, 400);
            window.Show();
        }

        private void OnGUI()
        {
            string text = "Selected GameObject: ";

            var go = Selection.activeGameObject;

            if (go != null)
            {
                text += "\n Name: " + go.name;
                text += "\n HideFlags: " + go.hideFlags.ToString();
            }

            GUILayout.Label(text);
        }

    }
}
