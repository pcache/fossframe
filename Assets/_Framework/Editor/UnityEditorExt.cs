﻿namespace Framework.Editor
{
    using System;
    using UnityEditor;
    using UnityEngine;

    public static class UnityEditorExt
    {
        public static bool IsFolder(this UnityEngine.Object obj)
        {
            return obj.GetType() == typeof(UnityEditor.DefaultAsset);
        }
    }
}
