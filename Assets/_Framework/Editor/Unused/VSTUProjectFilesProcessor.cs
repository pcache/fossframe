namespace Framework.Editor.Unused
{
    using System;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// This shows how to setup filtering and processing of generated project files if needs be.
    /// </summary>
    internal class VSTUProjectGeneratorPostprocessor : AssetPostprocessor
    {
        private static string[] WhitelistProjects = new string[]
        {
            "Assembly-CSharp",
            "Assembly-CSharp-Editor",
            "Assembly-CSharp-Editor-firstpass",
            "Assembly-CSharp-firstpass"
        };

        private static bool Enabled = false;

        public static string OnGeneratedSlnSolution(string path, string content)
        {
            if (!Enabled)
                return content;

            string result = content;

            using (StringReader sr = new StringReader(content))
            {
                using (StringWriter sw = new StringWriter())
                {
                    while (sr.Peek() != -1)
                    {
                        string line = sr.ReadLine();

                        if (line.StartsWith("Project("))
                        {
                            bool found = false;
                            // Include the project lines containing whitelisted while skipping any others
                            foreach (var item in WhitelistProjects)
                            {
                                if (line.Contains(item))
                                {
                                    found = true;
                                    break;
                                }
                            }

                            if (found)
                            {
                                sw.WriteLine(line);
                                sw.WriteLine(sr.ReadLine()); // Skip closing token
                            }
                            else
                            {
                                sr.ReadLine();
                            }
                        }
                        else
                            sw.WriteLine(line);
                    }

                    result = sw.ToString();
                }
            }

            Debug.Log(nameof(VSTUProjectGeneratorPostprocessor) + "cleaned up solution.");
            return result;

        }

        public static string OnGeneratedCSProject(string path, string content)
        {
            return content;
        }
    } 
}