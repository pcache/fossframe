﻿using UnityEngine;

namespace Framework.Editor
{
    public static class EditorConsts
    {
        public static string ROOT_NAMESPACE => Application.productName;
        public static string KEY_EDITOR_USER => ROOT_NAMESPACE + "user.";
        public static string KEY_DEVMODE => ROOT_NAMESPACE + "devmode";
    }
}