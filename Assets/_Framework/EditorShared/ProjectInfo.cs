﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Linq;
using System.Text;
using Framework.Editor;

namespace Framework
{
    [CreateAssetMenu(fileName ="ProjectInfo", menuName = "Framework/ProjectInfo")]
    public class ProjectInfo : SerializedScriptableObject
    {
        private static ProjectInfo _instance;
        public static ProjectInfo Instance
        {
            get
            {
                if (!_instance)
#if UNITY_EDITOR
                    _instance = UnityExt.FindScriptableEditor<ProjectInfo>();
#else
                    _instance = Resources.Load<ProjectInfo>(nameof(ProjectInfo));
#endif

                return _instance;
            }
        }

        [OdinSerialize]
        public string PrefsNameSpace { get; private set; }

        public VersionInfo Last => GetLast();

        [Serializable]
        public class ChangeLogEntry
        {
            public enum EntryType
            {
                Added = 10,
                Removed = 11,
                Fixed = 20,
                Improved = 30,
                Changed = 40
            }

            public EntryType type = EntryType.Added;
            public string Text;
        }

        [Serializable]
        public struct Version : IComparable<Version>
        {
            public int Major, Minor, Sub;

            private static StringBuilder sb = new StringBuilder();

            public int CompareTo(Version other)
            {
                if (Major == other.Major && Minor == other.Minor && Sub == other.Sub)
                    return 0;

                if (Major > other.Major)
                    return 1;

                if (Minor > other.Minor)
                    return 1;

                if (Sub > other.Sub)
                    return 1;

                return -1;
            }

            public static bool operator ==(Version operand1, Version operand2)
            {
                return operand1.CompareTo(operand2) == 0;
            }

            public static bool operator !=(Version operand1, Version operand2)
            {
                return operand1.CompareTo(operand2) != 0;
            }

            public override string ToString()
            {
                sb.Clear();
                sb.Append(Major);
                sb.Append('.');
                sb.Append(Minor);
                sb.Append('.');
                sb.Append(Sub);
                return sb.ToString();
            }
        }

        [Serializable]
        public class VersionInfo
        {
            public Version Version;
            public string Name;
            public string Description;
            public DateTime time;
            public List<ChangeLogEntry> ChangeLogEntries;

        }

        [OdinSerialize, HideLabel, HideReferenceObjectPicker]
        public List<VersionInfo> Versions { get; private set; } = new List<VersionInfo>();

        public VersionInfo GetLast()
        {
            return Versions.OrderByDescending(x => x.Version).FirstOrDefault();
        }

        public void AddVersion(VersionInfo info)
        {
            Versions.Add(info);
            Versions = Versions.OrderByDescending(x => x.Version).ToList();
        }
    }
}
