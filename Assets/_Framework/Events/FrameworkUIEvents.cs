﻿using Framework.UserInterface;

namespace Framework.Events
{
    public struct OnUIStateEnter : IEvent
    {
        public UIState state;
    }
    public struct OnUIStateExit : IEvent
    {
        public UIState state;
    }
}
