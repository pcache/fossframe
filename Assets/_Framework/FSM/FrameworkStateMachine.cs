﻿namespace Framework
{
    using Cysharp.Text;
    using Framework.Internal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Dummy enum used for dead ends 
    /// </summary>
    public enum Leaf
    {

    }

    [SystemsTypeMapTerminator]
    public abstract class FrameworkStateMachineLeaf : FrameworkStateMachine<Leaf>
    {
    }


    public interface IFrameworkStateMachine : IFrameworkSystem
    {
        IFrameworkStateMachine Parent { get; }
        void DoEnter();
        void DoExit();
        void KillCoroutines();
        void SetParent(IFrameworkStateMachine sm);
        void OnParentStateEnter();
        void OnParentStateExit();
    }

    public interface IFrameworkStateMachine<T> : IFrameworkStateMachine where T : Enum
    {
        event Action<T> OnStateChanged;
        T StateKey { get; }
        IFrameworkStateMachine State { get; }
        T1 RegisterState<T1>(T key, T1 state) where T1 : class, IFrameworkStateMachine;
        void SetState(T key, bool force = false);
    }

    public abstract class FrameworkStateMachine : FrameworkSystem, IFrameworkStateMachine
    {
        public IFrameworkStateMachine Parent { get; private set; }
        public bool StateEnterComplete { get; protected set; }

        void IFrameworkStateMachine.DoEnter() { }
        void IFrameworkStateMachine.DoExit() { }
        void IFrameworkStateMachine.KillCoroutines() { }
        void IFrameworkStateMachine.SetParent(IFrameworkStateMachine sm)
        {
            Parent = sm;
        }

        public virtual void OnParentStateEnter() { }
        public virtual void OnParentStateExit() { }
    }

    public abstract class FrameworkStateMachine<Key> : 
        FrameworkStateMachine,
        IFrameworkStateMachine,
        IFrameworkStateMachine<Key> where Key : Enum
    {
        public event Action<Key> OnStateChanged;
        public Key StateKey { get; private set; }
        public IFrameworkStateMachine State { get; private set; }
        
        /// <summary>
        /// Is the state currently running
        /// </summary>
        public bool Active { get; private set; }

        protected IFrameworkStateMachine thisIState => this as IFrameworkStateMachine;

        private Dictionary<Key, IFrameworkStateMachine> states = new Dictionary<Key, IFrameworkStateMachine>();
        private CoroutineHandle onEnterHandle;
        private CoroutineHandle onExitHandle;

        private List<CoroutineHandle> coroutineHandles = new List<CoroutineHandle>();

        /// <summary>
        /// Used for initializing the state machine externally
        /// </summary>
        public void SelfEnter()
        {
            thisIState.DoEnter();
        }

        public T RegisterState<T>(Key key, T state) where T : class, IFrameworkStateMachine
        {
            if (state == null)
                throw new ArgumentNullException();
            if (states.ContainsKey(key))
                Debug.Break();
            states.Add(key, state);
            state.SetParent(this);
            RegisterSubSystem(state);
            return state;
        }

        public void SetState(Key state, bool force = false)
        {
            if (!state.Equals(StateKey) || force)
            {
                if (states.TryGetValue(state, out var s))
                {
                    State?.DoExit();
                    State = s;
                    StateKey = state;
                    State?.DoEnter();
                    OnHandleStateChange(state);
                    OnStateChanged?.Invoke(state);
                }
            }
        }

        public FrameworkStateMachine GetState(Key key) 
        {
            if (states.TryGetValue(key, out var state))
                return state as FrameworkStateMachine;
            return null;
        }

        public IEnumerator<float> AwaitEnterComplete()
        {
            while (!StateEnterComplete)
                yield return 0;

            yield break;
        }

        protected string Print(string prefix)
        {
            using (var sb = ZString.CreateUtf8StringBuilder())
            {
                sb.Append(prefix);
                sb.Append(Parent == null ? "NoParent" : Parent.GetType().Name);
                sb.Append('.');
                sb.Append(GetType().Name);

                return sb.ToString();
            }
        }

        protected virtual void OnPreEnter() { }
        protected virtual void OnPreExit() { }
        protected virtual IEnumerator<float> OnEnter() { yield break; }
        protected virtual IEnumerator<float> OnExit() { yield break; }
        protected virtual void OnHandleStateChange(Key state) { }

        private void HandleEnterComplete()
        {
            StateEnterComplete = true;
        }

        protected bool IsActive() => Active;

        void IFrameworkStateMachine.DoEnter()
        {
            Active = true;
            OnPreEnter();
            thisIState.KillCoroutines();
            foreach (var s in states)
            {
                    s.Value.OnParentStateEnter();
            }
            onEnterHandle = Async.Run(OnEnter()).OnDestroy(HandleEnterComplete);
        }

        void IFrameworkStateMachine.DoExit()
        {
            Active = false;
            OnPreExit();
            thisIState.KillCoroutines();
            foreach (var s in states)
            {
                s.Value.OnParentStateExit();
            }
            onExitHandle = Async.Run(OnExit());
            StateEnterComplete = false;
        }

        void IUpdatable.DoUpdate(float dt)
        {
            if(FrameworkDebugSettings.Instance.DebugSystemLoops)
                FLog.Log(Time.frameCount + " <frame> , Running Update on " + GetType());

            OnUpdate(dt);
            State?.DoUpdate(dt);
            foreach (var s in EnumerateSubSystems())
            {
                s.DoUpdate(dt);
            }
        }

        void ILateUpdatable.DoLateUpdate(float dt)
        {
            if(FrameworkDebugSettings.Instance.DebugSystemLoops)
                FLog.Log(Time.frameCount + " <frame> , Running LateUpdate on " + GetType());

            OnLateUpdate(dt);
            State?.DoLateUpdate(dt);
            foreach (var s in EnumerateSubSystems())
            {
                s.DoLateUpdate(dt);
            }
        }

        void IFrameworkStateMachine.KillCoroutines()
        {
            Async.KillCoroutines(onEnterHandle);
            Async.KillCoroutines(onExitHandle);

            for (int i = 0; i < coroutineHandles.Count; i++)
            {
                var c = coroutineHandles[i];
                Async.KillCoroutines(c);
            }
        }

    }
}