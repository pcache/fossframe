﻿using Cysharp.Text;
using Framework;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    public interface IStateMachine : IUpdatable, ILateUpdatable, IInitializeable
    {
        IStateMachine Parent { get; }
        void DoEnter();
        void DoExit();
        void KillCoroutines();
        void SetParent(IStateMachine sm);
        void OnParentStateEnter();
        void OnParentStateExit();
    }

    public interface IStateMachine<T> : IStateMachine where T : Enum
    {
        event Action<T> OnStateChanged;
        T StateKey { get; }
        IStateMachine State { get; }
        T1 RegisterState<T1>(T key, T1 state) where T1 : class, IStateMachine;
        void SetState(T key, bool force = false);
    }

    public abstract class StateMachine : IStateMachine
    {
        public IStateMachine Parent { get; private set; }
        public bool StateEnterComplete { get; protected set; }

        void IInitializeable.DoInitialize() {
            OnInitialize();
        }
        void ILateUpdatable.DoLateUpdate(float dt) { }
        void IUpdatable.DoUpdate(float dt) { }
        void IStateMachine.DoEnter() { }
        void IStateMachine.DoExit() { }
        void IStateMachine.KillCoroutines() { }
        void IStateMachine.SetParent(IStateMachine sm)
        {
            Parent = sm;
        }

        public virtual void OnInitialize() { }
        public virtual void OnParentStateEnter() { }
        public virtual void OnParentStateExit() { }
    }

    public abstract class StateMachineKeyless : StateMachine, IStateMachine
    {
        public event Action<IStateMachine> OnStateChanged;
        public IStateMachine State { get; private set; }

        /// <summary>
        /// Is the state currently running
        /// </summary>
        public bool Active { get; private set; }

        protected IStateMachine thisIState => this as IStateMachine;

        public ReadOnlyList<IStateMachine> States { get; private set; }

        private List<IStateMachine> states = new List<IStateMachine>();
        private CoroutineHandle onEnterHandle;
        private CoroutineHandle onExitHandle;

        private List<CoroutineHandle> coroutineHandles = new List<CoroutineHandle>();

        /// <summary>
        /// Used for initializing the state machine externally
        /// </summary>
        public void SelfEnter()
        {
            thisIState.DoEnter();
        }

        public IStateMachine RegisterState(IStateMachine state) 
        {
            if (state == null)
                throw new ArgumentNullException();
            if (states.Contains(state))
                Debug.Break();
            states.Add(state);
            state.SetParent(this);
            state.DoInitialize();
            return state;
        }

        public void SetState(IStateMachine state)
        {
            State?.DoExit();
            State = state;
            State?.DoEnter();
            OnHandleStateChange(state);
            OnStateChanged?.Invoke(state);
        }

        public IEnumerator<float> AwaitEnterComplete()
        {
            while (!StateEnterComplete)
                yield return 0;

            yield break;
        }

        protected string Print(string prefix)
        {
            using (var sb = ZString.CreateUtf8StringBuilder())
            {
                sb.Append(prefix);
                sb.Append(Parent == null ? "NoParent" : Parent.GetType().Name);
                sb.Append('.');
                sb.Append(GetType().Name);

                return sb.ToString();
            }
        }

        protected virtual void OnPreEnter() { }
        protected virtual void OnPreExit() { }
        protected virtual void OnUpdate(float dt) { }
        protected virtual void OnLateUpdate(float dt) { }
        protected virtual IEnumerator<float> OnEnter() { yield break; }
        protected virtual IEnumerator<float> OnExit() { yield break; }
        protected virtual void OnHandleStateChange(IStateMachine state) { }

        private void HandleEnterComplete()
        {
            StateEnterComplete = true;
        }

        protected bool IsActive() => Active;

        void IStateMachine.DoEnter()
        {
            Active = true;
            OnPreEnter();
            thisIState.KillCoroutines();
            foreach (var s in states)
            {
                s.OnParentStateEnter();
            }
            onEnterHandle = Async.Run(OnEnter()).OnDestroy(HandleEnterComplete);
        }

        void IStateMachine.DoExit()
        {
            Active = false;
            OnPreExit();
            thisIState.KillCoroutines();

            foreach (var s in states)
            {
                s.OnParentStateExit();
            }

            onExitHandle = Async.Run(OnExit());
            StateEnterComplete = false;
        }

        void IUpdatable.DoUpdate(float dt)
        {
            OnUpdate(dt);
            State?.DoUpdate(dt);

            foreach (var s in states)
            {
                s.DoUpdate(dt);
            }
        }

        void ILateUpdatable.DoLateUpdate(float dt)
        {
            OnLateUpdate(dt);
            State?.DoLateUpdate(dt);

            foreach (var s in states)
            {
                s.DoLateUpdate(dt);
            }
        }

        void IStateMachine.KillCoroutines()
        {
            Async.KillCoroutines(onEnterHandle);
            Async.KillCoroutines(onExitHandle);

            for (int i = 0; i < coroutineHandles.Count; i++)
            {
                var c = coroutineHandles[i];
                Async.KillCoroutines(c);
            }
        }

    }

    public abstract class StateMachine<Key> :
        StateMachine,
        IStateMachine,
        IStateMachine<Key> where Key : Enum
    {
        public event Action<Key> OnStateChanged;
        public Key StateKey { get; private set; }
        public IStateMachine State { get; private set; }

        /// <summary>
        /// Is the state currently running
        /// </summary>
        public bool Active { get; private set; }

        protected IStateMachine thisIState => this as IStateMachine;

        public Dictionary<Key, IStateMachine>.ValueCollection States => states.Values;

        private Dictionary<Key, IStateMachine> states = new Dictionary<Key, IStateMachine>();
        private CoroutineHandle onEnterHandle;
        private CoroutineHandle onExitHandle;

        private List<CoroutineHandle> coroutineHandles = new List<CoroutineHandle>();

        /// <summary>
        /// Used for initializing the state machine externally
        /// </summary>
        public void SelfEnter()
        {
            thisIState.DoEnter();
        }

        public T RegisterState<T>(Key key, T state) where T : class, IStateMachine
        {
            if (state == null)
                throw new ArgumentNullException();
            if (states.ContainsKey(key))
                Debug.Break();
            states.Add(key, state);
            state.SetParent(this);
            return state;
        }

        public void SetState(Key state, bool force = false)
        {
            if (!state.Equals(StateKey) || force)
            {
                if (states.TryGetValue(state, out var s))
                {
                    State?.DoExit();
                    State = s;
                    StateKey = state;
                    State?.DoEnter();
                    OnHandleStateChange(state);
                    OnStateChanged?.Invoke(state);
                }
            }
        }

        public FrameworkStateMachine GetState(Key key)
        {
            if (states.TryGetValue(key, out var state))
                return state as FrameworkStateMachine;
            return null;
        }

        public IEnumerator<float> AwaitEnterComplete()
        {
            while (!StateEnterComplete)
                yield return 0;

            yield break;
        }

        protected string Print(string prefix)
        {
            using (var sb = ZString.CreateUtf8StringBuilder())
            {
                sb.Append(prefix);
                sb.Append(Parent == null ? "NoParent" : Parent.GetType().Name);
                sb.Append('.');
                sb.Append(GetType().Name);

                return sb.ToString();
            }
        }

        protected virtual void OnPreEnter() { }
        protected virtual void OnPreExit() { }
        protected virtual void OnUpdate(float dt) { }
        protected virtual void OnLateUpdate(float dt) { }
        protected virtual IEnumerator<float> OnEnter() { yield break; }
        protected virtual IEnumerator<float> OnExit() { yield break; }
        protected virtual void OnHandleStateChange(Key state) { }

        private void HandleEnterComplete()
        {
            StateEnterComplete = true;
        }

        protected bool IsActive() => Active;

        void IStateMachine.DoEnter()
        {
            Active = true;
            OnPreEnter();
            thisIState.KillCoroutines();
            foreach (var s in states)
            {
                s.Value.OnParentStateEnter();
            }
            onEnterHandle = Async.Run(OnEnter()).OnDestroy(HandleEnterComplete);
        }

        void IStateMachine.DoExit()
        {
            Active = false;
            OnPreExit();
            thisIState.KillCoroutines();

            foreach (var s in states)
            {
                s.Value.OnParentStateExit();
            }

            onExitHandle = Async.Run(OnExit());
            StateEnterComplete = false;
        }

        void IUpdatable.DoUpdate(float dt)
        {
            OnUpdate(dt);
            State?.DoUpdate(dt);

            foreach (var s in states)
            {
                s.Value.DoUpdate(dt);
            }
        }

        void ILateUpdatable.DoLateUpdate(float dt)
        {
            OnLateUpdate(dt);
            State?.DoLateUpdate(dt);

            foreach (var s in states)
            {
                s.Value.DoLateUpdate(dt);
            }
        }

        void IStateMachine.KillCoroutines()
        {
            Async.KillCoroutines(onEnterHandle);
            Async.KillCoroutines(onExitHandle);

            for (int i = 0; i < coroutineHandles.Count; i++)
            {
                var c = coroutineHandles[i];
                Async.KillCoroutines(c);
            }
        }

    }
}
