﻿namespace Framework
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class Find
    {
        private static class Systems
        {
            internal static TypeMap Global { get; private set; }

            private static readonly TypeMapFilter _filter = new TypeMapFilter(
                    new List<Type>()
                    {
                    typeof(FrameworkSystem),
                    typeof(FrameworkStateMachine),
                    typeof(FrameworkSystemMono)
                    },
                    new List<Type>()
                    {
                    typeof(FrameworkSystem),
                    typeof(FrameworkSystemMono),
                    typeof(UserInterface.UIRootState)},
                    new List<Type>()
                    {
                    });


            internal static void Initialize()
            {
                var allTypes = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user);
                var withTerminator = ReflectionUtil.GetAllTypesWithAttribute(allTypes, typeof(SystemsTypeMapTerminatorAttribute));

                foreach (var type in withTerminator)
                {
                    _filter.terminators.Add(type);
                }

                Global = new TypeMap(_filter);
            }

            internal static FrameworkStateMachine<TStateKey> GetStateMachineByKey<TStateKey>() where TStateKey : Enum
            {
                var genericTypeDefinition = typeof(FrameworkStateMachine<>);
                var genericType = genericTypeDefinition.MakeGenericType(typeof(TStateKey));

                return (FrameworkStateMachine<TStateKey>)Global.Get(genericType);
            }

            internal static T Get<T>() where T : class, IFrameworkSystem
            {
                var system = Global.Get<T>();
                if(system == null && AppLoader.Instance.AllowDynamicLoad)
                {
                    system = ResolveDynamic<T>();
                }
                return system;
            }

            internal static void Get<T>(ref T system) where T : class, IFrameworkSystem
            {
                system = Global.Get<T>();
                if (system == null && AppLoader.Instance.AllowDynamicLoad)
                {
                    // Attempt to create the system
                    system = ResolveDynamic<T>();
                }
            }

            internal static T ResolveDynamic<T>() where T : class, IFrameworkSystem
            {
                Type t = typeof(T);
                T system = null;

                if(t.IsSubclassOf(typeof(FrameworkSystemMono)))
                {
                    system = (T)Find.InScene(t);
                }
                else
                {
                    system = Activator.CreateInstance<T>();
                }

                AppLoader.AppRootSystem.RegisterSubSystem(system);

                if (AppLoader.Phase <= SystemInitializationPhase.Construct)
                    system.PhasedInitialization(SystemInitializationPhase.Construct);

                if(AppLoader.Phase <= SystemInitializationPhase.Initialize)
                    system.PhasedInitialization(SystemInitializationPhase.Initialize);

                if (AppLoader.Phase <= SystemInitializationPhase.SceneStart)
                    system.PhasedInitialization(SystemInitializationPhase.SceneStart);

                return system;
            }

        }

        internal static void RegisterSystem(IFrameworkSystem isystem)
        {
            Systems.Global.Add(isystem);
        }

        internal static void Initialize()
        {
            Systems.Initialize();
        }

        public static T InScene<T>() where T : Component
        {
            T instance = GameObject.FindObjectOfType<T>();
            if (!instance)
            {
                if (FrameworkScene.Instance)
                {
                    instance = FindInList<T>(FrameworkScene.Instance.GetSceneRoots());
                }
            }

            return instance;
        }

        public static object InScene(Type type)
        {
            UnityEngine.Object instance = GameObject.FindObjectOfType(type);
            if (!instance)
            {
                if (FrameworkScene.Instance)
                {
                    instance = FindInList(FrameworkScene.Instance.GetSceneRoots(), type);
                }
            }

            return instance;
        }

        private static UnityEngine.Object FindInList(List<GameObject> list, Type type)
        {
            foreach (var item in list)
            {
                UnityEngine.Object comp = item.GetComponentInChildren(type);
                if (comp)
                    return comp;
            }

            return null;
        }

        private static T FindInList<T>(List<GameObject> list) where T : Component
        {
            foreach (var item in list)
            {
                T comp = item.GetComponentInChildren<T>();
                if (comp)
                    return comp;
            }

            return null;
        }

        /// <summary>
        /// Returns you any globally registered FrameworkStateMachine by a type key generic argument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static FrameworkStateMachine<TStateKey> StateMachineByKey<TStateKey>() where TStateKey : Enum
        {
            return Systems.GetStateMachineByKey<TStateKey>();
        }

        /// <summary>
        /// Returns you any globally registered system by end or base type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T System<T>() where T : class, IFrameworkSystem
        {
            if(AppLoader.Phase <  SystemInitializationPhase.Initialize)
            {
                FLog.LogError("Getting references to systems is not allowed before Initialize phase.", FLogCategory.Framework);
                return null;
            }

            return Systems.Get<T>();
        }

        /// <summary>
        /// Returns you any globally registered system by end or base type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static void System<T>(ref T system) where T : class, IFrameworkSystem
        {
            if (AppLoader.Phase < SystemInitializationPhase.Initialize)
            {
                FLog.LogError("Getting references to systems is not allowed before Initialize phase.", FLogCategory.Framework);
                system = null;
                return;
            }

            Systems.Get(ref system);

            if(system == null)
            {
                if(AppLoader.Instance.VerboseLogging)
                    FLog.LogError($"Requested system {typeof(T).FullName} was not found in App.", FLogCategory.Framework);
            }
        }
    }
}
