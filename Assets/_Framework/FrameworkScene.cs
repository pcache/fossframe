﻿namespace Framework
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    [DefaultExecutionOrder(-32000)]
    public class FrameworkScene : MonoBehaviour
    {
        public static FrameworkScene Instance { get; private set; }

        [SerializeField] List<GameObject> DontDisableList = new List<GameObject>();
        private List<GameObject> roots = new List<GameObject>();
        private HashSet<GameObject> _dontDisableSet = new HashSet<GameObject>();

        private List<GameObject> _previouslyActiveObjects = new List<GameObject>();

        private void Awake()
        {
            Instance = this;

            for (int i = 0; i < DontDisableList.Count; i++)
            {
                _dontDisableSet.Add(DontDisableList[i]);
            }

            roots = SceneManager.GetActiveScene().GetRootGameObjects().ToList();

            foreach (var item in roots)
            {
                if (item.GetComponent<AppLoader>())
                    continue;

                if (_dontDisableSet.Contains(item))
                    continue;

                if (item.activeSelf)
                    _previouslyActiveObjects.Add(item);

                item.SetActive(false);
            }

        }

        public void InitializeScene()
        {
            foreach (var item in _previouslyActiveObjects)
            {
                item.SetActive(true);
            }

            _previouslyActiveObjects.Clear();
        }

        public List<GameObject> GetSceneRoots()
        {
            return roots;
        }
    }
}
