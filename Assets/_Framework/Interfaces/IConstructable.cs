﻿namespace Framework
{
    public interface IConstructable
    {
        void DoConstruct();
    }
}
