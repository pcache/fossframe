﻿namespace Framework
{
    public interface IDestroyable
    {
        void DoDestroy();
    }
}
