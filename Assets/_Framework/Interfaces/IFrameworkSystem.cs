﻿using Framework;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    public interface IFrameworkSystem : 
        IUpdatable,
        ILateUpdatable,
        IDestroyable
    {
        T RegisterSubSystem<T>(T subsystem) where T  : class, IFrameworkSystem;
        T GetSubSystem<T>() where T : class, IFrameworkSystem;
        FrameworkStateMachine<TStateKey> GetSubStateMachine<TStateKey>() where TStateKey : Enum;
        IEnumerable<IFrameworkSystem> EnumerateSubSystems();
        Transform AppRootTransform { get; }
        void PhasedInitialization(SystemInitializationPhase phase);

        int RunningVersion { get; }
        int AppRunningVersion { get; }
    }
}
