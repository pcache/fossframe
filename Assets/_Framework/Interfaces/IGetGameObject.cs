﻿namespace Framework
{
    using UnityEngine;
    public interface IGetGameObject
    {
        GameObject gameObject { get; }
    }
}
