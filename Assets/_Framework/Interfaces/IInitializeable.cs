﻿using Framework;
using System;

namespace Framework
{
    public interface IInitializeable
    {
        public void DoInitialize();
    }
}
