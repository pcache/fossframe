﻿namespace Framework
{
    public interface ILateUpdatable
    {
        void DoLateUpdate(float dt);
    }
}
