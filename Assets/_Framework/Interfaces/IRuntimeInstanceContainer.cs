﻿using System;

namespace Framework
{
    public interface IRuntimeInstanceContainer<T>
    {
        T GetRuntimeInstance();
    }
}
