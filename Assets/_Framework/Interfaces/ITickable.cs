﻿namespace Framework
{
    public enum Result
    {
        Success,
        Fail,
        Running
    }

    public interface ITickable
    {
        Result DoTick(float dt);
    }

}