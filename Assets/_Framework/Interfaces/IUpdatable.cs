﻿namespace Framework
{
    public interface IUpdatable
    {
        void DoUpdate(float dt);
    }
}