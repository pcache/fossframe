﻿namespace Framework
{
    using Framework.Internal;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [System.Serializable]
    public abstract class PlayerProfile : IDestroyable
    {
        public string Name { get; private set; }

        public ProfileValuesCollection<bool> Booleans { get; protected set; } = new ProfileValuesCollection<bool>();
        public ProfileValuesCollection<float> Numbers { get; protected set; } = new ProfileValuesCollection<float>();

        protected Dictionary<string, HashSet<Guid>> GuidBoundStrings = new Dictionary<string, HashSet<Guid>>();

        public PlayerProfile(string name)
        {
            this.Name = name;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsTrue(ProfileValueWithGuid value)
        {
            if (GuidBoundStrings.TryGetValue(value.Value, out var set))
            {
                return set.Contains(value.Guid);
            }
            return false;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetTrue(ProfileValueWithGuid value)
        {
            if (!GuidBoundStrings.TryGetValue(value.Value, out var set))
            {
                set = new HashSet<Guid>();
                GuidBoundStrings.Add(value.Value, set);
            }

            if (!set.Contains(value.Guid))
                set.Add(value.Guid);
        }

        public virtual void DoDestroy()
        {

        }
    }
}
