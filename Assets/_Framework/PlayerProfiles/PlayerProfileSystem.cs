﻿using System;

namespace Framework
{
    public abstract class PlayerProfileSystem : FrameworkSystem
    {
        public PlayerProfile CurrentBase { get; protected set; }

        protected override void OnConstruct()
        {
            base.OnConstruct();
        }

        public virtual void ResetProfile()
        {

        }

        public virtual string GetDebugDump()
        {
            return string.Empty;
        }
    }

    public abstract class PlayerProfileSystem<TProfileType> : PlayerProfileSystem
        where TProfileType : PlayerProfile
    {
        public virtual TProfileType Current
        {
            get => CurrentBase as TProfileType;
            set => CurrentBase = value;
        }
    }
}
