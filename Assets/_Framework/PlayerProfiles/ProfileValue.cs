﻿using Framework;
using System;

namespace Framework
{
    public interface IProfileValue : IDestroyable
    {
        
    }

    public abstract class ProfileValue<T> : IProfileValue where T : struct
    {
        public string Name { get; private set; }

        protected PlayerProfileSystem profileSystem;

        public ProfileValue(string name)
        {
            Find.System(ref profileSystem);
            this.Name = name;
        }

        abstract public T Get();
        abstract public void Set(T value);
        public virtual void DoDestroy() { }
    }

    public class BoolProfileValue : ProfileValue<bool>
    {
        public BoolProfileValue(string name) : base(name)
        {
        }

        public override bool Get()
        {
            return profileSystem.CurrentBase.Booleans.Get(Name);
        }

        public override void Set(bool value)
        {
            profileSystem.CurrentBase.Booleans.Set(Name, value);
        }
    }

    public class ScalarProfileValue : ProfileValue<float>
    {
        public ScalarProfileValue(string name) : base(name)
        {
        }

        public override float Get()
        {
            return profileSystem.CurrentBase.Numbers.Get(Name);
        }

        public override void Set(float value)
        {
            profileSystem.CurrentBase.Numbers.Set(Name, value);
        }
    }

    /// <summary>
    /// Binds to a specified event and tracks event invocations with optional predicate.
    /// </summary>
    /// <typeparam name="TEvent"></typeparam>
    public class BindableScalarStatistic<TEvent> : ScalarProfileValue where TEvent : struct, IEvent
    {
        public event Func<TEvent, float> HandlerOverride;
        private Func<TEvent, bool> predicate;
        private EventBinding<TEvent> eventBinding;

        public BindableScalarStatistic(string name, Func<TEvent, bool> predicate = null) : base(name)
        {
            this.predicate = predicate;
            this.eventBinding = new EventBinding<TEvent>(HandleEvent);
        }

        protected virtual void HandleEvent(TEvent eventData)
        {
            bool isValid = true;

            if (predicate != null)
                isValid = predicate(eventData);

            if (isValid)
            {
                if (HandlerOverride == null)
                {
                    Set(Get() + 1);
                }
                else
                    Set(Get() + HandlerOverride.Invoke(eventData));
            }
        }

        public override string ToString()
        {
            return $"ScalarStatistic: {typeof(TEvent).Name} - {Get()}";
        }

        public override void DoDestroy()
        {
            base.DoDestroy();

            eventBinding.Listen = false;
        }
    }
}
