﻿namespace Framework
{
    using System;
    using System.Runtime.CompilerServices;

    public struct ProfileValueWithGuid
    {
        public string Value;
        public Guid Guid;

        PlayerProfileSystem profileSys;

        public ProfileValueWithGuid(string value, Guid guid)
        {
            this.Value = value;
            this.Guid = guid;
            this.profileSys = null;
            Find.System(ref profileSys);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Set()
        {
            profileSys.CurrentBase.SetTrue(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Get()
        {
            return profileSys.CurrentBase.IsTrue(this);
        }

        public static implicit operator bool(ProfileValueWithGuid val)
        {
            return val.Get();
        }
    }
}
