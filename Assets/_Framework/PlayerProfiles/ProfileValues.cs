﻿namespace Framework
{
    using Framework;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public abstract class ProfileValues : IDestroyable
    {
        protected List<IProfileValue> statisticsValues = new List<IProfileValue>();
        private object[] ctorParams = new object[2];

        public ProfileValues()
        {
            this.InitializeProperties(true, x =>
            {
                ctorParams[0] = x.Name;
                var obj = Activator.CreateInstance(x.PropertyType, ctorParams);
                return obj as IProfileValue;
            });

            statisticsValues = this.GetPropertyValuesOfType<IProfileValue>(true);
        }

        public virtual void DoDestroy()
        {
            foreach (var item in statisticsValues)
            {
                item.DoDestroy();
            }
        }

        public virtual string GetDebugDump()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in statisticsValues)
            {
                sb.AppendLine().Append('-').Append(item.ToString());
            }

            return sb.ToString();
        }
    }
}
