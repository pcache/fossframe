﻿using Framework;
using System;
using System.Collections.Generic;

namespace Framework.Internal
{
    [System.Serializable]
    public class ProfileValuesCollection<T> 
    {
        private Dictionary<string, T> values = new Dictionary<string, T>();

        public virtual T Get(string name)
        {
            if (values.TryGetValue(name, out T val))
                return val;
            return default;
        }

        public virtual void Set(string name, T value)
        {
            values[name] = value;
        }
    }
}
