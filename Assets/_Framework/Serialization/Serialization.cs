﻿using Framework;
using Framework.Internal;
using System;
using System.IO;
using UnityEngine;

namespace Framework
{
    public class Serialization : FrameworkSystem
    {
        /*---------------------------------------------*/
        /* DEPENDENCIES                                                 */

        PlayerProfileSystem playerProfileSystem;
        /*---------------------------------------------*/

        public class ProfileDirectory : FrameworkDirectory
        {
            private const string SAVES = "/Saves";
            private const string DATA = "/Data";
            private const string SETTINGS = "/Settings";
            private const string AUTOBACKUP = "/Autobackup";

            public FrameworkDirectory Parent { get; private set; }
            public FrameworkDirectory Saves { get; private set; }
            public FrameworkDirectory Data { get; private set; }
            public FrameworkDirectory Settings { get; private set; }
            public FrameworkDirectory Autobackup { get; private set; }

            public ProfileDirectory(FrameworkDirectory parent, string name)
            {
                this.Parent = parent;
                this.Path = parent.Path.AppendPath(name);
                EnsureDirectory(Path);

                Saves = new FrameworkDirectory(Path.AppendPath(SAVES));
                Data = new FrameworkDirectory(Path.AppendPath(DATA));
                Settings = new FrameworkDirectory(Path.AppendPath(SETTINGS));
                Autobackup = new FrameworkDirectory(Path.AppendPath(AUTOBACKUP));
            }
        }


        private const string PROFILES = "Profiles";
        private const string GLOBAL = "Global";

        public FrameworkDirectory Profiles { get; private set; }
        public FrameworkDirectory Global { get; private set; }
        public ProfileDirectory CurrentProfileDir { get; set; }

        protected override void OnConstruct()
        {
            base.OnConstruct();

            var root = Application.persistentDataPath;

            Profiles = new FrameworkDirectory(root.AppendPath(PROFILES));
            Global = new FrameworkDirectory(root.AppendPath(GLOBAL));

            CurrentProfileDir = new ProfileDirectory(Profiles, "defaultProfile");
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            Find.System(ref playerProfileSystem);
        }

        public virtual void SaveGame()
        {

        }

        public virtual void LoadGame()
        {

        }
    }
}
