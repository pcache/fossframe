﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    [AttributeUsage((AttributeTargets.Class | AttributeTargets.Struct), AllowMultiple = false, Inherited = false)]
    public class SystemsTypeMapTerminatorAttribute : Attribute
    {
    }

}
