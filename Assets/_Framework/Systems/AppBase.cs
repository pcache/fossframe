﻿namespace Framework
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [SystemsTypeMapTerminator]
    public abstract class AppBase<T, TStateMachineKey> : FrameworkStateMachine<TStateMachineKey>,
        IFrameworkApp
        where T : AppBase<T, TStateMachineKey>, new()
        where TStateMachineKey : Enum
    {
        public DefDB DefDB { get; private set; }

        public static T Instance => AppLoader.App as T;
        public static event Action OnAppDestroyed;
        public static event Action OnSceneInitialized;

        protected override void OnConstruct()
        {
            base.OnConstruct();

            FLog.Log("Initializing app", FLogCategory.Framework);

            DefDB = Resources.Load<DefDB>("DefDB");
            DefDB.RuntimeInitialize();
        }

        /*        void IFrameworkApp.SceneStart(AppLoader initializer)
                {
                    ((IFrameworkSystem)this)?.DoSceneStart();
                    FLog.Log("Scene Start complete", FLogCategory.Framework);
                    OnSceneInitialized?.Invoke();
                }*/

        protected virtual void FixedUpdate(float dt) { }
        protected virtual void OnGUI() { }
        protected virtual void OnDestroy() { }

        void IMonoEventUpdate.OnMonoUpdate()
        {
            ((IUpdatable)this).DoUpdate(Time.deltaTime);
        }
        void IMonoEventLateUpdate.OnMonoLateUpdate() => ((ILateUpdatable)this).DoLateUpdate(Time.deltaTime);
        void IMonoEventFixedUpdate.OnMonoFixedUpdate() => FixedUpdate(Time.deltaTime);
        void IMonoEventOnDestroy.OnMonoOnDestroy()
        {
            Async.KillCoroutines();
            ((IDestroyable)this).DoDestroy();
            OnAppDestroyed?.Invoke();
        }

        void IMonoEventOnGUI.OnMonoGUI()
        {
            OnGUI();
        }
    }
}