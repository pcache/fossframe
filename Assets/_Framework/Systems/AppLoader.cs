namespace Framework
{
    using Sirenix.OdinInspector;
    using Sirenix.Serialization;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Reflection;
    using System.Linq;
    using Framework.Data;
    using UnityEngine.SceneManagement;

    public interface IMonoEventAwake
    {
        void OnMonoAwake(GameObject gameobject);
    }

    public interface IMonoEventStart
    {
        void OnMonoStart();
    }

    public interface IMonoEventUpdate
    {
        void OnMonoUpdate();
    }

    public interface IMonoEventLateUpdate
    {
        void OnMonoLateUpdate();
    }

    public interface IMonoEventFixedUpdate
    {
        void OnMonoFixedUpdate();
    }

    public interface IMonoEventOnDestroy
    {
        void OnMonoOnDestroy();
    }

    public interface IMonoEventOnGUI
    {
        void OnMonoGUI();
    }
    
    public interface IFrameworkApp : 
        IMonoEventFixedUpdate,
        IMonoEventLateUpdate,
        IMonoEventOnDestroy,
        IMonoEventOnGUI,
        IMonoEventUpdate
    { 
    }

    public enum SystemInitializationPhase
    {
        None = 0,
        Construct = 1,
        Initialize = 2,
        SceneStart = 3,
        Run = 4
    }

    public sealed class AppLoader :  SerializedMonoBehaviour
    {
        public static Transform RootTransform { get; private set; }
        public static IFrameworkSystem AppRootSystem { get; private set; }
        public static IFrameworkApp App { get; private set; }
        public static event Action<bool> onDevModeChanged;
        public static SystemInitializationPhase Phase { get; private set; }
        public static AppLoader Instance { get; private set; }
        public static int RunningVersion { get; private set; }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void AutoInit()
        {
            Async.KillCoroutines();
            EventBusUtility.Init();
            onDevModeChanged = null;
            RootTransform = null;
            AppRootSystem = null;
            App = null;
        }

        private static bool _devMode;
        public static bool DevMode
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            get => _devMode;
#else
            get => false; 
#endif
            set
            {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                _devMode = value;
                onDevModeChanged?.Invoke(_devMode);
#endif
            }
        }

        [OdinSerialize, ValueDropdown("GetAppTypeList")]
        public string AppID { get; private set; }
        
        [OdinSerialize]
        public bool AllowDynamicLoad { get; private set; }

        [OdinSerialize]
        public bool VerboseLogging { get; private set; }

        private IEnumerable<string> GetAppTypeList()
        {
            var types = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user);
            var filtered = ReflectionUtil.GetAllTypesWithAttribute(types, typeof(AppIDAttribute));

            foreach (Type item in filtered)
            {
                var attr = item.GetCustomAttribute<AppIDAttribute>();
                yield return attr.ID;
            }
        }

        private void Awake()
        {
            Instance = this;

#if UNITY_EDITOR
            DevMode = Framework.Editor.EditorConsts.KEY_DEVMODE.GetEditorPrefsBool();
#else
            DevMode =  Debug.isDebugBuild;
#endif
            Async.KillCoroutines();

            RootTransform = transform;
            var types = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user);
            var filtered = ReflectionUtil.GetAllTypesWithAttribute(types, typeof(AppIDAttribute));
            var appType = filtered.First(x => x.GetCustomAttribute<AppIDAttribute>().ID == AppID);

            Find.Initialize();

            App = Activator.CreateInstance(appType) as IFrameworkApp;
            AppRootSystem = App as IFrameworkSystem;

            Phase = SystemInitializationPhase.Construct;
            AppRootSystem.PhasedInitialization(SystemInitializationPhase.Construct);

            Phase = SystemInitializationPhase.Initialize;
            AppRootSystem.PhasedInitialization(SystemInitializationPhase.Initialize);

            Phase = SystemInitializationPhase.SceneStart;
            var sceneInitializer = FrameworkScene.Instance;
            if (sceneInitializer)
                sceneInitializer.InitializeScene();
            AppRootSystem.PhasedInitialization(SystemInitializationPhase.SceneStart);

            Phase = SystemInitializationPhase.Run;
            RunningVersion++;
        }

        private void Update() => App.OnMonoUpdate();
        private void LateUpdate() => App.OnMonoLateUpdate();
        private void FixedUpdate() => App.OnMonoFixedUpdate();
        private void OnDestroy()
        {
            App.OnMonoOnDestroy();
            AutoInit();
        }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        private void OnGUI() => App.OnMonoGUI(); 
#endif
    }

    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false)]
    public class AppIDAttribute : System.Attribute
    {
        public string ID { get; private set; }

        public AppIDAttribute(string id)
        {
            this.ID = id;
        }
    }
}