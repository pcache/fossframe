﻿namespace Framework
{
    using Framework.Internal;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class FrameworkSystem : IFrameworkSystem
    {
        public UnityEngine.Transform AppRootTransform => AppLoader.RootTransform;
        protected UnityEngine.Transform SystemTransform { get; private set; }
        public int RunningVersion { get; private set; }
        public int AppRunningVersion => AppLoader.RunningVersion;

        public SystemInitializationPhase CompletedPhase;

        public List<IFrameworkSystem> subSystems = new List<IFrameworkSystem>();

        public T RegisterSubSystem<T>(T subsystem) where T : class, IFrameworkSystem
        {
            IFrameworkSystem isystem = subsystem;
            if (isystem == null)
            {
                FLog.LogError(GetType().Name + " Tried to register a null system: ", FLogCategory.Framework);
                return null;
            }

            if (subSystems.Contains(isystem))
            {
                FLog.LogError(GetType().Name + " SubSystem already registered: " + isystem.GetType().Name, FLogCategory.Framework);
                return null;
            }

            subSystems.Add(isystem);

            if (FrameworkDebugSettings.Instance.DebugSystemInit)
            {
                FLog.Log(GetType().Name + " Registered sub system: ", isystem.GetType().Name, FLogCategory.Framework);
            }

            Find.RegisterSystem(subsystem);

            return subsystem;
        }

        public T GetSubSystem<T>() where T : class, IFrameworkSystem
        {
            var type = typeof(T);

            for (int i = 0; i < subSystems.Count; i++)
            {
                var stype = subSystems[i].GetType();
                if (type == stype || stype.IsSubclassOf(type))
                    return subSystems[i] as T;
            }

            return null;
        }

        FrameworkStateMachine<TStateKey> IFrameworkSystem.GetSubStateMachine<TStateKey>()
        {
            var genericType = ReflectionUtil.GetCachedGenericType(typeof(FrameworkStateMachine<>), typeof(TStateKey));

            for (int i = 0; i < subSystems.Count; i++)
            {
                var stype = subSystems[i].GetType();
                if (stype.IsSubclassOf(genericType))
                    return subSystems[i] as FrameworkStateMachine<TStateKey>;
            }

            return null;
        }

        public IEnumerable<IFrameworkSystem> EnumerateSubSystems()
        {
            foreach (var system in subSystems)
                yield return system;
        }

        void IFrameworkSystem.PhasedInitialization(SystemInitializationPhase phase)
        {
            if (CompletedPhase == phase)
                return;

            switch (phase)
            {
                case SystemInitializationPhase.None:
                    break;
                case SystemInitializationPhase.Construct:
                    {
                        CompletedPhase = SystemInitializationPhase.Construct;

                        RunningVersion = AppLoader.RunningVersion;

                        var go = new UnityEngine.GameObject(this.GetType().Name);
                        SystemTransform = go.transform;
                        SystemTransform.SetParent(AppRootTransform);

                        OnConstruct();

                        for (int i = 0; i < subSystems.Count; i++)
                        {
                            subSystems[i].PhasedInitialization(phase);
                        }
                    }
                    break;
                case SystemInitializationPhase.Initialize:
                    {
                        CompletedPhase = SystemInitializationPhase.Initialize;

                        if (FrameworkDebugSettings.Instance.DebugSystemInit)
                        {
                            if (GetType().IsSubclassOf(typeof(FrameworkStateMachine)))
                            {
                                FLog.Log("Initializing sub state: ", GetType().Name, FLogCategory.Framework);
                            }
                            else
                            {
                                FLog.Log("Initializing sub system: ", GetType().Name, FLogCategory.Framework);
                            }
                        }

                        OnInitialize();


                        for (int i = 0; i < subSystems.Count; i++)
                        {
                            subSystems[i].PhasedInitialization(phase);
                        }
                    }
                    break;
                case SystemInitializationPhase.SceneStart:
                    {
                        CompletedPhase = SystemInitializationPhase.SceneStart;

                        OnSceneStart();

                        for (int i = 0; i < subSystems.Count; i++)
                        {
                            subSystems[i].PhasedInitialization(phase);
                        }
                    }
                    break;
                case SystemInitializationPhase.Run:
                    {
                        CompletedPhase = SystemInitializationPhase.Run;
                    }
                    break;
                default:
                    break;
            }
        }

        void IUpdatable.DoUpdate(float dt)
        {
            if (FrameworkDebugSettings.Instance.DebugSystemLoops)
                FLog.Log(UnityEngine.Time.frameCount + " <frame> , Running Update on " + GetType());

            OnUpdate(dt);
            for (int i = 0; i < subSystems.Count; i++)
            {
                subSystems[i].DoUpdate(dt);
            }
        }

        void ILateUpdatable.DoLateUpdate(float dt)
        {
            if (FrameworkDebugSettings.Instance.DebugSystemLoops)
                FLog.Log(UnityEngine.Time.frameCount + " <frame> , Running LateUpdate on " + GetType());

            OnLateUpdate(dt);
            for (int i = 0; i < subSystems.Count; i++)
            {
                subSystems[i].DoLateUpdate(dt);
            }
        }

        void IDestroyable.DoDestroy()
        {
            OnDestroyed();
            for (int i = 0; i < subSystems.Count; i++)
            {
                subSystems[i].DoDestroy();
            }
        }

        protected virtual void OnConstruct() { }
        protected virtual void OnInitialize() { }
        protected virtual void OnSceneStart() { }
        protected virtual void OnUpdate(float dt) { }
        protected virtual void OnLateUpdate(float dt) { }
        protected virtual void OnDestroyed() { }

        public static implicit operator bool(FrameworkSystem system)
        {
            return system != null;
        }
    }
}
