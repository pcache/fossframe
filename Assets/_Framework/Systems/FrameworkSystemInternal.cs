﻿/*namespace Framework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    internal class FrameworkSystemInternal
    {
        public SystemInitializationPhase Phase;
        public const bool PrintDebugUpdates = false;
        public const bool PrintSystemInitialization = false;

        public List<IFrameworkSystem> subSystems;
        public IFrameworkSystem owner;

        public FrameworkSystemInternal(IFrameworkSystem owner)
        {
            this.Phase = SystemInitializationPhase.None;
            this.owner = owner;
            this.subSystems = new List<IFrameworkSystem>();
        }

        public T GetSubSystem<T>() where T : class, IFrameworkSystem
        {
            var type = typeof(T);

            for (int i = 0; i < subSystems.Count; i++)
            {
                var stype = subSystems[i].GetType();
                if (type == stype || stype.IsSubclassOf(type))
                    return subSystems[i] as T;
            }

            return null;
        }

        public T RegisterSubSystem<T>(T subsystem) where T : class, IFrameworkSystem
        {
            IFrameworkSystem isystem = subsystem;
            if (isystem == null)
            {
                FLog.LogError(owner.GetType().Name + " Tried to register a null system: ", FLogCategory.Framework);
                return null;
            }

            if (subSystems.Contains(isystem))
            {
                FLog.LogError(owner.GetType().Name + " SubSystem already registered: " + isystem.GetType().Name, FLogCategory.Framework);
                return null;
            }

            subSystems.Add(isystem);
            Find.RegisterSystem(isystem);

            isystem.PhasedInitialization(SystemInitializationPhase.Construct);

            if (PrintSystemInitialization)
            {
                FLog.Log(owner.GetType().Name + " Registered sub system: ", isystem.GetType().Name, FLogCategory.Framework);
            }

            return subsystem;
        }
*//*
        public T RegisterSubSystemDynamic<T>(T subsystem) where T : IFrameworkSystem
        {
            Type t = typeof(T);
            IFrameworkSystem isystem = subsystem;

            if (isystem == null)
            {
                FLog.LogError(owner.GetType().Name + " Tried to register a null system: ", FLogCategory.Framework);
                return default;
            }

            if (subSystems.Contains(isystem))
            {
                FLog.LogError(owner.GetType().Name + " SubSystem already registered: " + isystem.GetType().Name, FLogCategory.Framework);
                return default;
            }

            subSystems.Add(isystem);
            Find.RegisterSystem(isystem);

            if (PrintSystemInitialization)
            {
                FLog.Log(owner.GetType().Name + " Registered sub system: ", isystem.GetType().Name, FLogCategory.Framework);
            }

            if (AppLoader.Phase >= AppLoader.AppLoadingPhase.Construct)
                isystem.DoConstruct();

            if (AppLoader.Phase >= AppLoader.AppLoadingPhase.Initialize)
                isystem.DoInitialize();

            if (AppLoader.Phase >= AppLoader.AppLoadingPhase.SceneStart)
                isystem.DoSceneStart();


            return subsystem;
        }*//*

        private void RegisterSubSystem(Type type, IFrameworkSystem isystem)
        {

        }

        public void PhasedInitialization(SystemInitializationPhase phase)
        {
            switch (phase)
            {
                case SystemInitializationPhase.None:
                    break;
                case SystemInitializationPhase.Construct:
                    foreach (var s in subSystems)
                        ((IConstructable)s).DoConstruct();
                    break;
                case SystemInitializationPhase.Initialize:
                    break;
                case SystemInitializationPhase.SceneStart:
                    break;
                case SystemInitializationPhase.Run:
                    break;
                default:
                    break;
            }
        }

        public FrameworkStateMachine<TStateKey> GetSubStateMachine<TStateKey> () where TStateKey : Enum
        {
            var genericType = ReflectionUtil.GetCachedGenericType(typeof(FrameworkStateMachine<>), typeof(TStateKey));

            for (int i = 0; i < subSystems.Count; i++)
            {
                var stype = subSystems[i].GetType();
                if (stype.IsSubclassOf(genericType))
                    return subSystems[i] as FrameworkStateMachine<TStateKey>;
            }

            return null;
        }

        public void Construct()
        {
            
        }

        public void Initialize()
        {
            if (PrintSystemInitialization)
            {
                if (owner.GetType().IsSubclassOf(typeof(FrameworkStateMachine)))
                {
                    FLog.Log("Initializing sub state: ", owner.GetType().Name, FLogCategory.Framework);
                }
                else
                {
                    FLog.Log("Initializing sub system: ", owner.GetType().Name, FLogCategory.Framework);
                }
            }
            foreach (var s in subSystems)
                ((IInitializeable)s).DoInitialize();
        }

        public void SceneStart()
        {
            foreach (var s in subSystems)
                s.DoSceneStart();
        }

        public void Update(float dt)
        {
            if (PrintDebugUpdates)
                FLog.Log(UnityEngine.Time.frameCount + " <frame> , Running Update on " + owner.GetType());

            foreach (var s in subSystems)
                s.DoUpdate(dt);
        }

        public void LateUpdate(float dt)
        {
            if (PrintDebugUpdates)
                FLog.Log(UnityEngine.Time.frameCount + " <frame> , Running LateUpdate on " + owner.GetType());

            foreach (var s in subSystems)
                s.DoLateUpdate(dt);
        }

        public void Destroy()
        {
            foreach (var s in subSystems)
            {
                if (s != null)
                    s.DoDestroy();
            }
        }
    }
}
*/