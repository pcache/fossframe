﻿using Framework;
using System;
using System.Collections.Generic;

namespace Framework
{
    public abstract class ObjectPoolConstructor<T> :
        IObjectPoolConstructorInternal
        where T : class, IPoolable
    {
        public abstract T Construct();

        IPoolable IObjectPoolConstructorInternal.ConstructInternal()
        {
            T obj = Construct();
            obj.OnPoolRent();
            return obj;
        }
    }

    public interface IPoolable
    {
        void OnPoolReset();
        void OnPoolRent();
    }

    interface IObjectPoolConstructorInternal
    {
        IPoolable ConstructInternal();
    }

    public class ObjectPool : FrameworkSystem
    {
        protected virtual int PrewarmCount { get; } = 1;

        private struct Pool
        {
            public Type type;
            public IObjectPoolConstructorInternal ctor;
            public Stack<IPoolable> stack;

            public Pool(Type type, IObjectPoolConstructorInternal ctor)
            {
                this.type = type;
                this.ctor = ctor;
                this.stack = new Stack<IPoolable>();
            }

            public IPoolable Rent()
            {
                if (stack.Count == 0)
                {
                    if (ctor == null)
                    {
                        IPoolable poolable = Activator.CreateInstance(type) as IPoolable;
                        poolable.OnPoolRent();
                        return poolable;
                    }

                    return ctor.ConstructInternal();
                }
                else
                    return stack.Pop();
            }

            public void Return(IPoolable poolable)
            {
                poolable.OnPoolReset();
                stack.Push(poolable);
            }
        }

        private Dictionary<Type, Pool> _pools = new Dictionary<Type, Pool>();

        private Dictionary<Type, IObjectPoolConstructorInternal> _ctors = new Dictionary<Type, IObjectPoolConstructorInternal>();

        protected override void OnConstruct()
        {
            base.OnConstruct();

            var types = ReflectedTypeStore.GetUnityTypes(
                ReflectedTypeStore.UnityAssembly.user,
                typeof(ObjectPoolConstructor<>), true, false);

            foreach (var type in types)
            {
                var ctorType = type.GenericTypeArguments[0];
                var ctor = Activator.CreateInstance(type) as IObjectPoolConstructorInternal;

                _ctors.Add(ctorType, ctor);
            }
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        public T Rent<T>() where T : class, IPoolable, new()
        {
            if(!_pools.TryGetValue(typeof(T), out Pool pool))
            {
                pool = ConstructAndRegisterPool<T>();
            }

            return pool.Rent() as T;
        }

        public void Return<T>(T target) where T : class, IPoolable, new()
        {
            if(!_pools.TryGetValue(typeof(T), out Pool pool))
            {
                pool = ConstructAndRegisterPool<T>();
            }

            pool.Return(target);
        }

        private Pool ConstructAndRegisterPool<T>() where T : class, IPoolable, new()
        {
            _ctors.TryGetValue(typeof(T), out var ctor);
            Pool pool = new Pool(typeof(T), ctor);
            _pools.Add(typeof(T), pool);
            return pool;
        }
    }
}
