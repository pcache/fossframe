﻿using Framework;
using System;

namespace Framework.UserInterface
{
    public abstract class BindableUIState<TStateKey> : UIState where TStateKey : Enum
    {
        protected FrameworkStateMachine<TStateKey> FSM { get; private set; }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            FSM = Find.StateMachineByKey<TStateKey>();
            FSM.OnStateChanged -= HandleFSMStateChange; 
            FSM.OnStateChanged += HandleFSMStateChange;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();
            HandleFSMStateChange(FSM.StateKey);
        }

        protected abstract void HandleFSMStateChange(TStateKey key);
    }
}
