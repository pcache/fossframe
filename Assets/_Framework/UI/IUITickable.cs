﻿namespace Framework.UserInterface
{
    public interface IUITickable : IGetGameObject
    {
        void OnUITick(float dt);
    }
}
