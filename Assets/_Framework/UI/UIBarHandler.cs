﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.UI;
using Txt = TMPro.TextMeshProUGUI;

namespace Framework.UserInterface
{
    public class UIBarHandler : MonoBehaviour
    {
        public enum Mode
        {
            SingleText,
            SeparateText
        }


        [InfoBox("This class handles a generic game bar, health, stamina, whatever. Needs to be hooked through code."), ReadOnly]
        public string readme = "readme";

        [SerializeField] Mode mode = Mode.SingleText;

        [SerializeField] bool unscaled;
        [SerializeField] Txt txt_value;
        [SerializeField] Txt txt_max;
        [SerializeField] Image img_bar;
        [SerializeField] float lerpMult;

        [SerializeField]
        private Color DiffColorPositive = Color.green;
        [SerializeField]
        private Color DiffColorNegative = Color.red;

        private Func<float> getterNormalized;
        private Func<float> getterValue;
        private Func<float> getterValueMax;
        private float currentNormalized = float.NegativeInfinity;
        private float current = float.NegativeInfinity;
        private Image img_diffBar;
        private bool modePositive;
        private float diffValue;

        public void Init(Func<float> getterNormalized, Func<float> getterValue = null, Func<float> getterValueMax = null)
        {
            if (getterNormalized == null || getterValue == null)
            {
                FLog.LogError(nameof(UIBarHandler), " Getter can't be null", FLogCategory.UI, this.gameObject);
            }
            this.getterNormalized = getterNormalized;
            this.getterValue = getterValue;
            this.getterValueMax = getterValueMax;
        }

        private void Start()
        {
            // Create a bar copy to display diffs
            img_diffBar = img_bar.CloneGameObject();
            img_diffBar.transform.SetSiblingIndex(img_bar.transform.GetSiblingIndex());
        }

        private void Update()
        {
            float newvalueNormalized = getterNormalized();
            float newvalue = getterValue();

            if (newvalueNormalized != currentNormalized || newvalue != current)
            {
                current = newvalue;
                float diff = newvalueNormalized - currentNormalized;
                modePositive = diff > 0;
                diffValue = Mathf.Abs(diff);
                currentNormalized = newvalueNormalized;
                img_diffBar.fillAmount = currentNormalized + diffValue;
                img_diffBar.color = modePositive ? DiffColorPositive : DiffColorNegative;

                if (getterValue != null)
                {
                    float value = getterValue();
                    float max = value;

                    if (getterValueMax != null)
                    {
                        max = getterValueMax();

                        switch (mode)
                        {
                            case Mode.SingleText:
                                txt_value.ZSetFormat("{0}/{1}", Mathf.RoundToInt(value), Mathf.RoundToInt(max));
                                break;
                            case Mode.SeparateText:
                                txt_value.ZSetFormat("{0}", Mathf.RoundToInt(value));
                                txt_max.ZSetFormat("{0}", Mathf.RoundToInt(max));
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        txt_value.ZSet(value);
                    }
                }
            }

            float time = unscaled ? Time.unscaledDeltaTime : Time.deltaTime;
            img_bar.fillAmount = Mathf.Lerp(img_bar.fillAmount, currentNormalized, time * lerpMult);

        }
    }
}
