﻿namespace Framework.UserInterface
{
    using Framework;
    using Sirenix.OdinInspector;
    using Sirenix.Serialization;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class UISimpleStateMachine
    {
        [SerializeField]
        public List<UISimpleState> states = new List<UISimpleState>();

        public UISimpleState CurrentState { get; private set; }
        protected Dictionary<UISimpleState, UISimpleState> statesMap = new Dictionary<UISimpleState, UISimpleState>();

        public void Initialize()
        {
            foreach (var state in states)
            {
                ((IInitializeable)state).DoInitialize();
                statesMap.Add(state, state);
            }
        }

        public void SetState(UISimpleState state)
        {
            if(CurrentState)
            {
                CurrentState.OnStateExit();
            }

            if (state == null)
            {
                CurrentState = null;
                return;
            }

            if(statesMap.TryGetValue(state, out state))
            {
                CurrentState = state;
                CurrentState.OnStateEnter();
            }
        }
    }
}