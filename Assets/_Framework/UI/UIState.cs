namespace Framework.UserInterface
{
    using Framework;
    using Framework.Events;
    using Framework.Internal;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UIState : MonoBehaviour, IUITickable
    {
        public UIState State { get; protected set; }
        public UIState ParentState { get; protected set; }
        public ReadOnlyList<UIState> Substates { get; protected set; }

        private Dictionary<Type, UIState> substatesMap = new Dictionary<Type, UIState>();
        private bool FirstEnter { get; set; } = true;
        private bool Initialized { get; set; }

        protected virtual void Awake()
        {
            TrySelfInit();
        }

        protected virtual void OnEnable()
        {
            TrySelfInit();

            UITicker.Register(this);

            if (!ParentState)
                OnStateEnter();
        }

        private void TrySelfInit()
        {
            if (!Initialized)
            {
                GetSubstates();
                OnInitialize();
                Initialized = true;
            }
        }

        protected virtual void OnInitialize()
        {

        }

        protected virtual void OnDisable()
        {
            UITicker.Unregister(this);
        }

        public virtual void OnUITick(float dt)
        {
        }

        protected void GetSubstates()
        {
            ParentState = gameObject.GetComponentInParentOnly<UIState>();

            Substates = new ReadOnlyList<UIState>(transform.GetComponentsInDirectChildren<UIState>());
            foreach (var item in Substates)
            {
                if (item)
                {
                    substatesMap.Add(item.GetType(), item);
                    item.gameObject.SetActive(false);
                    item.ParentState = this;
                }
            }
        }

        protected virtual void OnStateEnter()
        {
            if (FrameworkDebugSettings.Instance.DebugUI)
                FLog.Log(FLog.CategoryArrow("UI State"), "State Enter: ", this.GetType().Name, FLogCategory.UI, gameObject);

            gameObject.SetActive(true);

            if (FirstEnter)
            {
                OnFirstStateEnter();
                FirstEnter = false;
            }

            EventBus<OnUIStateEnter>.Raise(new OnUIStateEnter() { state = this });
        }

        protected virtual void OnFirstStateEnter()
        {

        }

        protected virtual void OnStateExit()
        {
            if (FrameworkDebugSettings.Instance.DebugUI)
                FLog.Log(FLog.CategoryArrow("UI State"), "State Exit: ", this.GetType().Name, FLogCategory.UI, gameObject);

            gameObject.SetActive(false);
            EventBus<OnUIStateExit>.Raise(new OnUIStateExit() { state = this });
        }

        public virtual void SetState<T>()
        {
            if (substatesMap.TryGetValue(typeof(T), out var state))
            {
                if (State)
                {
                    State.OnStateExit();
                }

                State = state;
                State.OnStateEnter();
            }
        }

        public virtual void ExitSubstates()
        {
            if (State)
            {
                State.OnStateExit();
                State = null;
            }
        }

        public virtual T GetState<T>() where T : class
        {
            if (substatesMap.TryGetValue(typeof(T), out var state))
            {
                return state as T;
            }

            return null;
        }

    }
}