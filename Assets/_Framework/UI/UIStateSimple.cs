﻿namespace Framework.UserInterface
{
    using Framework;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UISimpleState : MonoBehaviour, IInitializeable
    {
        private bool FirstEnter { get; set; } = true;
        public CanvasGroup CanvasGroup => cg;

        [SerializeField] CanvasGroup cg;


        void IInitializeable.DoInitialize()
        {
            OnInitialize();
        }

        protected virtual void OnInitialize()
        {
        }

        public virtual void OnStateEnter()
        {
            FLog.Log(FLog.CategoryArrow("UI State"), "State Enter: ", this.GetType().Name, FLogCategory.UI, gameObject);

            if (FirstEnter)
            {
                OnFirstStateEnter();
                FirstEnter = false;
            }
        }

        protected virtual void OnFirstStateEnter()
        {

        }

        public virtual void OnStateExit()
        {
            FLog.Log(FLog.CategoryArrow("UI State"), "State Exit: ", this.GetType().Name, FLogCategory.UI, gameObject);
        }
    }
}