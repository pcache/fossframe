﻿namespace Framework.UserInterface
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    /// <summary>
    /// This is a facade class for UITickerSystem, for ease of access.
    /// </summary>
    public static class UITicker
    {
        /*---------------------------------------------*/
        /* DEPENDENCIES                                                 */
        private static UITickerSystem system; 
        /*---------------------------------------------*/

        public static void Init(UITickerSystem _system)
        {
            system = _system;
        }

        public static void Register(IUITickable tickable) => system.Register(tickable);
        public static void Unregister(IUITickable tickable) => system.Unregister(tickable);
    }

    public class UITickerSystem : FrameworkSystem
    {
        protected HashSet<IUITickable> uitickables = new HashSet<IUITickable>();

        private Ticker ticker = new Ticker(0.05f, true);

        protected override void OnConstruct()
        {
            base.OnConstruct();
            UITicker.Init(this);
        }

        protected override void OnUpdate(float dt)
        {
            base.OnUpdate(dt);
            UpdateTickables(dt);
        }

        protected virtual void UpdateTickables(float dt)
        {
            if (ticker.Update())
            {
                foreach (var tickable in uitickables)
                {
                    if (tickable.gameObject.activeInHierarchy)
                        tickable.OnUITick(ticker.DeltaTime);
                }
            }
        }

        public void Register(IUITickable tickable)
        {
            uitickables.Add(tickable);
        }

        public void Unregister(IUITickable tickable)
        {
            uitickables.Remove(tickable);
        }
    }
}
