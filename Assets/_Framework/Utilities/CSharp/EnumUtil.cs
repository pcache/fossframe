﻿using Framework;
using System;

namespace Framework
{
    public static class EnumUtil
    {
        public static bool HasFlag(this int @enum, int flag)
        {
            return ((@enum & flag) == flag);
        }
    }
}
