﻿namespace Framework
{
    using System;
    using System.Collections.Generic;

    public class StringCache
    {
        private Dictionary<int, string> ints = new Dictionary<int, string>();

        private string IntFormating;

        public void SetIntFormatting(string formating)
        {
            IntFormating = formating;
        }

        public void PreallocateInts(int from, int to)
        {
            for (int i = from; i < to; i++)
            {
                ints.Add(i, i.ToString(IntFormating));
            }
        }

        public string FromInt(int number)
        {
            if (ints.TryGetValue(number, out string val))
            {
                return val;
            }

            val = number.ToString(IntFormating);
            ints.Add(number, val);
            return val;
        }
    }


}