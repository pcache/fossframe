﻿namespace Framework
{
    using System.Text;
    using System.Runtime.CompilerServices;

    public static class StringCtr
    {
        public class Builder
        {
            private StringBuilder sb = new StringBuilder();

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public Builder Clear()
            {
                sb.Clear();
                return this;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public Builder _(string s)
            {
                sb.Append(s);
                return this;
            }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public string String() => sb.ToString();
            
            public static implicit operator string(Builder b) => b.String();
        }

        private static Builder builder = new Builder();

        public static Builder New(string s)
        {
            builder.Clear();
            builder._(s);
            return builder;
        }
    }
}
