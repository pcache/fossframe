﻿namespace Framework
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class CollectionsExt
    {
        private static class Buffer<T>
        {
            public static List<T> buffer = new List<T>();
        }

        public static void AddRange<T>(this HashSet<T> set, T[] range)
        {
            for (int i = 0; i < range.Length; i++)
                set.Add(range[i]);
        }

        public static void AddRange<T>(this HashSet<T> set, IList<T> range)
        {
            for (int i = 0; i < range.Count; i++)
                set.Add(range[i]);
        }

        public static bool NullOrEmpty<T>(this List<T> list)
        {
            if (list == null || list.Count == 0)
                return true;

            return false;
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static T Random<T>(this IList<T> list)
        {
            int r = UnityEngine.Random.Range(0, list.Count);
            return list[r];
        }

        public static T Random<T>(this ReadOnlyList<T> list)
        {
            int r = UnityEngine.Random.Range(0, list.Count);
            return list[r];
        }

        public static T RandomExact<T>(this IList<T> list, Func<T, bool> predicate)
        {
            if (list == null)
                return default;

            if (predicate == null)
                return default;

            var buffer = Buffer<T>.buffer;
            buffer.Clear();
            buffer.AddRange(list);
            buffer.Shuffle();

            for (int i = 0; i < buffer.Count; i++)
            {
                if (predicate(buffer[0]))
                    return buffer[0];
            }

            return default;
        }

        public static T First<T>(this ReadOnlyList<T> list, Func<T, bool> predicate)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (predicate(list[i]))
                    return list[i];
            }
            return default;
        }

        public static T Last<T>(this ReadOnlyList<T> list, Func<T, bool> predicate)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (predicate(list[i]))
                    return list[i];
            }
            return default;
        }

        public static T RandomExact<T>(this ReadOnlyList<T> list, Func<T, bool> predicate)
        {
            if (predicate == null)
                return default;

            var buffer = Buffer<T>.buffer;
            buffer.Clear();
            buffer.AddRange(list);
            buffer.Shuffle();

            for (int i = 0; i < buffer.Count; i++)
            {
                if (predicate(buffer[i]))
                    return buffer[i];
            }

            return default;
        }
        //public static T Random<T>(this ReadOnlyList<T> list)
        //{
        //    int r = UnityEngine.Random.Range(0, list.Count);
        //    return list[r];
        //}

        public static T Random<T>(this SerializedReadOnlyList<T> list)
        {
            int r = UnityEngine.Random.Range(0, list.Count);
            return list[r];
        }

        public static ComponentList<T> ToComponentList<T>(this IEnumerable<T> enumerable) where T : Component
        {
            return new ComponentList<T>(enumerable);
        }

        public static float GetAVG(this List<float> list)
        {
            if (list == null || list.Count == 0)
                return 0;

            float total = 0;

            for (int i = 0; i < list.Count; i++)
            {
                total += list[i];
            }

            return total / list.Count;
        }

        public static (float min, float max) GetMinMax(this List<float> list)
        {
            if (list == null || list.Count == 0)
                return (0, 0);

            float min = float.MaxValue;
            float max = float.MinValue;

            for (int i = 0; i < list.Count; i++)
            {
                float v = list[i];

                if (v < min)
                    min = v;

                if (v > max)
                    max = v;
            }

            return (min, max);
        }

        ///<summary>Finds the index of the first item matching an expression in an enumerable.</summary>
        ///<param name="items">The enumerable to search.</param>
        ///<param name="predicate">The expression to test the items against.</param>
        ///<returns>The index of the first matching item, or -1 if no items match.</returns>
        public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int retVal = 0;
            foreach (var item in items)
            {
                if (predicate(item)) return retVal;
                retVal++;
            }
            return -1;
        }

        public static T Find<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            foreach (var item in items)
            {
                if (predicate(item)) return item;
            }

            return default;
        }

        ///<summary>Finds the index of the first occurrence of an item in an enumerable.</summary>
        ///<param name="items">The enumerable to search.</param>
        ///<param name="item">The item to find.</param>
        ///<returns>The index of the first matching item, or -1 if the item was not found.</returns>
        public static int IndexOf<T>(this IEnumerable<T> items, T item) { return items.FindIndex(i => EqualityComparer<T>.Default.Equals(item, i)); }
    }
}
