﻿namespace Framework
{
    using System.Collections.Generic;
    using UnityEngine;

    public class ComponentList<T> : List<T>, IEnumerable<T>
        where T : Component
    {
        public ComponentList(IEnumerable<T> collection) : base(collection) { }
        public ComponentList() : base() { }

        public void DestroyAndClear()
        {
            for (int i = 0; i < Count; i++)
            {
                var c = this[i];
                if (c)
                {
                    GameObject.Destroy(c.gameObject);
                }
            }

            Clear();
        }
    }
}
