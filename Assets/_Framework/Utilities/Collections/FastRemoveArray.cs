﻿using Framework;
using System;
using System.Collections.Generic;

namespace Framework
{
    public interface IFastRemoveArrayElement
    {
        int FastArrayIndex { get; set; }
    }

    /// <summary>
    /// Its a list, but is aimed at fast remove ops
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FastRemoveArray<T> where T : class, IFastRemoveArrayElement
    {
        public int Count => index + 1;
        private int index = -1;
        protected T[] array;

        public FastRemoveArray(int capacity = 4)
        {
            array = new T[4];
        }

        public T this[int i]
        {
            get { return array[i]; }
        }

        public void Add(T elem)
        {
            if(Count + 1 > array.Length)
            {
                Expand();
            }
            elem.FastArrayIndex = ++index;
            array[index] = elem;
        }

        public void Remove(T elem)
        {
            array[index].FastArrayIndex = elem.FastArrayIndex;
            array[elem.FastArrayIndex] = array[index];
            array[index] = default;
            elem.FastArrayIndex = -1;
        }

        private void Expand()
        {
            var narray = new T[array.Length * 2];
            Array.Copy(array, narray, array.Length);
            array = narray;
        }
    }
}
