﻿namespace Framework
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public struct LazyList<T> : IEnumerable<T>, IList<T>
    {
        public int Count => list == null ? 0 : list.Count;

        public bool IsReadOnly => false;

        List<T> list;

        public T this[int i]
        {
            get
            {
                return list == null ? default : list[i];
            }
            set
            {
                if (list == null)
                    return;

                list[i] = value;
            }
        }

        private void LazyInit()
        {
            if (list == null)
                list = new List<T>();
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (list == null)
                return null;

            return ((IEnumerable<T>)list).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (list == null)
                return null;

            return list.GetEnumerator();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int IndexOf(T item)
        {
            if (list == null)
                return -1;

            return list.IndexOf(item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Insert(int index, T item)
        {
            LazyInit();
            list.Insert(index, item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveAt(int index)
        {
            if (list == null)
                return;
            list.RemoveAt(index);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            if (list == null)
                return;

            list.Clear();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T item)
        {
            if (list == null)
                return false;
            return list.Contains(item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (list == null)
                return;

            list.CopyTo(array, arrayIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T item)
        {
            if (list == null)
                return false;

            return list.Remove(item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(T item)
        {
            LazyInit();
            list.Add(item);
        }
    }
}
