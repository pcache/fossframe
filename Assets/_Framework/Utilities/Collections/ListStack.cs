﻿using Framework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Framework
{
    /// <summary>
    /// Behaves as a stack, but allows enumeration
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListStack<T> : IEnumerable<T>
    {
        public int Count => list.Count;

        private List<T> list = new List<T>();


        public void Push(T item)
        {
            list.Insert(0, item);
        }

        public T Pop()
        {
            if (list.Count == 0)
                return default;

            int index = list.Count - 1;
            var item = list[index];
            list.RemoveAt(index);
            return item;
        }

        public void Remove(T item)
        {
            list.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }
}
