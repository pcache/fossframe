﻿using Sirenix.Serialization;

namespace Framework
{
    using Sirenix.OdinInspector;

    [System.Serializable]
    public class OptionalSerializedReadOnlyList<T> : SerializedReadOnlyList<T>
    {
        [OdinSerialize, PropertyOrder(-10), BoxGroup("List")]
        public bool Enabled { get; private set; }

        protected override bool IsEnabled() => Enabled;
    }

}
