﻿namespace Framework
{
    using System.Collections;
    using System.Collections.Generic;

    public struct ReadOnlyArray<T> : IEnumerable<T>
    {
        public int Length => array.Length;
        T[] array;

        public T this[int i]
        {
            get { return array[i]; }
        }

        public ReadOnlyArray(T[] array)
        {
            this.array = array;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return array.GetEnumerator();
        }
    }
}
