﻿
namespace Framework.Internal
{
    using System.Collections.Generic;

    public interface IReadOnlyDictionaryInternal<TKey, TValue>
    {
        Dictionary<TKey, TValue> InternalDictionary { get; }
    }
}

namespace Framework
{
    using Framework.Internal;
    using System;
    using System.Collections;
    using System.Collections.Generic;


    public struct ReadOnlyDictionary<TKey, TValue> :
        IEnumerable<KeyValuePair<TKey, TValue>>, 
        IReadOnlyDictionary<TKey, TValue>,
        IReadOnlyDictionaryInternal<TKey, TValue>
    {
        public int Count => dictionary.Count;
        Dictionary<TKey, TValue> dictionary;

        public Dictionary<TKey, TValue>.KeyCollection Keys => dictionary.Keys;
        public Dictionary<TKey, TValue>.ValueCollection Values => dictionary.Values;

        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => throw new NotImplementedException();
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => throw new NotImplementedException();
        Dictionary<TKey, TValue> IReadOnlyDictionaryInternal<TKey, TValue>.InternalDictionary => dictionary;

        public TValue this[TKey key]
        {
            get { return dictionary[key]; }
        }

        public ReadOnlyDictionary(Dictionary<TKey, TValue> source)
        {
            this.dictionary = source;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<TKey, TValue>>)dictionary).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }
    }
}
