﻿
namespace Framework.Internal
{
    using System.Collections.Generic;

    public interface IReadOnlyListInternal<T>
    {
        List<T> InternalList { get; }
    }
}

namespace Framework
{
    using Framework.Internal;
    using System.Collections;
    using System.Collections.Generic;

    public struct ReadOnlyList<T> : 
        IEnumerable<T>,
        IReadOnlyListInternal<T>
    {
        public int Count => list.Count;

        public List<T> InternalList => list;

        List<T> list;

        private static List<T> defaultList = new List<T>();

        public T this[int i]
        {
            get { return list[i]; }
        }

        public ReadOnlyList(List<T> list)
        {
            this.list = list;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (list == null)
                return ((IEnumerable<T>)defaultList).GetEnumerator();

            return ((IEnumerable<T>)list).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (list == null)
                return defaultList.GetEnumerator();

            return list.GetEnumerator();
        }
    }
}
