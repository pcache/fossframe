﻿namespace Framework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public struct ReadOnlyQueue<TArg> : IEnumerable<TArg>
    {
        public int Count => queue.Count;
        Queue<TArg> queue;

        public ReadOnlyQueue(Queue<TArg> source)
        {
            this.queue = source;
        }

        public TArg Dequeue()
        {
            return queue.Dequeue();
        }

        public void Enqueue(TArg arg)
        {
            queue.Enqueue(arg);
        }

        public IEnumerator<TArg> GetEnumerator()
        {
            return ((IEnumerable<TArg>)queue).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return queue.GetEnumerator();
        }
    }
}
