﻿using Sirenix.Serialization;

namespace Framework
{
    using Sirenix.OdinInspector;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class SerializedReadOnlyListTable<T> : IEnumerable<T>
    {
        public int Count => list.Count;

        [SerializeField, OdinSerialize, HideLabel, ShowIf("$IsEnabled"), BoxGroup("List", ShowLabel = false)]
        protected List<T> list = new List<T>();

        public T this[int i] => list[i];
        public SerializedReadOnlyListTable() { }
        public SerializedReadOnlyListTable(List<T> list) => this.list = list;
        public SerializedReadOnlyListTable(T[] arr) => this.list = new List<T>(arr);
        public IEnumerator<T> GetEnumerator() => ((IEnumerable<T>)list).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => list.GetEnumerator();
        public List<T> GetListClone() => list.Clone();
        public List<T> GetInternalList() => list;

        protected virtual bool IsEnabled() { return true; }
    }
}
