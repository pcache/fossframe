﻿using Sirenix.Serialization;

namespace Framework
{
    using Sirenix.OdinInspector;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class SerializedReadOnlyListTypefilter<T, Filter> : IEnumerable<T>
    {
        public int Count => list.Count;

        [SerializeField, OdinSerialize, HideLabel, ShowIf("$IsEnabled"), BoxGroup("List", ShowLabel = false), TypeFilter(nameof(GetValidTypes))]
        protected List<T> list = new List<T>();

        public T this[int i] => list[i];
        public SerializedReadOnlyListTypefilter() { }
        public SerializedReadOnlyListTypefilter(List<T> list) => this.list = list;
        public SerializedReadOnlyListTypefilter(T[] arr) => this.list = new List<T>(arr);
        public IEnumerator<T> GetEnumerator() => ((IEnumerable<T>)list).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => list.GetEnumerator();
        public List<T> GetListClone() => list.Clone();
        public List<T> GetInternalList() => list;

        protected virtual bool IsEnabled() { return true; }

        protected virtual bool UseTypeFilter => false;
        protected virtual Type GetTypeFilter() => typeof(Filter);
        protected virtual IEnumerable<Type> GetValidTypes()
        {
            var filtered = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.userAndPlugins, GetTypeFilter());
            return filtered;
        }
    }
}
