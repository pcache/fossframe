﻿namespace Framework
{
    using Framework;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class TypeMapFilter
    {
        public HashSet<Type> terminators { get; private set; } = new HashSet<Type>();
        public HashSet<Type> whitelist { get; private set; } = new HashSet<Type>();
        public HashSet<Type> blacklist { get; private set; } = new HashSet<Type>();

        public void RegisterTerminator(Type terminatorType)
        {
            this.terminators.Add(terminatorType);
        }

        public TypeMapFilter(List<Type> terminators, List<Type> whitelist, List<Type> blacklist)
        {
            this.terminators.AddRange(terminators);
            this.whitelist.AddRange(whitelist);
            this.blacklist.AddRange(blacklist);
        }
    }


    /// <summary>
    /// This class allows resolution of a reference to a specific object by any valid type in its 
    /// inheritance chain. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypeMap
    {
        private Dictionary<object, List<Type>> chains = new Dictionary<object, List<Type>>();
        private Dictionary<Type, object> refsByType = new Dictionary<Type, object>();
        private List<Type> typeBuffer = new List<Type>();
        private TypeMapFilter filter;

        public TypeMap(TypeMapFilter filter)
        {
            this.filter = filter;
        }

        public void Add(object value)
        {
            // Get the type chain up until the terminator
            if (ReflectionUtil.GetTypeChainComplex(
                 value.GetType(),
                 filter.terminators,
                 filter.whitelist,
                 filter.blacklist,
                 typeBuffer))
            {
                // store the chain
                chains.Add(value, typeBuffer.Clone());

                // map instance to every type in the hierarchy 
                for (int i = 0; i < typeBuffer.Count; i++)
                {
                    refsByType.Add(typeBuffer[i], value);
                }

            }

            typeBuffer.Clear();
        }

        public object Get(Type type)
        {
            if (refsByType.TryGetValue(type, out var value))
                return value;
            return default;
        }

        public T Get<T>()
        {
            return (T)Get(typeof(T));
        }

        public bool TryGetValue<T>(out T value)
        {
            value = Get<T>();
            if (value != null)
                return true;
            return false;
        }
    }
}
