﻿using Framework;
using System;
using System.Collections.Generic;

namespace Framework
{
    /// <summary>
    /// Ensures uniqueness of each item.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UniqueStack<T> 
    {
        private ListStack<T> stack = new ListStack<T>();
        private HashSet<T> set = new HashSet<T>();

        public int Count => stack.Count;

        public bool Push(T item)
        {
            if (item == null)
            {
                FLog.LogError(nameof(UniqueStack<T>), " argument is null.");
                return false;
            }

            if(set.Contains(item))
            {
                FLog.LogError(nameof(UniqueStack<T>), " already contains item.");
                return false;
            }

            set.Add(item);
            stack.Push(item);
            return true;
        }

        public bool Contains(T item)
        {
            return set.Contains(item);
        }

        public void Remove(T item)
        {
            stack.Remove(item);
            set.Remove(item);
        }

        public T Pop()
        {
            if(stack.Count == 0)
            {
                FLog.LogError(nameof(UniqueStack<T>), " stack is empty.");
                return default;
            }

            var item = stack.Pop();
            set.Remove(item);
            return item;
        }
    }
}
