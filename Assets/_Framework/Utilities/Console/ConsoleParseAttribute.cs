﻿namespace Framework.Console
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ConsoleParseAttribute : Attribute
    {
    }

}