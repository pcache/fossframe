﻿namespace Framework
{
    using System;

    public class EventBinding<T> where T : struct, IEvent
    {
        private bool _listen;
        public bool Listen
        {
            get => _listen;
            set
            {
                if (value)
                    EventBus<T>.Register(this);
                else
                    EventBus<T>.Unregister(this);

                _listen = value;
            }
        }

        public int InternalIndex { get; set; } = -1;

        public Action<T> onEvent;
        public Action onEventNoArg;

        public EventBinding(Action<T> onEvent, bool listen = true)
        {
            this.onEvent = onEvent;
            this.Listen = listen;
        }

        public EventBinding(Action onEventNoArgs, bool listen = true)
        {
            this.onEventNoArg = onEventNoArgs;
            this.Listen = listen;
        }

        public static implicit operator EventBinding<T>(Action onEventNoArgs)
        {
            return new EventBinding<T>(onEventNoArgs);
        }

        public static implicit operator EventBinding<T>(Action<T> onEvent)
        {
            return new EventBinding<T>(onEvent);
        }
    } 
}