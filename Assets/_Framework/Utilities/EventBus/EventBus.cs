namespace Framework
{
    using System;
    using System.Reflection;
    using System.Collections.Generic;
    using UnityEngine;

    public static class EventBusUtility
    {
        public static void Init()
        {
            var types =  ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user);
            var typedef = typeof(EventBus<>);

            for (int i = 0; i < types.Count; i++)
            {
                var type = types[i];

                if ((typeof(IEvent)) != type && (typeof(IEvent)).IsAssignableFrom(type))
                {
                    var gentype = typedef.MakeGenericType(type);
                    gentype.GetMethod("Clear", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null,null);
                }

            }
        }
    }

    public static class EventBus<T> where T : struct, IEvent 
    {
        private static EventBinding<T>[] bindings = new EventBinding<T>[64];
        private static int count;

        private static void Clear()
        {
            bindings = new EventBinding<T>[64];
            count = 0;
        }

        public static void Register(EventBinding<T> binding)
        {
            if (bindings.Length <= count)
            {
                EventBinding<T>[] newarray = new EventBinding<T>[bindings.Length * 2];
                Array.Copy(bindings, newarray, bindings.Length);
                bindings = newarray;
            }

            binding.InternalIndex = count;
            bindings[count] = binding;

            count += 1;
        }

        public static void Unregister(EventBinding<T> binding)
        {
            int index = binding.InternalIndex;

            if (index == -1 || index > count)
            {
                // binding invalid
                return;
            }

            if (bindings[index] != binding)
            {
                // binding invalid
                return;
            }

            int lastIndex = count - 1;
            var last = bindings[lastIndex];

            bindings[index] = last;
            bindings[lastIndex] = null;

            last.InternalIndex = index;
            binding.InternalIndex = -1;

            count -= 1;
        }

        public static void Raise()
        {
            Raise(default);
        }

        public static void Raise(T ev)
        {
            for (int i = 0; i < count; i++)
            {
                bindings[i].onEvent?.Invoke(ev);
                bindings[i].onEventNoArg?.Invoke();
            }
        }

        public static string GetDebugInfoString()
        {
            return "Bindings: " + count + " BufferSize: " + bindings.Length;
        }


    } 
}