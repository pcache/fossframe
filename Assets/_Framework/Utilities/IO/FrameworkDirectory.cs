﻿namespace Framework.Internal
{
    using System;
    using System.IO;

    public class FrameworkDirectory
    {
        public string Path { get; protected set; }

        public FrameworkDirectory()
        {
        }

        public FrameworkDirectory(string path)
        {
            this.Path = path;
            EnsureDirectory(path);
        }

        protected void EnsureDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
