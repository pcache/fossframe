﻿using Framework;
using System;
using System.IO;
using System.Text;

namespace Framework
{
    public static class IOExt
    {
        public static void CopyDirRecursive(string source, string target)
        {
            DirectoryInfo diSource = new DirectoryInfo(source);
            DirectoryInfo diTarget = new DirectoryInfo(target);

            CopyAll(diSource, diTarget);
        }

        private static StringBuilder sb = new StringBuilder();

        private static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            sb.Clear();
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                sb.AppendLine($@"Copying {target.FullName}\{fi.Name}");
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }

            FLog.Log(sb.ToString());
        }
    }
}
