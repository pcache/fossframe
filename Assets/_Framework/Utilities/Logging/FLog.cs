﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cysharp.Text;

public enum FLogCategory
{
    None,
    Game,
    UI,
    Graphics,
    Data,
    Editor,
    Framework,
    Core,
    Steam
}

public enum FLogColor
{
    black,
    blue,
    brown,
    cyan,
    darkblue,
    green,
    grey,
    lightblue,
    line,
    magenta,
    maroon,
    navy,
    olive,
    orange,
    purple,
    red,
    silver,
    teal,
    white,
    yellow,
}

public class FLog
{
    public const char ARROW = '►';
    public const FLogColor ColorCrawl = FLogColor.yellow;

    private static string[] colorStrings = new string[]
    {
            "<color=black>",
            "<color=blue>",
            "<color=brown>",
            "<color=cyan>",
            "<color=darkblue>",
            "<color=green>",
            "<color=grey>",
            "<color=lightblue>",
            "<color=line>",
            "<color=magenta>",
            "<color=#bc0000>",
            "<color=navy>",
            "<color=#d4c17e>",
            "<color=orange>",
            "<color=purple>",
            "<color=red>",
            "<color=silver>",
            "<color=teal>",
            "<color=white>",
            "<color=yellow>",
    };


    private static Utf8ValueStringBuilder stringBuilder = ZString.CreateUtf8StringBuilder();
    private static Utf8ValueStringBuilder coloredSb = ZString.CreateUtf8StringBuilder();
    private static Utf8ValueStringBuilder categoryArrowSB = ZString.CreateUtf8StringBuilder();

    private static bool LogEnabled = true;
    private static bool LogTime = false;

    public static void Log(object message, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message, null, null, category, context);
    }

    public static void Log(object message1, object message2, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, null, category, context);
    }

    public static void Log(object message1, object message2, object message3, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, message3, category, context);
    }

    public static void LogWarning(object message, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message, null, null, category, context, UnityEngine.LogType.Warning);
    }

    public static void LogWarning(object message1, object message2, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, null, category, context, UnityEngine.LogType.Warning);
    }

    public static void LogWarning(object message1, object message2, object message3, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, message3, category, context, UnityEngine.LogType.Warning);
    }

    public static void LogError(object message, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message, null, null, category, context, UnityEngine.LogType.Error);
    }

    public static void LogError(object message1, object message2, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, null, category, context, UnityEngine.LogType.Error);
    }

    public static void LogError(object message1, object message2, object message3, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null)
    {
        LogInternal(message1, message2, message3, category, context, UnityEngine.LogType.Error);
    }

    private static void LogInternal(object message1, object message2, object message3, FLogCategory category = FLogCategory.None, UnityEngine.Object context = null, UnityEngine.LogType logType = UnityEngine.LogType.Log)
    {
        if (!LogEnabled)
            return;

        if (message1 == null)
            return;

        stringBuilder.Clear();

        AppendCategory(category);

        if (LogTime)
            AppendTimeStamp();

        stringBuilder.Append(message1);
        stringBuilder.Append(' ');

        if (message2 != null)
        {
            stringBuilder.Append(message2);
            stringBuilder.Append(' ');
        }

        if (message3 != null)
        {
            stringBuilder.Append(message3);
            stringBuilder.Append(' ');
        }

        switch (logType)
        {
            case UnityEngine.LogType.Error:
                UnityEngine.Debug.LogError(stringBuilder.ToString(), context);
                break;
            case UnityEngine.LogType.Assert:
                UnityEngine.Debug.LogAssertion(stringBuilder.ToString(), context);
                break;
            case UnityEngine.LogType.Warning:
                UnityEngine.Debug.LogWarning(stringBuilder.ToString(), context);
                break;
            case UnityEngine.LogType.Log:
                UnityEngine.Debug.Log(stringBuilder.ToString(), context);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Cache this
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static string CategoryArrow(string name)
    {
        categoryArrowSB.Clear();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        categoryArrowSB.Append(' ');
        categoryArrowSB.Append("<b>");
        categoryArrowSB.Append(name);
        categoryArrowSB.Append(' ');
        categoryArrowSB.Append(ARROW);
        categoryArrowSB.Append("</b>");
        categoryArrowSB.Append(' ');
#else
        categoryArrowSB.Append(' ');
        categoryArrowSB.Append(name);
        categoryArrowSB.Append(' ');
        categoryArrowSB.Append(ARROW);
        categoryArrowSB.Append(' ');
#endif
        return categoryArrowSB.ToString();
    }

    public static string GetColored(string message, FLogColor logColor)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        coloredSb.Clear();

        string color = colorStrings[(int)logColor];
        coloredSb.Append(color);
        coloredSb.Append(message);
        coloredSb.Append("</color>");
        return coloredSb.ToString();
#else
        return message;
#endif
    }

    private static void AppendCategory(FLogCategory cat)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        switch (cat)
        {
            case FLogCategory.None:
                stringBuilder.Append("<color=gray><b>NONE ►</b> </color>");
                break;
            case FLogCategory.Game:
                stringBuilder.Append("<color=orange><b>GAME ►</b> </color>");
                break;
            case FLogCategory.UI:
                stringBuilder.Append("<color=c949ff><b>UI ►</b> </color>");
                break;
            case FLogCategory.Graphics:
                stringBuilder.Append("<color=49b0ff><b>GFX ►</b> </color>");
                break;
            case FLogCategory.Data:
                stringBuilder.Append("<color=bdff49><b>DATA ►</b> </color>");
                break;
            case FLogCategory.Editor:
                stringBuilder.Append("<color=7fb6d2><b>EDITOR ►</b> </color>");
                break;
            case FLogCategory.Framework:
                stringBuilder.Append("<color=c61a05><b>FRAMEWORK ►</b> </color>");
                break;
            default:
                break;
        }
#else
        switch (cat)
        {
            case FLogCategory.None:
                stringBuilder.Append("NONE ►");
                break;
            case FLogCategory.Game:
                stringBuilder.Append("GAME ►");
                break;
            case FLogCategory.UI:
                stringBuilder.Append("UI ►");
                break;
            case FLogCategory.Graphics:
                stringBuilder.Append("GFX ►");
                break;
            case FLogCategory.Data:
                stringBuilder.Append("DATA ►");
                break;
            case FLogCategory.Editor:
                stringBuilder.Append("EDITOR ►");
                break;
            case FLogCategory.Framework:
                stringBuilder.Append("FRAMEWORK ►");
                break;
            default:
                break;
        }
#endif
    }

    private static void AppendTimeStamp()
    {
        float timeSinceStart = UnityEngine.Time.time;
        float timeRealtime = UnityEngine.Time.unscaledTime;

        stringBuilder.Append('<');
        stringBuilder.Append("T:");
        stringBuilder.Append(timeSinceStart);
        stringBuilder.Append(',');
        stringBuilder.Append("R:");
        stringBuilder.Append(timeRealtime);
        stringBuilder.Append('>');
        stringBuilder.Append(' ');
    }

    private static void AppendBracketed(string s)
    {
        stringBuilder.Append('[');
        stringBuilder.Append(s);
        stringBuilder.Append(']');
    }
}
