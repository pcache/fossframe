﻿
namespace Framework
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class MathExt
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Sqr(this float val) => val * val;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double Sqr(this double val) => val * val;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Sqr(this int val) => val * val;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float MoveTowardsInlined(float current, float target, float maxDelta)
        {
            bool flag = Math.Abs(target - current) <= maxDelta;
            float result;
            if (flag)
            {
                result = target;
            }
            else
            {
                result = current + Math.Sign(target - current) * maxDelta;
            }
            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool InRange(this float val, float min, float max)
        {
            return val >= min && val <= max;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool InRange(this int val, int min, int max)
        {
            return val >= min && val <= max;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsCloseTo(this Vector3 v3, Vector3 target)
        {
            return Vector3.Distance(v3, target) < 0.1f;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Remap(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Remap01(this float value, float from1, float to1)
        {
            return (value - from1) / (to1 - from1) * (1f - 0f) + 0f;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double Remap(this double value, double from1, double to1, double from2, double to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float WrapNumber(float value, float min, float max)
        {
            float result;
            if (value < min)
                result = max - (min - value) % (max - min);
            else
                result = min + (value - min) % (max - min);

            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Rotate(this Vector3 direction, float? x = null, float? y = null, float? z = null)
        {
            return Quaternion.Euler(
                x.HasValue ? x.Value : 0F,
                y.HasValue ? y.Value : 0F,
                z.HasValue ? z.Value : 0F) * direction;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToDirection(this float angle)
        {
            return new Vector3(0, Mathf.Sin(Mathf.Deg2Rad * angle), Mathf.Cos(Mathf.Deg2Rad * angle));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 With(this Vector3 v, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x.HasValue ? x.Value : v.x,
                               y.HasValue ? y.Value : v.y,
                               z.HasValue ? z.Value : v.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Add(this Vector3 v, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x.HasValue ? v.x + x.Value : v.x,
                               y.HasValue ? v.y + y.Value : v.y,
                               z.HasValue ? v.z + z.Value : v.z);
        }

        /// <summary>
        /// Negative values to subtract
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 AddFloats(this Vector3 vec, float x, float y, float z)
        {
            vec.x += x;
            vec.y += y;
            vec.z += z;
            return vec;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 AddFloats(this Vector3 vec, float val)
        {
            return AddFloats(vec, val, val, val);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 MultiplyFloats(this Vector3 vec, float? x = null, float? y = null, float? z = null)
        {
            if (x.HasValue)
                vec.x *= x.Value;
            if (y.HasValue)
                vec.y *= y.Value;
            if (z.HasValue)
                vec.z *= z.Value;
            return vec;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 MultiplyByFloat(this Vector3 vec, float x)
        {
            vec.x *= x;
            vec.y *= x;
            vec.z *= x;
            return vec;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 DivideFloats(this Vector3 vec, float x, float y, float z)
        {
            if (x == 0 || y == 0 || z == 0)
            {
                Debug.LogError("Top lel.");
            }
            vec.x /= x;
            vec.y /= y;
            vec.z /= z;
            return vec;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 GetPointBetween(Vector3 a, Vector3 b)
        {
            return (a + b) / 2f;
        }


        /// <summary>
        /// Imagine two circles inside each other, this limits vector to only area in between the two
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Vector3 LimitRadiusArea(this Vector3 vec, float min, float max)
        {
            vec.x = _limiradiusArea(vec.x, max, min);
            vec.y = _limiradiusArea(vec.y, max, min);
            vec.z = _limiradiusArea(vec.z, max, min);
            return vec;
        }
        private static float _limiradiusArea(float _in, float max, float min)
        {
            if (_in > 0)
            {
                if (_in > max)
                    _in = max;
                if (_in < min)
                    _in = min;
            }
            else
            {
                if (_in < max * -1)
                    _in = max * -1;
                if (_in > min * -1)
                    _in = min * -1;
            }
            return _in;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(
                Vector3.Dot(n, Vector3.Cross(v1, v2)),
                Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 CastV3(this Vector2 v2)
        {
            return new Vector3(v2.x, v2.y, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ToV3Grid(this Vector2 v2)
        {
            return new Vector3(v2.x, 0, v2.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 ToV2Grid(this Vector3 v3)
        {
            return new Vector2(v3.x, v3.z);
        }

        /// <summary>
        ///  This is not very optimized. There are at least n + 1 and at most 2n Vector3.Distance
        ///  calls (where n is the number of waypoints). </summary>
        /// <param name="waypoints"></param>
        /// <param name="ratio"></param>
        /// <returns></returns>
        public static Vector3 MultiLerp(Vector3[] waypoints, float ratio)
        {
            Vector3 position = Vector3.zero;
            float totalDistance = waypoints.MultiDistance();
            float distanceTravelled = totalDistance * ratio;

            int indexLow = GetVectorIndexFromDistanceTravelled(waypoints, distanceTravelled);
            int indexHigh = indexLow + 1;

            // we're done
            if (indexHigh > waypoints.Length - 1)
                return waypoints[waypoints.Length - 1];


            // calculate the distance along this waypoint to the next
            Vector3[] completedWaypoints = new Vector3[indexLow + 1];

            for (int i = 0; i < indexLow + 1; i++)
            {
                completedWaypoints[i] = waypoints[i];
            }

            float distanceCoveredByPreviousWaypoints = completedWaypoints.MultiDistance();
            float distanceTravelledThisSegment = distanceTravelled - distanceCoveredByPreviousWaypoints;
            float distanceThisSegment = Vector3.Distance(waypoints[indexLow], waypoints[indexHigh]);

            float currentRatio = distanceTravelledThisSegment / distanceThisSegment;
            position = Vector3.Lerp(waypoints[indexLow], waypoints[indexHigh], currentRatio);

            return position;
        }

        public static float MultiDistance(this Vector3[] waypoints)
        {
            float distance = 0f;

            for (int i = 0; i < waypoints.Length; i++)
            {
                if (i + 1 > waypoints.Length - 1)
                    break;

                distance += Vector3.Distance(waypoints[i], waypoints[i + 1]);
            }

            return distance;
        }

        public static int GetVectorIndexFromDistanceTravelled(Vector3[] waypoints, float distanceTravelled)
        {
            float distance = 0f;

            for (int i = 0; i < waypoints.Length; i++)
            {
                if (i + 1 > waypoints.Length - 1)
                    return waypoints.Length - 1;

                float segmentDistance = Vector3.Distance(waypoints[i], waypoints[i + 1]);

                if (segmentDistance + distance > distanceTravelled)
                {
                    return i;
                }

                distance += segmentDistance;
            }

            return waypoints.Length - 1;
        }
    }
}