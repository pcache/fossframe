﻿namespace Framework
{
    using UnityEngine;

    public static class Pooling
    {
        public static GameObject Instantiate(GameObject go, Transform parent = null)
        {
            return GameObject.Instantiate(go, parent);
        }

        public static T Instantiate<T>(T go, Transform parent = null) where T : UnityEngine.Object
        {
            return GameObject.Instantiate(go, parent);
        }
    }
}