﻿namespace Framework
{
    using System.Collections.Generic;
    using System;
    using System.Reflection;
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor; 
#endif

    public static class ReflectedTypeStore
    {
        private static List<Type> _unityUserTypes;
        private static List<Type> _unityPluginsTypes;
        private static List<Type> _unityUserAndPluginsTypes;

        [Flags]
        public enum UnityAssembly
        {
            user = 2,
            plugins = 4,
            userAndPlugins = user | plugins
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnAssemblyReload()
        {
            Initialize();
        }

#if UNITY_EDITOR
        [InitializeOnLoadMethod]
        private static void HandleDevMode()
        {
            Initialize();
        }
#endif
        /// <summary>
        /// Caches all relevant types
        /// </summary>
        private static void Initialize()
        {
            var allassemblies = GetDomainAssemblies();

            bool getasmb(string name, out List<Type> list)
            {
                if (allassemblies.TryGetValue(name, out Assembly asmb))
                {
                    list = new List<Type>(asmb.GetTypes());
                    return true;
                }

                list = new List<Type>();
                return false;
            }

            getasmb("Assembly-CSharp", out _unityUserTypes);
            getasmb("Assembly-CSharp-firstpass", out _unityPluginsTypes);

            _unityUserAndPluginsTypes = new List<Type>(_unityUserTypes);
            _unityUserAndPluginsTypes.AddRange(_unityPluginsTypes);
        }

        public static void TryInit()
        {
            if(_unityPluginsTypes == null)
            {
                Initialize();
            }
        }

        public static List<Type> GetUnityTypes(UnityAssembly assembly, Type filterBySuperOrInterface = null, bool excludeAbstract = true, bool includeItself = true)
        {
            List<Type> filtered = new List<Type>();

            if (filterBySuperOrInterface != null)
            {
                switch (assembly)
                {
                    case UnityAssembly.user:
                        return GetFiltered(_unityUserTypes);
                    case UnityAssembly.plugins:
                        return GetFiltered(_unityPluginsTypes);
                    case UnityAssembly.userAndPlugins:
                        return GetFiltered(_unityUserAndPluginsTypes);
                    default:
                        break;
                }
            }

            void TryAdd(Type type)
            {
                if (excludeAbstract && type.IsAbstract)
                    return;

                filtered.Add(type);
            }

            List<Type> GetFiltered(List<Type> list)
            {
                if (filterBySuperOrInterface == null)
                    return list.Clone();

                filtered.Clear();

                if (includeItself)
                    filtered.Add(filterBySuperOrInterface);

                for (int i = 0; i < list.Count; i++)
                {
                    var item = list[i];
                    if (filterBySuperOrInterface.IsInterface && filterBySuperOrInterface != item && filterBySuperOrInterface.IsAssignableFrom(item))
                    {
                        TryAdd(item);
                    }
                    else
                    {
                        if (item.IsSubclassOf(filterBySuperOrInterface))
                        {
                            TryAdd(item);
                        }
                    }
                }

                return filtered.Clone();
            }

            return _unityUserTypes.Clone();
        }

        private static List<Type> GetListByAsmbEnum(UnityAssembly assembly)
        {
            switch (assembly)
            {
                case UnityAssembly.user:
                    return _unityUserTypes;
                case UnityAssembly.plugins:
                    return _unityPluginsTypes;
                case UnityAssembly.userAndPlugins:
                    return _unityUserAndPluginsTypes;
                default:
                    break;
            }

            return null;
        }

        private static Dictionary<string, Assembly> GetDomainAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Dictionary<string, Assembly> map = new Dictionary<string, Assembly>();

            for (int i = 0; i < assemblies.Length; i++)
            {
                var asmb = assemblies[i];
                map.Add(asmb.GetName().Name, asmb);
            }

            return map;
        }
    }
}
