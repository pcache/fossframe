﻿namespace Framework
{
#if UNITY_EDITOR
    using UnityEditor;
#endif

    using System;
    using System.Reflection;
    using System.Collections.Generic;
    using UnityEngine;

    public static class ReflectionUtil
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void UnityInit()
        {
            genericTypeCache_arg1.Clear();
        }


        private static Dictionary<Type, Dictionary<Type, Type>> genericTypeCache_arg1 = new Dictionary<Type, Dictionary<Type, Type>>();


        private const BindingFlags flags_static_property =
                BindingFlags.Static |
                BindingFlags.GetProperty |
                BindingFlags.Public |
                BindingFlags.NonPublic |
                BindingFlags.SetProperty;

        private const BindingFlags flags_static_field =
                BindingFlags.Static |
                BindingFlags.Public |
                BindingFlags.NonPublic;

        private const BindingFlags flags_property =
                BindingFlags.Instance |
                BindingFlags.GetProperty |
                BindingFlags.Public |
                BindingFlags.NonPublic |
                BindingFlags.SetProperty | 
                BindingFlags.FlattenHierarchy;

        private const BindingFlags flags_field =
                BindingFlags.Instance |
                BindingFlags.Public |
                BindingFlags.NonPublic;

        public static bool IsStatic(this Type t) => t.IsAbstract && t.IsSealed;

        public class ObjectInfo
        {
            public ReadOnlyList<PropertyInfo> Properties { get; private set; }
            public ReadOnlyList<FieldInfo> Fields { get; private set; }
            public ReadOnlyList<MethodInfo> Methods { get; private set; }

            public ReadOnlyDictionary<string, PropertyInfo> PropertiesMap { get; private set; }
            public ReadOnlyDictionary<string, FieldInfo> FieldsMap { get; private set; }
            public ReadOnlyDictionary<string, MethodInfo> MethodsMap { get; private set; }

            public ObjectInfo(Type type, bool includePrivate)
            {
                if(type == null)
                {
                    FLog.LogError(FLog.CategoryArrow(nameof(ObjectInfo)), " Provided type is null.");
                    return;
                }

                var flags = BindingFlags.Instance | BindingFlags.Public;

                if (includePrivate)
                    flags |= BindingFlags.NonPublic;

                var props = type.GetProperties(flags);
                var fields = type.GetFields(flags);
                var methods = type.GetMethods(flags);

                Properties = new ReadOnlyList<PropertyInfo>(new List<PropertyInfo>(props));
                Fields = new ReadOnlyList<FieldInfo>(new List<FieldInfo>(fields));
                Methods = new ReadOnlyList<MethodInfo>(new List<MethodInfo>(methods));

                Dictionary<string, PropertyInfo> propmap = new Dictionary<string, PropertyInfo>();
                Dictionary<string, FieldInfo> fieldmap = new Dictionary<string, FieldInfo>();
                Dictionary<string, MethodInfo> methodmap = new Dictionary<string, MethodInfo>();

                PropertiesMap = new ReadOnlyDictionary<string, PropertyInfo>(propmap);
                FieldsMap = new ReadOnlyDictionary<string, FieldInfo>(fieldmap);
                MethodsMap = new ReadOnlyDictionary<string, MethodInfo>(methodmap);

                foreach (var p in Properties)
                    propmap.Add(p.Name, p);

                foreach (var f in Fields)
                    fieldmap.Add(f.Name, f);

                foreach (var m in Methods)
                    methodmap.Add(m.Name, m);
            }
        }

        public static void InitializeProperties<TPropType>(this object target, bool includeSubclasses, Func<PropertyInfo, TPropType> customCtor)
        {
            var type = target.GetType();
            var props = type.GetPropertiesInfo<TPropType>(includeSubclasses);

            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    if (customCtor != null)
                    {
                        prop.SetValue(target, customCtor(prop));
                    }
                    else
                    {
                        prop.SetValue(target, Activator.CreateInstance(prop.PropertyType));
                    }
                }
            }
        }

        public static void Static_InitializeProperties<TPropType>(Type targetStaticType, bool includeSubclasses, Func<PropertyInfo, TPropType> customCtor) where TPropType : new()
        {
            var props = Static_GetPropertiesInfo<TPropType>(targetStaticType, includeSubclasses);

            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    if (customCtor != null)
                    {
                        prop.SetValue(null, customCtor(prop));
                    }
                    else
                    {
                        prop.SetValue(null, new TPropType());
                    }
                }
            }
        }

        public static List<PropertyInfo> GetPropertiesInfo<TPropType>(this Type targetType, bool includeSubclasses)
        {
            if (IsStatic(targetType))
            {
                FLog.LogError(FLog.CategoryArrow("ReflectionUtil : is a static type"));
                return null;
            }

            List<PropertyInfo> list = new List<PropertyInfo>();

            var props = targetType.GetProperties(flags_property);

            for (int i = 0; i < props.Length; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    list.Add(prop);
                }
            }

            return list;
        }

        public static List<TPropType> GetPropertyValuesOfType<TPropType>(this object target, bool includeSubclasses)
        {
            var type = target.GetType();
            var props = GetPropertiesInfo<TPropType>(type, includeSubclasses);
            var list = new List<TPropType>();

            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    object value = prop.GetValue(target);
                    if (value != null)
                    {
                        list.Add((TPropType)value);
                    }
                }
            }

            return list;
        }

        public static List<PropertyInfo> Static_GetPropertiesInfo<TPropType>(Type targetStaticType, bool includeSubclasses)
        {
            if (!IsStatic(targetStaticType))
            {
                FLog.LogError(FLog.CategoryArrow("ReflectionUtil : not a static type"));
                return null;
            }

            List<PropertyInfo> list = new List<PropertyInfo>();

            var props = targetStaticType.GetProperties(flags_static_property);

            for (int i = 0; i < props.Length; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    list.Add(prop);
                }
            }

            return list;
        }

        public static List<FieldInfo> Static_GetFieldsInfo<TFieldType>(Type targetStaticType, bool includeSubclasses)
        {
            if (!IsStatic(targetStaticType))
            {
                FLog.LogError(FLog.CategoryArrow("ReflectionUtil : not a static type"));
                return null;
            }

            List<FieldInfo> list = new List<FieldInfo>();

            var fields = targetStaticType.GetFields(flags_static_field);

            for (int i = 0; i < fields.Length; i++)
            {
                var field = fields[i];

                if (field.IsValidField<TFieldType>(includeSubclasses))
                {
                    list.Add(field);
                }
            }

            return list;
        }

        public static List<TPropType> Static_GetPropertyValuesOfType<TPropType>(Type targetStaticType, bool includeSubclasses)
        {
            var props = Static_GetPropertiesInfo<TPropType>(targetStaticType, includeSubclasses);
            var list = new List<TPropType>();

            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];

                if (prop.IsValidProperty<TPropType>(includeSubclasses))
                {
                    object value = prop.GetValue(null);
                    if (value != null)
                    {
                        list.Add((TPropType)value);
                    }
                }
            }

            return list;
        }

        public static List<TFieldType> Static_GetFieldValuesOfType<TFieldType>(Type targetStaticType, bool includeSubclasses)
        {
            if (!IsStatic(targetStaticType))
            {
                FLog.LogError(FLog.CategoryArrow("ReflectionUtil : not a static type"));
                return null;
            }

            var fields = Static_GetFieldsInfo<TFieldType>(targetStaticType, includeSubclasses);
            List<TFieldType> list = new List<TFieldType>();

            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];

                if (field.IsValidField<TFieldType>(includeSubclasses))
                {
                    object value = field.GetValue(null);
                    if (value != null)
                    {
                        list.Add((TFieldType)value);
                    }
                }
            }

            return list;
        }

        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        private static bool IsValidProperty<TPropType>(this PropertyInfo info, bool includeSubclasses)
        {
            bool valid = false;
            valid |= info.PropertyType == typeof(TPropType);
            valid |= info.PropertyType.IsSubclassOf(typeof(TPropType));
            valid |= typeof(TPropType).IsAssignableFrom(info.PropertyType);
            return valid;
        }

        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
        private static bool IsValidField<TFieldType>(this FieldInfo info, bool includeSubclasses)
        {
            bool valid = false;
            valid |= info.FieldType == typeof(TFieldType);
            valid |= info.FieldType.IsSubclassOf(typeof(TFieldType));
            valid |= typeof(TFieldType).IsAssignableFrom(info.FieldType);

            return valid;
        }

        /// <summary>
        /// Convenience method. Automatically sets the target as dirty in unity editor.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="asType"></param>
        public static void SetPropertyReflection(this object target, string name, object value, Type asType = null)
        {
            var type = asType != null ? asType : target.GetType();
            var property = type.GetProperty(name, flags_property);
            if (property != null)
            {
                property.SetValue(target, value);
            }

#if UNITY_EDITOR
            if (type.IsSubclassOf(typeof(UnityEngine.Object)))
                EditorUtility.SetDirty(target as UnityEngine.Object);
#endif
        }


        /// <summary>
        /// Convenience method. Automatically sets the target as dirty in unity editor.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="asType"></param>
        public static void SetFieldReflection(this object target, string name, object value, Type asType = null)
        {
            var type = asType != null ? asType : target.GetType();
            var field = type.GetField(name, flags_field);
            if (field != null)
            {
                field.SetValue(target, value);
            }

#if UNITY_EDITOR
            if (type.IsSubclassOf(typeof(UnityEngine.Object)))
                EditorUtility.SetDirty(target as UnityEngine.Object);
#endif
        }

        public static object GetPropertyValueReflection(this object target, string name, Type asType = null)
        {
            var type = asType != null ? asType : target.GetType();
            var property = type.GetProperty(name, flags_property);
            if (property != null)
            {
                return property.GetValue(target);
            }

#if UNITY_EDITOR
            if (type.IsSubclassOf(typeof(UnityEngine.Object)))
                EditorUtility.SetDirty(target as UnityEngine.Object);
#endif

            return null;
        }

        public static void GetTypeChain(Type type, Type terminator, List<Type> buffer)
        {
            buffer.Clear();
            var t = type.GetType();
            do
            {
                buffer.Add(t);
                t = t.BaseType;
            }
            while (t != null && t != terminator);
        }

        private static List<Type> internalTypeListBuffer = new List<Type>();

        // DO NOT try to add interface filtering here, there is a shitshow of interfaces,
        // getting reimplemented in base and subs and all over, so its not usable.
        public static bool GetTypeChainComplex(Type type, HashSet<Type> terminators, HashSet<Type> whitelist, HashSet<Type> blacklist, List<Type> buffer)
        {
            buffer.Clear();
            internalTypeListBuffer.Clear();
            do
            {
                internalTypeListBuffer.Add(type);
                type = type.BaseType;
            }
            while (type != null);

            if (blacklist != null && blacklist.Count > 0)
            {
                for (int i = 0; i < internalTypeListBuffer.Count; i++)
                {
                    if (blacklist.Contains(internalTypeListBuffer[i]))
                    {
                        internalTypeListBuffer.Clear();
                        return false;
                    }
                }
            }

            if (whitelist != null && whitelist.Count > 0)
            {
                bool valid = false;
                for (int i = 0; i < internalTypeListBuffer.Count; i++)
                    valid |= whitelist.Contains(internalTypeListBuffer[i]);

                if (!valid)
                    return false;
            }

            for (int i = 0; i < internalTypeListBuffer.Count; i++)
            {
                var t = internalTypeListBuffer[i];
/*                if(t.IsGenericType)
                {
                    var definition = t.GetGenericTypeDefinition();
                }*/

                if (terminators.Contains(t))
                    break;

                buffer.Add(t);
            }

            return true;
        }

        public static Type GetFirstTypeWithAttribute(List<Type> list, Type attribute, bool isStatic = false)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var t = list[i];
                if (isStatic && !t.IsStatic())
                    continue;

                var attr = t.GetCustomAttribute(attribute);
                if (attr != null)
                    return t;
            }

            return null;
        }

        public static List<Type> GetAllTypesWithAttribute(List<Type> list, Type attribute, bool isStatic = false)
        {
            List<Type> filtered = new List<Type>();
            for (int i = 0; i < list.Count; i++)
            {
                var t = list[i];
                if (isStatic && !t.IsStatic())
                    continue;

                var attr = t.GetCustomAttribute(attribute);
                if (attr != null)
                {
                    filtered.Add(t);
                }
            }

            return filtered;
        }

        public static List<Type> GetAllTypesWithInterface(List<Type> list, Type _interface, bool isStatic = false, bool excludeAbstract = true)
        {
            List<Type> filtered = new List<Type>();
            for (int i = 0; i < list.Count; i++)
            {
                var t = list[i];
                if (isStatic && !t.IsStatic())
                    continue;

                if (t.IsAbstract || t == _interface)
                    continue;

                if(_interface.IsAssignableFrom(t))
                {
                    filtered.Add(t);
                }
            }

            return filtered;
        }

        public static MethodInfo GetAnyMethodWithAttribute(List<Type> list, Type attribute)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var methods = list[i].GetMethods();

                for (int t = 0; t < methods.Length; t++)
                {
                    var method = methods[t];
                    if (method.GetCustomAttribute(attribute) != null)
                        return method;
                }
            }

            return null;
        }


        public static Type GetCachedGenericType(Type definition, Type arg1)
        {
            if(!genericTypeCache_arg1.TryGetValue(definition, out var dict))
            {
                dict = new Dictionary<Type, Type>();
                genericTypeCache_arg1.Add(definition, dict);
            }
            
            if(!dict.TryGetValue(arg1, out var finalType))
            {
                finalType = definition.MakeGenericType(arg1);
                dict.Add(arg1, finalType);
            }

            return finalType;
        }
    }

}