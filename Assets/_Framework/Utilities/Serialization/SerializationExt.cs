﻿using UnityEngine;

namespace Framework
{
    public static class SerializationExt
    {
        /// <summary>
        /// Creates a copy using OdinSerializer
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static object Clone(this object obj)
        {
            return Sirenix.Serialization.SerializationUtility.CreateCopy(obj);
        }

        /// <summary>
        /// Creates a copy using OdinSerializer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Clone<T>(this T obj) where T : class
        {
            return Sirenix.Serialization.SerializationUtility.CreateCopy(obj) as T;
        }

        /// <summary>
        /// Clones unity object through Instantiate()
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T CloneUnityObject<T>(this T obj) where T : UnityEngine.Object
        {
            return GameObject.Instantiate<T>(obj);
        }

        /// <summary>
        /// Clones the object while keeping it under the same parent it was.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="worldPositionStays"></param>
        /// <returns></returns>
        public static T CloneGameObject<T>(this T obj, bool keepParent = false, bool worldPositionStays = true) where T : Component
        {
            return GameObject.Instantiate<T>(obj, keepParent ? obj.transform.parent : null, true);
        }

        /// <summary>
        /// Clones the object while keeping it under the same parent it was.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="worldPositionStays"></param>
        /// <returns></returns>
        public static GameObject CloneGameObject(this GameObject obj, bool keepParent = false, bool worldPositionStays = true)
        {
            return GameObject.Instantiate(obj, keepParent ? obj.transform.parent : null, true);
        }

    }
}