﻿using HashDepot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace TheGrid
{
    /// <summary>
    /// Designed by Alex Antonov
    /// </summary>

    public class Grid
    {
        public enum HashMethod
        {
            Spooky,
            XXHash
        }

        public enum Mode
        {
            Mode2D,
            Mode3D
        }

        public int CellSize => cellSize;
        private int cellSize = 8;

        public HashMethod hashMethod { get; set; } = HashMethod.Spooky;

        public Vector3 CellDimensions => new Vector3(cellSize, cellSize, cellSize);

        private GridArrayPool pool = new GridArrayPool();

        private const int BIG_ENOUGH_INT = 16 * 1024;
        private const double BIG_ENOUGH_FLOOR = BIG_ENOUGH_INT + 0.0000;

        private Dictionary<int, GridBucket> shash = new Dictionary<int, GridBucket>();
        private Mode mode;

        private List<GridBucket> areaBuffer = new List<GridBucket>();

        public Grid(int cellSize = 8, Mode mode = Mode.Mode2D)
        {
            this.cellSize = cellSize;
            this.mode = mode;
        }



        private void PopulateCircleToAreaBuffer(float diameter, Vector3 origin, GridQuery query = null)
        {
            origin = new Vector3(origin.x, 0, origin.z);
            var bounds = GetBounds(origin, diameter);

            if (query != null && query.Debug)
                query.Bounds = bounds;

            GetMinMax(bounds, out var min, out var max);
            PopulateAreaBuffer(min, max, query);
        }

        private void PopulateAreaBuffer(Vector2 min, Vector2 max, GridQuery query = null)
        {
            areaBuffer.Clear();


            // Case 1 : same cell
            if (min == max)
            {
                if (shash.TryGetValue(Hash(new Vector3(min.x, 0, min.y)), out var array))
                {
                    areaBuffer.Add(array);
                    if (query != null && query.Debug)
                    {
                        query.TestedCells.Add(array.Position);
                        query.TestedHashes.Add(array.Hash);
                    }
                    return;
                }
            }

            // case 2: N cells
            int xsteps = (int)Ceil((max.x - min.x) / cellSize);
            int ysteps = (int)Ceil((max.y - min.y) / cellSize);

            for (int x = 0; x <= xsteps; x++)
            {
                for (int y = 0; y <= ysteps; y++)
                {
                    var cell = new Vector3(cellSize * x + min.x, 0, cellSize * y + min.y);
                    var hash = Hash(cell);

                    if (shash.TryGetValue(hash, out var array))
                    {
                        areaBuffer.Add(array);
                    }

                    if (query != null && query.Debug)
                    {
                        query.TestedCells.Add(cell);
                        query.TestedHashes.Add(hash);
                    }
                }
            }
        }

        public void OverlapCircle(float radius, Vector3 origin, GridQuery query)
        {
            if (query == null)
            {
                return;
            }

            query.Clear();
            // flatten origin
            origin = new Vector3(origin.x, 0, origin.z);
            float sqrmag = radius * radius;

            PopulateCircleToAreaBuffer(radius * 2, origin, query);

            for (int i = 0; i < areaBuffer.Count; i++)
            {
                for (int t = 0; t < areaBuffer[i].Count; t++)
                {
                    var obj = areaBuffer[i].InternalArray[t];
                    var pos = obj.Position;
                    float colDistance = radius + obj.Radius;
                    float objSqrMag = (pos - origin).sqrMagnitude;
                    if (objSqrMag <= colDistance * colDistance)
                    {
                        if (query.TryAddObject(obj))
                            obj.LastQuerySqrMag = objSqrMag;
                    }
                }
            }
        }

        public void Insert(IGridObject obj, int? hashPrecalc = null, Bounds? boundsPrecalc = null)
        {
            var pos = obj.Position;
            pos = new Vector3(pos.x, 0, pos.z);
            var rad = obj.Radius;

            void InsertIn(int _lhash, Vector3 arrayPosition)
            {
                if (!shash.TryGetValue(_lhash, out var array))
                {
                    array = GridBucket.Rent(pool);
                    array.Position = arrayPosition;
                    shash.Add(_lhash, array);
                }

                int index = array.Insert(obj);
                obj.BucketData.Add(_lhash, new GridObjectBucketData(_lhash, index));
            }

            int hash = hashPrecalc.HasValue ? hashPrecalc.Value : Hash(pos);
            obj.LastPositionHash = hash;

            if (rad == 0)
            {
                InsertIn(hash, FloorPos(pos));
                return;
            }
            else
            {
                float diameter = rad * 2;
                /*Bounds b = new Bounds(pos, new Vector3(diameter, 0, diameter));
                Vector2 min = new Vector2(GridFloor(b.min.x), GridFloor(b.min.z));
                Vector2 max = new Vector2(GridFloor(b.max.x), GridFloor(b.max.z));*/

                Bounds newBounds = GetBounds(pos, diameter);
                GetMinMax(newBounds, out var min, out var max);
                obj.LastBounds = newBounds;

                if (min == max)
                {
                    InsertIn(hash, FloorPos(pos));
                    return;
                }
                else
                {
                    int xsteps = (int)Ceil((max.x - min.x) / cellSize);
                    int ysteps = (int)Ceil((max.y - min.y) / cellSize);

                    for (int x = 0; x <= xsteps; x++)
                    {
                        for (int y = 0; y <= ysteps; y++)
                        {
                            var npos = new Vector3(cellSize * x + min.x, 0, cellSize * y + min.y);
                            InsertIn(Hash(npos), npos);
                        }
                    }

                    return;
                }
            }
        }

        public void Remove(IGridObject obj)
        {
            foreach (var pair in obj.BucketData)
            {
                if (shash.TryGetValue(pair.Key, out var array))
                {
                    array.Remove(obj, pair.Value);
                    if (array.Count == 0)
                    {
                        GridBucket.Return(array);
                        shash.Remove(pair.Key);
                    }
                }
            }

            obj.BucketData.Clear();

            /*            for (int i = obj.GridHashes.Count - 1; i >= 0; i--)
                        {
                            if (shash.TryGetValue(obj.GridHashes[i], out var array))
                            {
                                array.Remove(obj);
                                if (array.Count == 0)
                                {
                                    GridBucket.Return(array);
                                    shash.Remove(obj.GridHashes[i]);
                                }
                            }
                        }

                        obj.GridHashes.Clear();*/
        }

        public void Update(IGridObject obj)
        {
            // depending on radius we either do fast or aabb update
            if (obj.Radius == 0)
            {
                var hash = Hash(obj.Position);
                if (hash != obj.LastPositionHash)
                {
                    Remove(obj);
                    Insert(obj, hash);
                }
            }
            else
            {
                var bounds = GetBounds(obj.Position, obj.Radius * 2f);
                if (bounds != obj.LastBounds)
                {
                    Remove(obj);
                    Insert(obj);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void GetMinMax(Bounds bounds, out Vector2 min, out Vector2 max)
        {
            min = new Vector2(GridFloor(bounds.min.x), GridFloor(bounds.min.z));
            max = new Vector2(GridFloor(bounds.max.x), GridFloor(bounds.max.z));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Bounds GetBounds(Vector3 origin, float diameter)
        {
            return new Bounds(origin, new Vector3(diameter, 0, diameter));
        }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //private static int FastFloor(float f)
        //{
        //    return (int)(f + BIG_ENOUGH_FLOOR) - BIG_ENOUGH_INT;
        //}


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static void FromShort(short number, out byte byte1, out byte byte2)
        {
            byte2 = (byte)(number >> 8);
            byte1 = (byte)(number >> 0);
        }

        private static byte[] hashBuffer = new byte[4];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private unsafe int Hash(Vector3 v)
        {
            Vector3 floored = FloorPos(v);
            FromShort((short)floored.x, out var b0, out var b1);
            FromShort((short)floored.z, out var b2, out var b3);
            hashBuffer[0] = b0;
            hashBuffer[1] = b1;
            hashBuffer[2] = b2;
            hashBuffer[3] = b3;
            return MemoryMarshal.Read<int>(new Span<byte>(hashBuffer));
        }
            /*           Span<short> fs = new Span<short>(shortBuffer);
                       Span<byte> s = MemoryMarshal.AsBytes(fs);
                       if (hashMethod == HashMethod.Spooky)
                       {
                           Hash128 hash = new Hash128();

                           HashUtilities.QuantisedVectorHash(ref floored, ref hash);
                           return hash.GetHashCode();
                       }
                       else
                       {



                         кeturn (int)XXHash.Hash64(s);
                       }*/

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private float GridFloor(float f)
        {
            int sign = f < 0 ? -1 : 1;
            f = Abs(f);
            f = f - (f % cellSize) + (cellSize / 2F);
            return f * sign;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 FloorPos(Vector3 v)
        {
            return new Vector3(GridFloor(v.x),
                                    0,
                                    GridFloor(v.z));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private float Abs(float val)
        {
            return (val >= 0) ? val : (-val);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private float Ceil(float val)
        {
            float num = (float)Math.Floor(val);
            if (num != val)
            {
                num += 1.0F;
            }
            return num;
        }

        public void DrawGizmos()
        {
            Gizmos.color = new Color(1, 1, 1, 0.2F);
            foreach (var array in shash.Values)
            {
                if (array.Count > 0)
                    Gizmos.DrawWireCube(array.Position, Vector3.one * cellSize);
            }
        }
    }

}
