﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TheGrid
{
    internal class GridArrayPool
    {
        public int Min => 4;
        public int CurrentMax { get; private set; } = 0;

        private List<Stack<IGridObject[]>> map;

        public GridArrayPool()
        {
            map = new List<Stack<IGridObject[]>>(1024);
            CurrentMax = Min;
            for (int i = 0; i < CurrentMax + 1; i++)
            {
                map.Add(null);
            }
        }

        /// <summary>
        /// Allowed sizes 4 to 256 POT
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public IGridObject[] Rent(int size)
        {
            Stack<IGridObject[]> stack = null;
            
            if (size > CurrentMax)
            {
                Expand();
                map[size] = stack = new Stack<IGridObject[]>();
            }
            else
            {
                stack = map[size];
                if (stack == null)
                    map[size] = stack = new Stack<IGridObject[]>();
            }

            if (stack.Count == 0)
                return new IGridObject[size];

            return stack.Pop();
        }

        private void Expand()
        {
            int newSize = CurrentMax * 2 ;
            for (int i = 0; i < newSize + 1; i++)
            {
                map.Add(null);
            }
            CurrentMax = newSize;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Return(IGridObject[] arr, int toClear)
        {
            Clear(arr, toClear);
            map[arr.Length].Push(arr);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Clear(IGridObject[] arr, int count)
        {
            count = Mathf.Clamp(count, 0, arr.Length);

            for (int i = 0; i < count; i++)
            {
                arr[i] = null;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Copy(IGridObject[] source, IGridObject[] dest, int count)
        {
            Array.Copy(source, dest, count);
        }

        public void Grow(ref IGridObject[] arr)
        {
            var narr = Rent(arr.Length * 2);
            Copy(arr, narr, arr.Length);
            Return(arr, arr.Length);
            arr = narr;
        }

        public void Shrink(ref IGridObject[] arr)
        {
            var narr = Rent(arr.Length / 2);
            Copy(arr, narr, narr.Length);
            Return(arr, arr.Length + 1);
            arr = narr;
        }
    }
}
