﻿namespace TheGrid
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public struct GridObjectBucketData
    {
        public int hash;
        public int index;

        public GridObjectBucketData(int hash, int index)
        {
            this.hash = hash;
            this.index = index;
        }
    }

    internal class GridBucket
    {
        public int Count => _index + 1; 
        public int Size => _array == null ? 0 : _array.Length;
        public Vector3 Position { get; set; }
        public int Hash { get; set; }
        public IGridObject[] InternalArray => _array;

        private int _index = -1;
        private GridArrayPool _pool;
        private IGridObject[] _array;

        private static Stack<GridBucket> _localPool = new Stack<GridBucket>();

        public GridBucket(GridArrayPool pool)
        {
            this._pool = pool;
        }

        public int Insert(IGridObject obj)
        {
            if (_array == null)
                _array = _pool.Rent(_pool.Min);

            if (_array.Length < Count + 1)
                _pool.Grow(ref _array);

            _array[++_index] = obj;
            return _index;
        }

        public void Remove(IGridObject obj, GridObjectBucketData data)
        {
            if(Count == 1)
            {
                _pool.Return(_array, 1);
                _array = null;
                _index = -1;
                return;
            }

            if(data.index == _index)
            {
                // no need to swap
                _array[_index] = null;
            }
            else
            {
                // swap last for this and null the last, update grid index
                int nindex = data.index;
                _array[nindex] = _array[_index];
                _array[nindex].BucketData[data.hash] = new GridObjectBucketData(data.hash, nindex);
                _array[_index] = null;
            }

            _index--;

            if(Size > _pool.Min)
            {
                int shrink = Mathf.Max(_array.Length / 2, _pool.Min);

                if (Count <= shrink)
                {
                    _pool.Shrink(ref _array);
                }
            }
        }

        public static GridBucket Rent(GridArrayPool pool)
        {
            if (_localPool.Count > 0)
                return _localPool.Pop();
            return new GridBucket(pool);
        }

        public static void Return(GridBucket gridArray)
        {
            gridArray._index = -1;
            _localPool.Push(gridArray);
        }
    }
}
