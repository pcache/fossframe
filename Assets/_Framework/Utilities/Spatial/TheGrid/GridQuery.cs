﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TheGrid
{
    internal interface IGridQueryInternal
    {
        HashSet<IGridObject> GetTransient();
    }

    [Serializable]
    public class GridQuery : IGridQueryInternal, IComparer<IGridObject>
    {
        public bool Debug { get; private set; }
        public List<IGridObject> Results { get; private set; } = new List<IGridObject>();
        public List<Vector3> TestedCells => testedCells;
        public List<int> TestedHashes => testedHashes;
        public Bounds Bounds { get; set; }

        [SerializeField]
        private List<Vector3> testedCells;

        [SerializeField]
        private List<int> testedHashes;


        private HashSet<IGridObject> transientBuffer = new HashSet<IGridObject>();

        HashSet<IGridObject> IGridQueryInternal.GetTransient() => transientBuffer;

        public GridQuery(bool debug = false)
        {
            this.Debug = debug;

            if(Debug)
            {
                testedCells = new List<Vector3>();
                testedHashes = new List<int>();
            }
        }

        public void Clear()
        {
            transientBuffer.Clear();
            Results.Clear();
            if (Debug)
            {
                TestedCells.Clear();
                TestedHashes.Clear();
            }
        }

        public bool TryAddObject(IGridObject gridObject)
        {
            if(!transientBuffer.Contains(gridObject))
            {
                transientBuffer.Add(gridObject);
                Results.Add(gridObject);
                return true;
            }
            return false;
        }

        public void SortByProximity()
        {
            if(Results.Count > 1)
                Results.Sort(this); 
        }

        public void AddCell(Vector3 cell)
        {
            if (Debug) TestedCells.Add(cell);
        }

        public void AddHash(int hash)
        {
            if (Debug) TestedHashes.Add(hash);
        }

        public int Compare(IGridObject x, IGridObject y)
        {
            return (x.LastQuerySqrMag.CompareTo(y.LastQuerySqrMag));
        }
    }
}
