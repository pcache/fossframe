using System.Collections;
using System.Collections.Generic;
using TheGrid;
using UnityEngine;

public class GridTest : MonoBehaviour
{
    public static GridTest instance;
    public int cellSize = 32;

    public TheGrid.Grid.HashMethod HashMethod;
    public TheGrid.Grid grid;

    private void Awake()
    {
        grid = new TheGrid.Grid(cellSize);
        instance = this;
    }

    public void Register(IGridObject target)
    {
        grid.Insert(target);
    }

    public void Unregister(IGridObject target)
    {
        grid.Remove(target);
    }

    public void UpdateObject(IGridObject target)
    {
        grid.Update(target);
    }

    private void Update()
    {
        grid.hashMethod = HashMethod;
    }

    private void OnDrawGizmos()
    {
        if(grid != null)
            grid.DrawGizmos();
    }
}


