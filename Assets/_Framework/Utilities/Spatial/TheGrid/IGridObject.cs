﻿namespace TheGrid
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public interface IGridObject
    {
        Vector3 Position { get; }
        float Radius { get; }
        float LastQuerySqrMag { get; set; }
        int LastPositionHash { get; set; }
        Bounds LastBounds { get; set; }
        Dictionary<int, GridObjectBucketData> BucketData { get; set; }
    }
}
