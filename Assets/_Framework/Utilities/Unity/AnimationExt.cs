﻿namespace Framework
{
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class AnimationExt
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float FrameToTime(this int frame, AnimationClip clip)
        {
            if (!clip)
                return 0f;

            float f = frame;
            float fr = clip.frameRate;

            return 1f / fr * frame;
        }
    }
}