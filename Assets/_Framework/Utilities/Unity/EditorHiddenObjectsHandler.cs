﻿using Framework;
using System;
using UnityEditor;
using UnityEngine;

namespace Framework
{
    /// <summary>
    /// This is a utility object that handles Editor only attachment of hidden objects.
    /// Usage is:
    /// Call the  OnEnable, OnDisable in the user class, OnValidate is optional.
    /// Modify construction through a callback using provided methods, like AttachPrefab()
    /// Make sure you add [SelectionBase] attribute to the user class.
    /// This has to be serialized and hidden in inspector.
    /// </summary>
    [Serializable]
    public class EditorHiddenObjectsHandler
    {
        public bool DestroyAllOnReconstruct { get; set; } = true;

        private GameObject owner;
        [SerializeField] private Transform _hiddenTransform;

        public delegate void OnReconstructDelegate(bool requiresFullReconstruct);
        private OnReconstructDelegate onReconstructDelegate;

        private const string hiddenTransformName = "_hidden_transform_tag";

        public EditorHiddenObjectsHandler(bool destroyAllOnReconstruct = true)
        {
            DestroyAllOnReconstruct = destroyAllOnReconstruct;
        }

        public void ForceReconstruct(bool requiresFullReconstruct)
        {
            EditorApplication.delayCall += ()=> OnEditorUpdate(requiresFullReconstruct);
        }
        
        public void Clear()
        {
            if (GetHiddenTransform())
            {
                if (Application.isPlaying)
                {
                    _hiddenTransform.DestroyAllChildren();
                }
                else
                {
                    _hiddenTransform.DestroyAllChildrenImmidiate();
                }
            }
        }

        public void OnValidate(GameObject owner, OnReconstructDelegate onReconstruct)
        {
#if UNITY_EDITOR
            this.owner = owner;
            this.onReconstructDelegate = onReconstruct;
            if (Application.isPlaying)
                return;

            EditorApplication.delayCall += () => OnEditorUpdate(false);
#endif
        }

        public void OnEnable(GameObject owner, OnReconstructDelegate onReconstruct)
        {
#if UNITY_EDITOR
            this.owner = owner;
            this.onReconstructDelegate = onReconstruct;

            Clear();

            if (!Application.isPlaying)
            {
                EditorApplication.delayCall += () =>OnEditorUpdate(true);
            }
#endif
        }

        public void OnDisable(GameObject owner)
        {
#if UNITY_EDITOR
            this.owner = owner;

            Clear();
#endif
        }

        public void OnDestroy(GameObject owner)
        {
#if UNITY_EDITOR
            this.owner = owner;
            Clear();
#endif
        }

        public Transform GetHiddenTransform()
        {
            if (!_hiddenTransform)
            {
                // Try locate
                _hiddenTransform = owner.transform.Find(hiddenTransformName);
            }
            return _hiddenTransform;
        }

        public GameObject AttachPrefab(GameObject prefab, Vector3 localPosition = default)
        {
            if(!Application.isPlaying)
            {
                if (GetHiddenTransform())
                {
                    var gameobject = PrefabUtility.InstantiatePrefab(prefab, _hiddenTransform);
                    gameobject.name = "editor_hidden_" + gameobject.name;
                    GameObject pgo = gameobject as GameObject;
                    pgo.transform.localPosition = localPosition;
                    PrefabUtility.UnpackPrefabInstance(pgo, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                    UnityExt.SetHideFlagsRecursive(pgo.transform, HideFlags.HideAndDontSave);
                    return pgo;
                }
            }

            return null;
        }

        private void OnEditorUpdate(bool requiresFullReconstruct)
        {
            if (UnityExt.IsNullOrDestroyedSilent(owner))
                return;

            if (!owner.activeInHierarchy)
                return;

            if (owner && PrefabEditorExt.IsPrefabAsset(owner))
                return;

            if (!GetHiddenTransform())
            {
                HideFlags hideFlags = HideFlags.HideAndDontSave;

                _hiddenTransform = new GameObject(hiddenTransformName).transform;
                _hiddenTransform.Attach(owner.transform).ZeroOut();
                _hiddenTransform.gameObject.hideFlags = hideFlags;
            }

            if(requiresFullReconstruct || DestroyAllOnReconstruct)
                _hiddenTransform.DestroyAllChildrenImmidiate();

            onReconstructDelegate?.Invoke(requiresFullReconstruct);
        }
    }

}
