﻿namespace Framework
{
#if UNITY_EDITOR
    using UnityEditor;
#endif

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public enum PlayerPrefsCategory
    {
        Dev,
        Game
    }

    public abstract class PlayerPrefsValueHandler<T> where T : struct
    {
        protected string Path { get; private set; }
        public PlayerPrefsValueHandler(string name, PlayerPrefsCategory cat)
        {
            Path = PrefsExt.Root + "." + PrefsExt.PlayerPrefsCategoryToString(cat) + name;
        }
        public abstract void Set(T value);
        public abstract T Get();
        public abstract void Toggle();
    }

    public sealed class PlayerPrefsBoolHandler : PlayerPrefsValueHandler<bool>
    {
        public PlayerPrefsBoolHandler(string name, PlayerPrefsCategory cat) : base(name, cat) { }
        public override void Set(bool value) => Path.SetPlayerPrefsBool(value);
        public override bool Get() => Path.GetPlayerPrefsBool();
        public override void Toggle() => Set(!Get());
    }

#if UNITY_EDITOR
    public sealed class EditorPrefsBoolHandler : PlayerPrefsValueHandler<bool>
    {
        public EditorPrefsBoolHandler(string name, PlayerPrefsCategory cat) : base(name, cat) { }
        public override void Set(bool value) => Path.SetEditorPrefsBool(value);
        public override bool Get() => Path.GetEditorPrefsBool();
        public override void Toggle() => Set(!Get());
    } 
#endif

    public static class PrefsExt
    {
        public static string Root => ProjectInfo.Instance.PrefsNameSpace;

        public static string PlayerPrefsCategoryToString(PlayerPrefsCategory cat)
        {
            switch (cat)
            {
                case PlayerPrefsCategory.Dev:
                    return "dev.";
                case PlayerPrefsCategory.Game:
                    return "dev.";
                default:
                    return "default.";
            }
        }

#if UNITY_EDITOR
        public static bool GetEditorPrefsBool(this string path) => EditorPrefs.GetBool(path);
        public static void SetEditorPrefsBool(this string path, bool value) => EditorPrefs.SetBool(path, value);
#endif

        public static bool GetPlayerPrefsBool(this string path)
        {
            var i = PlayerPrefs.GetInt(path);
            return i != 0;
        }

        public static void SetPlayerPrefsBool(this string path, bool value) => PlayerPrefs.SetInt(path, value ? 1 : 0);
    }
}
