﻿using Framework;
using System;
using UnityEngine;

namespace Framework
{
    [System.Serializable]
    public class RectTransformHelper
    {
        public RectTransform rectTransform { get; private set; }

        public float xMin { get; private set; }
        public float xMax { get; private set; }
        public float yMin { get; private set; }
        public float yMax { get; private set; }
        public float width { get; private set; }
        public float widthUnscaled { get; private set; }
        public float height { get; private set; }
        public float heightUnscaled { get; private set; }

        public Vector3 scale { get; private set; }
        public Vector3 botLeft { get; private set; }
        public Vector3 topLeft { get; private set; }
        public Vector3 botRight { get; private set; }
        public Vector3 topRight { get; private set; }
        public Vector3 center { get; private set; }
        public Vector3[] WorldCorners { get; private set; } = new Vector3[4];
        public Vector3[] LocalCorners { get; private set; } = new Vector3[4];

        private float[] floatbuffer4 = new float[4];

        public RectTransformHelper(RectTransform rectTransform)
        {
            this.rectTransform = rectTransform;
        }

        public RectTransformHelper(Transform rectTransform)
        {
            this.rectTransform = rectTransform.AsRect();
        }

        public void Update()
        {
            scale = rectTransform.lossyScale;

            rectTransform.GetWorldCorners(WorldCorners);

            botLeft = WorldCorners[0];
            topLeft = WorldCorners[1];
            topRight = WorldCorners[2];
            botRight = WorldCorners[3];

            void cacheScalar(int scalar)
            {
                for (int i = 0; i < 4; i++)
                    floatbuffer4[i] = WorldCorners[i][scalar];
            }

            cacheScalar(0);
            xMin = Mathf.Min(floatbuffer4);
            xMax = Mathf.Max(floatbuffer4);

            cacheScalar(1);
            yMin = Mathf.Min(floatbuffer4);
            yMax = Mathf.Max(floatbuffer4);

            center = default;
            for (int i = 0; i < 4; i++)
            {
                center += WorldCorners[i];
            }

            rectTransform.GetLocalCorners(LocalCorners);

            widthUnscaled = LocalCorners[3].x - LocalCorners[0].x;
            width = (widthUnscaled) * scale.x;

            heightUnscaled = LocalCorners[1].y - LocalCorners[0].y;
            height = (heightUnscaled) * scale.y;

        }

        public void DrawGizmos()
        {
            if (WorldCorners == null || WorldCorners.Length < 4)
                return;

            Gizmos.DrawLine(WorldCorners[0], WorldCorners[1]);
            Gizmos.DrawLine(WorldCorners[1], WorldCorners[2]);
            Gizmos.DrawLine(WorldCorners[2], WorldCorners[3]);
            Gizmos.DrawLine(WorldCorners[3], WorldCorners[0]);
        }
    }
}
