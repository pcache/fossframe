﻿namespace Framework
{
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class TextureExt
    {
        public static void FillTexture(Color color, Texture2D texture)
        {
            Color[] cols = texture.GetPixels();

            for (int i = 0; i < cols.Length; i++)
            {
                cols[i] = color;
            }

            texture.SetPixels(cols);

            texture.Apply();
        }
    }
}