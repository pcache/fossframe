﻿namespace Framework
{
    using System;
    using UnityEngine;
    
    public class Ticker
    {
        public float DeltaTime { get; private set; }
        public float Tickrate => GetTickrate();
        public float Progress { get; private set; }

        protected float tickrate;
        private float lastTick;
        private bool unscaled;
        private bool instantFirstTick;

        public Ticker(float tickrate, bool useUnscaledTime, bool instantFirstTick = false)
        {
            this.tickrate = tickrate;
            this.unscaled = useUnscaledTime;
            this.instantFirstTick = instantFirstTick;
        }

        public void Reset()
        {
            if (instantFirstTick)
                lastTick = float.MinValue;
            else
                lastTick = GetTime();
        }

        protected virtual float GetTime()
        {
            return unscaled ? Time.unscaledTime : Time.time; 
        }

        protected virtual float GetTickrate()
        {
            return tickrate;
        }

        public bool Update()
        {
            float time = GetTime();

            Progress = Mathf.InverseLerp(lastTick, lastTick + Tickrate, time);
            
            if (lastTick + Tickrate < time)
            {
                DeltaTime = time - lastTick;
                lastTick = time;
                return true;
            }

            return false;
        }
    }

    public class DynamicValueTicker : Ticker
    {
        protected Func<float> tickrateGetter;

        public DynamicValueTicker(Func<float> tickrateGetter, bool useUnscaledTime, bool instantFirstTick = false) : base(1F, useUnscaledTime, instantFirstTick)
        {
            this.tickrateGetter = tickrateGetter;
        }

        protected override float GetTickrate()
        {
            if (tickrateGetter != null)
                return tickrateGetter();

            return default;
        }
    }
}