﻿
namespace Framework
{
    using UnityEngine;
    using UnityEngine.UI;

    public static class UGUIExt
    {
        public static RectTransform AsRect(this Transform transform) => transform as RectTransform;
        public static RectTransform RectTransform<T>(this T mono) where T : MonoBehaviour => mono.transform as RectTransform;

        public static bool RectContainsMouse(this RectTransform rt)
        {
            Vector2 localMousePosition = rt.InverseTransformPoint(Input.mousePosition);
            return rt.rect.Contains(localMousePosition);
        }

        public static bool RectContainsMouse(this Transform tr)
        {
            return ((RectTransform)tr).RectContainsMouse();
        }

        public static void SetAlpha(this Graphic graphic, float alpha)
        {
            var color = graphic.color;
            color.a = alpha;
            graphic.color = color;
        }
    }
}