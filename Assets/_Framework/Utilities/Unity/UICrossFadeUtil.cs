﻿namespace Framework
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using TMPro;
    using UnityEngine.UI;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    public static class UICrossFadeUtil
    {
        private static Dictionary<object, float> init_alpha = new Dictionary<object, float>();
        private static Dictionary<object, Color> init_colors = new Dictionary<object, Color>();

        public static void CrossFadeAlphaOptimized(this TMP_Text text, float alpha, float speed, bool ignoreTimeScale, float? customDelta = null)
        {
            var delta = customDelta.HasValue ? customDelta.Value : (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);
            
            var objalpha = text.alpha;
            
            if (objalpha != alpha)
            {
                objalpha = Mathf.MoveTowards(objalpha, alpha, speed * delta);
                text.alpha = objalpha;
            }
        }

        public static void CrossFadeAlphaOptimized(this Graphic graphic, float alpha, float speed, bool ignoreTimeScale, float? customDelta = null)
        {
            var delta = customDelta.HasValue ? customDelta.Value : (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);
            
            var objcolor = graphic.color;
            
            if (objcolor.a != alpha)
            {
                objcolor.a = Mathf.MoveTowards(objcolor.a, alpha, speed * delta);
                graphic.color = objcolor;
            }
        }

        public static void CrossFadeAlphaOptimized(this CanvasGroup group, float alpha, float speed, bool ignoreTimeScale, float? customDelta = null)
        {
            var delta = customDelta.HasValue ? customDelta.Value : (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);

            var groupAlpha = group.alpha;
            
            if (groupAlpha != alpha)
            {
                groupAlpha = Mathf.MoveTowards(groupAlpha, alpha, speed * delta);
                group.alpha = groupAlpha;
            }
        }

        public static void CrossFadeColorOptimized(this Graphic graphic, Color target, float speed, bool ignoreTimeScale, bool useAlpha, float? customDelta = null)
        {
            var delta = customDelta.HasValue ? customDelta.Value : (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);
            
            var objcolor = graphic.color;
            
            if (objcolor != target)
            {
                objcolor = Color.Lerp(objcolor, target, speed * delta);
                graphic.color = objcolor;
            }
        }
    }
}