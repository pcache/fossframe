﻿#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Framework
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public static class UnityExt
    {
        public static bool IsNullOrDestroyedSilent(this object uobject)
        {
            try
            {
                return !(uobject as UnityEngine.Object);
            }
            catch
            {
                // swallow the exception
            }
            return true;
        }

        public static bool IsNull(this System.Object obj)
        {
            if (object.ReferenceEquals(obj, null)) return true;

            if (obj is UnityEngine.Object) return (obj as UnityEngine.Object) == null;

            return false;
        }

        public static T GetOrAddComponent<T>(this Component source) where T : Component
        {
            var comp = source.GetComponent<T>();
            if (!comp)
                comp = source.gameObject.AddComponent<T>();
            return comp;
        }

        public static T GetComponentInParentOnly<T>(this Transform transform) where T : Component
        {
            if (transform.parent)
            {
                return transform.parent.GetComponent<T>();
            }
            return null;
        }

        public static T GetComponentInParentOnly<T>(this GameObject gameobject) where T : Component
        {
            if (gameobject.transform.parent)
            {
                return gameobject.transform.parent.GetComponent<T>();
            }
            return null;
        }

        public static T GetComponentInParentOnly<T>(this Component component) where T : Component
        {
            if (component.transform.parent)
            {
                return component.transform.parent.GetComponent<T>();
            }
            return null;
        }

        public static void VisitHierarchyTransforms(Transform root, Action<Transform> visitor)
        {
            visitor?.Invoke(root);
            foreach (Transform transform in root)
            {
                VisitHierarchyTransforms(transform, visitor);
            }
        }

        public static void SetHideFlagsRecursive(Transform root, HideFlags flags)
        {
            void SetRecursive(Transform tr)
            {
                tr.gameObject.hideFlags = flags;
                foreach (Transform transform in tr)
                {
                    SetRecursive(transform);
                }
            }

            SetRecursive(root);
        }

        public static void GetAllComponentsInList<T>(this List<GameObject> source, List<T> buffer) where T : class
        {
            buffer.Clear();
            List<Component> getBuffer = new List<Component>();

            for (int i = 0; i < source.Count; i++)
            {
                var go = source[i];
                go.GetComponents(typeof(T), getBuffer);
                for (int t = 0; t < getBuffer.Count; t++)
                {
                    buffer.Add(getBuffer[t] as T);
                }
            }
        }

        private static List<GameObject> GetAllGameObjectsInScene_GetBuffer = new List<GameObject>();
        public static List<GameObject> GetAllGameObjectsInActiveScene()
        {
            var roots = SceneManager.GetActiveScene().GetRootGameObjects();
            List<GameObject> allGameObjects = new List<GameObject>();
            foreach (var go in roots)
            {
                go.GetAllGameObjectsInHierarchy(GetAllGameObjectsInScene_GetBuffer);
                allGameObjects.AddRange(GetAllGameObjectsInScene_GetBuffer);
            }

            return allGameObjects;
        }

        /// <summary>
        /// Allocates a list. Meant for massive hierarchies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="go"></param>
        /// <param name="buffer"></param>
        public static void GetAllComponentsInHierarchy<T>(this GameObject go, List<T> buffer) where T : class
        {
            List<Component> getComponentBuffer = new List<Component>();

            void GetAllComponentsInHierarchyRecursive(Transform tr)
            {
                int count = tr.childCount;
                for (int i = 0; i < count; i++)
                {
                    var ctr = tr.GetChild(i);
                    ctr.GetComponents(typeof(T), getComponentBuffer);
                    for (int t = 0; t < getComponentBuffer.Count; t++)
                    {
                        buffer.Add(getComponentBuffer[t] as T);
                    }
                    GetAllComponentsInHierarchyRecursive(ctr);
                }
            }

            buffer.Clear();

            go.GetComponents(typeof(T), getComponentBuffer);
            for (int t = 0; t < getComponentBuffer.Count; t++)
            {
                buffer.Add(getComponentBuffer[t] as T);
            }

            GetAllComponentsInHierarchyRecursive(go.transform);
        }

        public static void GetAllGameObjectsInHierarchy(this GameObject root, List<GameObject> buffer)
        {
            void GetAllGameObjectsInHierarchyRecursive(Transform tr)
            {
                int count = tr.childCount;
                for (int i = 0; i < count; i++)
                {
                    var ctr = tr.GetChild(i);
                    buffer.Add(ctr.gameObject);
                    GetAllGameObjectsInHierarchyRecursive(ctr);
                }
            }

            buffer.Clear();
            buffer.Add(root);
            GetAllGameObjectsInHierarchyRecursive(root.transform);
        }

        public static bool CompareResolution(this Resolution lhs, Resolution rhs)
        {
            return lhs.width == rhs.width && lhs.height == rhs.height && lhs.refreshRate == rhs.refreshRate;
        }

#if UNITY_EDITOR
        public static T FindScriptableEditor<T>() where T : ScriptableObject
        {
            Type t = typeof(T);
            var assets = AssetDatabase.FindAssets($"t:{t.Name}");

            if (assets.Length > 0)
                return AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(assets[0]));

            return null;
        }
#endif

        public static void SetVisible(this CanvasGroup cg, bool visible)
        {
            cg.alpha = visible ? 1F : 0F;
            cg.interactable = visible ? true : false;
            cg.blocksRaycasts = visible ? true : false;
        }

        public static string AppendPath(this string path, string subpath)
        {
            return path + '/' + subpath;
        }

        public static string ToUnityPath(this string path)
        {
            return path.Replace("\\", "/").Replace("//", "/");
        }

        public static void DestroyGameObjectsAndClear<T>(this List<T> list) where T : MonoBehaviour
        {
            for (int i = 0; i < list.Count; i++)
            {
                GameObject.Destroy(list[i].gameObject);
            }

            list.Clear();
        }

        public static void DestroyGameObjectsAndClear(this List<GameObject> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                GameObject.Destroy(list[i]);
            }

            list.Clear();
        }

        public static List<T> GetComponentsInDirectChildren<T>(this Transform go, bool includeInactive = true) where T : Component
        {
            List<T> list = new List<T>();

            foreach (Transform transform in go)
            {
                if (transform.gameObject.activeInHierarchy || includeInactive)
                {
                    var comp = transform.GetComponent<T>();
                    if (comp)
                        list.Add(comp);
                }
            }

            return list;
        }

        public static void Destroy(UnityEngine.Object target)
        {
            if (Application.isPlaying)
                GameObject.Destroy(target);
            else
                GameObject.DestroyImmediate(target);
        }

        public static void DestroyIfExists(GameObject go)
        {
            if (go)
                GameObject.Destroy(go);
        }

        public static void DestroyIfExists(Component comp)
        {
            if (comp)
                GameObject.Destroy(comp.gameObject);
        }

        public static T Instantiate<T>(this T obj, Transform tr = null, bool inWorldSpace = false) where T : UnityEngine.Object
        {
            return GameObject.Instantiate(obj, tr, inWorldSpace);
        }

        public static Transform Attach<T>(this T obj, Transform parent, bool worldPositionStays = true, bool zeroOut = false) where T : UnityEngine.Component
        {
            var tr = obj.transform;
            tr.SetParent(parent, worldPositionStays);
            if (zeroOut)
            {
                tr.localPosition = Vector3.zero;
                tr.localRotation = Quaternion.identity;
            }
            return tr;
        }

        public static GameObject Attach(this GameObject obj, Transform parent, bool worldPositionStays = true, bool zeroOut = false)
        {
            var tr = obj.transform;
            tr.SetParent(parent.transform, worldPositionStays);
            if (zeroOut)
            {
                tr.localPosition = Vector3.zero;
                tr.localRotation = Quaternion.identity;
            }
            return obj;
        }

        public static void SetActive(this Component comp, bool active)
        {
            var go = comp.gameObject;
            if (go.activeSelf == active)
                return;

            go.SetActive(active);
        }

        public static void SetActive(this IList<Component> list, bool active)
        {
            foreach (var item in list)
                if (item) item.gameObject.SetActive(active);
        }

        public static void SetActive(this IList<GameObject> list, bool active)
        {
            foreach (var item in list)
                if (item) item.SetActive(active);
        }

        public static RectTransform Rect(this Component comp)
        {
            return comp.transform.AsRect();
        }

        public static Transform DestroyAllChildren(this Component comp, bool onlyActive = false)
        {
            var tr = comp.transform;
            for (int i = tr.childCount - 1; i >= 0; i--)
            {
                var c = tr.GetChild(i).gameObject;
                if (onlyActive)
                {
                    if (c.activeSelf)
                        GameObject.Destroy(c);
                }
                else
                    GameObject.Destroy(c);
            }

            return tr;
        }

        public static Transform DestroyAllChildren(this Transform tr, bool onlyActive = false)
        {
            for (int i = tr.childCount - 1; i >= 0; i--)
            {
                var c = tr.GetChild(i).gameObject;
                if (onlyActive)
                {
                    if (c.activeSelf)
                        GameObject.Destroy(c);
                }
                else
                    GameObject.Destroy(c);
            }

            return tr;
        }

        public static Transform DestroyAllChildrenImmidiate(this Transform tr, bool onlyActive = false)
        {
            for (int i = tr.childCount - 1; i >= 0; i--)
            {
                var c = tr.GetChild(i).gameObject;
                if (onlyActive)
                {
                    if (c.activeSelf)
                        GameObject.DestroyImmediate(c);
                }
                else
                    GameObject.DestroyImmediate(c);
            }

            return tr;
        }

        public static Transform DestroyAllChildren<T>(this Transform tr) where T : Component
        {
            for (int i = tr.childCount - 1; i >= 0; i--)
            {
                var c = tr.GetChild(i).gameObject;
                if (c.GetComponent<T>())
                    GameObject.Destroy(c);
            }

            return tr;
        }

        public static Transform ZeroOut(this Transform transform)
        {
            transform.localPosition = new Vector3();
            transform.localRotation = Quaternion.identity;
            return transform;
        }

        public static GameObject ZeroOut(this GameObject go)
        {
            go.transform.localPosition = new Vector3();
            go.transform.localRotation = Quaternion.identity;
            return go;
        }

        public static void CopyFrom(this Transform tr, Transform other)
        {
            RectTransform rtr = tr as RectTransform;
            RectTransform rother = other as RectTransform;

            if (rtr && rother)
            {
                rtr.anchoredPosition = rother.anchoredPosition;
                rtr.anchoredPosition3D = rother.anchoredPosition3D;
                rtr.anchorMax = rother.anchorMax;
                rtr.anchorMin = rother.anchorMin;
                rtr.offsetMax = rother.offsetMax;
                rtr.offsetMin = rother.offsetMin;
                rtr.pivot = rother.pivot;
                rtr.sizeDelta = rother.sizeDelta;
            }

            tr.position = other.position;
            tr.rotation = other.rotation;
            tr.localScale = other.localScale;

        }
    }
}