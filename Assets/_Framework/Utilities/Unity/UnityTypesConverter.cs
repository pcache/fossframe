﻿namespace Framework
{
    using System;
    using System.Text;
    using UnityEngine;

    public static class UnityTypesConverter
    {
        private const char TOKEN_VECTOR_OPEN = '(';
        private const char TOKEN_VECTOR_CLOSE = ')';
        private const char TOKEN_VECTOR_SPLIT = ',';

        private static StringBuilder sb = new StringBuilder();

        private static int[] intbuffer = new int[4];
        private static float[] floatbuffer = new float[4];
        private static byte[] bytebuffer = new byte[4];


        public static bool IsUnityType(Type type)
        {
            return type == typeof(Vector2) ||
                   type == typeof(Vector2Int) ||
                   type == typeof(Vector3) ||
                   type == typeof(Vector3Int) ||
                   type == typeof(Vector4) ||
                   type == typeof(Color) ||
                   type == typeof(Color32);
        }

        public static object ToUnityType(Type type, string str) 
        {
            object obj = Activator.CreateInstance(type);

            switch (obj)
            {
                case Vector2 a:
                    return ToVector2(str);
                case Vector2Int a:
                    return ToVector2Int(str);
                case Vector3 a:
                    return ToVector3(str);
                case Vector3Int a:
                    return ToVector3Int(str);
                case Vector4 a:
                    return ToVector4(str);
                case Color a:
                    return ToColor(str);
                case Color32 a:
                    return ToColor32(str);
            }

            return null;
        }

        public static Vector2 ToVector2(string str)
        {
            var success = FillBuffer(BufferType.Float, str);
            if (success)
            {
                return new Vector2(floatbuffer[0], floatbuffer[1]);
            }
            else
            {
                return new Vector2();
            }
        }

        public static Vector2Int ToVector2Int(string str)
        {
            var success = FillBuffer(BufferType.Int, str);
            if (success)
            {
                return new Vector2Int(intbuffer[0], intbuffer[1]);
            }
            else
            {
                return new Vector2Int();
            }
        }

        public static Vector3 ToVector3(string str)
        {
            var success = FillBuffer(BufferType.Float, str);
            if (success)
            {
                return new Vector3(floatbuffer[0], floatbuffer[1], floatbuffer[2]);
            }
            else
            {
                return new Vector3();
            }
        }

        public static Vector3Int ToVector3Int(string str)
        {
            var success = FillBuffer(BufferType.Int, str);
            if (success)
            {
                return new Vector3Int(intbuffer[0], intbuffer[1], intbuffer[2]);
            }
            else
            {
                return new Vector3Int();
            }
        }

        public static Vector4 ToVector4(string str)
        {
            var success = FillBuffer(BufferType.Float, str);
            if (success)
            {
                return new Vector4(floatbuffer[0], floatbuffer[1], floatbuffer[2], floatbuffer[3]);
            }
            else
            {
                return new Vector4();
            }
        }

        public static Color ToColor(string str)
        {
            var success = FillBuffer(BufferType.Float, str, prefillBuffer: true, prefillValue: 1f);
            if (success)
            {
                return new Color(floatbuffer[0], floatbuffer[1], floatbuffer[2], floatbuffer[3]);
            }
            else
            {
                return new Color();
            }
        }

        public static Color32 ToColor32(string str)
        {
            var success = FillBuffer(BufferType.Byte, str, prefillBuffer: true, prefillValue: 255);
            if (success)
            {
                return new Color32(bytebuffer[0], bytebuffer[1], bytebuffer[2], bytebuffer[3]);
            }
            else
            {
                return new Color32();
            }
        }

        private enum BufferType
        {
            Int,
            Float,
            Byte
        }

        private static bool FillBuffer(BufferType bufferType, string str, bool prefillBuffer = false, float prefillValue = 0)
        {
            sb.Clear();

            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            int len = str.Length;

            if (len < 2)
            {
                return false;
            }

            if (str[0] != TOKEN_VECTOR_OPEN || str[len - 1] != TOKEN_VECTOR_CLOSE)
            {
                return false;
            }

            if (bufferType == BufferType.Int)
            {
                for (int i = 0; i < intbuffer.Length; i++)
                {
                    intbuffer[i] = (int)prefillValue;
                }
            }
            else if (bufferType == BufferType.Float)
            {
                for (int i = 0; i < floatbuffer.Length; i++)
                {
                    floatbuffer[i] = prefillValue;
                }
            }
            else if (bufferType == BufferType.Byte)
            {
                for (int i = 0; i < bytebuffer.Length; i++)
                {
                    bytebuffer[i] = (byte)prefillValue;
                }
            }

            byte valueCount = 0;
            for (int i = 0; i < len; i++)
            {
                if (valueCount > 3)
                {
                    continue;
                }

                char current = str[i];
                if (current == TOKEN_VECTOR_OPEN)
                {
                    continue;
                }

                if (current == TOKEN_VECTOR_SPLIT || current == TOKEN_VECTOR_CLOSE)
                {
                    if (bufferType == BufferType.Int)
                    {
                        intbuffer[valueCount++] = Convert.ToInt32(sb.ToString());
                    }
                    else if (bufferType == BufferType.Float)
                    {
                        floatbuffer[valueCount++] = Convert.ToSingle(sb.ToString());
                    }
                    else if (bufferType == BufferType.Byte)
                    {
                        bytebuffer[valueCount++] = Convert.ToByte(sb.ToString());
                    }
                    sb.Clear();
                    continue;
                }
                sb.Append(current);
            }

            return true;
        }

        public static string GetVectorString(float x, float y)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(float x, float y, float z)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(z);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(float x, float y, float z, float w)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(z);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(w);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(int x, int y)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(int x, int y, int z)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(z);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(int x, int y, int z, int w)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(z);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(w);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }

        public static string GetVectorString(byte x, byte y, byte z, byte w)
        {
            sb.Clear();

            sb.Append(TOKEN_VECTOR_OPEN);
            sb.Append(x);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(y);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(z);
            sb.Append(TOKEN_VECTOR_SPLIT);
            sb.Append(w);
            sb.Append(TOKEN_VECTOR_CLOSE);

            return sb.ToString();
        }
    }
}