This is a lightweight framework implementing common gamedev patterns.
- Systems
- Defs/ Data objects with a Database accessible at runtime.
- Runtime console
- Project version/changelog window.
- Build script
- Large amount of utilities

Hard dependencies:
	OdinSerializer
	MEC
	ZString
	IonicZip
	EditorCoroutines


Soft dependencies:
	Animancer
	ConsoleEnhanced
	DOTween
	Graphy
	
	