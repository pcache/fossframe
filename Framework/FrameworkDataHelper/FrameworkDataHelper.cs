﻿namespace Framework.Editor
{

    using System;
    using UnityEngine;
    using System.Reflection;
    using System.Linq;
    using System.IO;
    using Framework;
    using Framework.Data;
    using UnityEditor;

    internal class FrameworkDataHelper : EditorWindow
    {
        private Type[] Defs;
        private Type[] Data;
        private FrameworkDataHelperColors colors;

        [MenuItem("Framework/DataHelper")]
        static void Init()
        {
            if (EditorWindow.HasOpenInstances<FrameworkDataHelper>())
                return;

            // Get existing open window or if none, make a new one:
            FrameworkDataHelper window = (FrameworkDataHelper)EditorWindow.GetWindow(typeof(FrameworkDataHelper));
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 420, 720);
            window.Show();

            FrameworkDataHelperColors color = ScriptableObject.CreateInstance<FrameworkDataHelperColors>();

            Type t = typeof(FrameworkDataHelperColors);
            var assets = AssetDatabase.FindAssets($"t:{t.Name}");

            if (assets.Length > 0)
                window.colors = AssetDatabase.LoadAssetAtPath<FrameworkDataHelperColors>(AssetDatabase.GUIDToAssetPath(assets[0]));

            window.colors.Init();
        }

        private void OnEnable()
        {
            FrameworkDataHelper window = (FrameworkDataHelper)EditorWindow.GetWindow(typeof(FrameworkDataHelper));
            window.Defs = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user, typeof(Def), true).ToArray();
            window.Data = ReflectedTypeStore.GetUnityTypes(ReflectedTypeStore.UnityAssembly.user, typeof(Data), true).OrderBy(x => x.Name).ToArray();

            window.Defs = window.Defs.OrderBy(x =>
            {
                var att = x.GetCustomAttribute(typeof(DefInfoAttribute)) as DefInfoAttribute;
                if (att != null)
                    return att.Group;
                return default;
            }).ThenBy(x => x.Name).ToArray();

            window.Data = window.Data.OrderBy(x =>
            {
                var att = x.GetCustomAttribute(typeof(DataInfoAttribute)) as DataInfoAttribute;
                if (att != null)
                    return att.Group;
                return default;
            }).ThenBy(x => x.Name).ToArray();
        }

        private void Update()
        {
            Repaint();
        }

        void OnGUI()
        {
            var e = Event.current;
            var selection = Selection.activeObject;

            if (!selection)
            {
                GUI.color = Color.red;
                GUILayout.Label("Select an asset, and its folder will be used to create new assets.");
                GUI.color = Color.white;
                return;
            }

            string path = AssetDatabase.GetAssetPath(selection);
            if (AssetDatabase.IsValidFolder(path))
                path = path.ToUnityPath();
            else
                path = Directory.GetParent(path).ToString().ToUnityPath();

            GUILayout.Label(path);

            if (e.keyCode == KeyCode.Escape)
            {
                this.Close();
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical();

            GUILayout.Label("-------DEFS-------");

            DataInfoGroup dataGroup = (DataInfoGroup)int.MaxValue;
            DefInfoGroup defGroup = (DefInfoGroup)int.MaxValue;

            foreach (var item in Defs)
            {
                var attr = item.GetCustomAttribute(typeof(DefInfoAttribute)) as DefInfoAttribute;
                if (attr != null && defGroup != attr.Group)
                {
                    GUILayout.Label(attr.Group.ToString());
                    defGroup = attr.Group;
                }

                if (colors.DefInfo.TryGetValue(defGroup, out var info))
                    GUI.color = info.color;
                else
                    GUI.color = Color.white;


                if (GUILayout.Button(item.Name, GUILayout.Width(200f)))
                {
                    CreateSO(item, path, item.Name, "Def");
                    Close();
                    return;
                }

                GUI.color = Color.white;
            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();

            GUILayout.Label("-------DATA-------");

            foreach (var item in Data)
            {
                var attr = item.GetCustomAttribute(typeof(DataInfoAttribute)) as DataInfoAttribute;
                if (attr != null && dataGroup != attr.Group)
                {
                    GUILayout.Label(attr.Group.ToString());
                    dataGroup = attr.Group;
                }

                if (colors.DataInfo.TryGetValue(dataGroup, out var info))
                    GUI.color = info.color;
                else
                    GUI.color = Color.white;

                if (GUILayout.Button(item.Name, GUILayout.Width(200f)))
                {
                    CreateSO(item, path, item.Name, "Data");
                    Close();
                    return;
                }

                GUI.color = Color.white;

            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
        }

        private void CreateSO(Type type, string path, string name, string prefix)
        {
            var so = ScriptableObject.CreateInstance(type);
            ProjectWindowUtil.CreateAsset(so, path + "/" + name + ".asset");
            ProjectWindowUtil.ShowCreatedAsset(so);
            //AssetDatabase.CreateAsset(so, );
            //Selection.activeObject = so;
        }
    }
}
