﻿namespace Framework.Editor
{
    using Framework;
    using Framework.Data;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [DataInfo(DataInfoGroup.Editor)]
    public class FrameworkDataHelperColors : Framework.Data.Data
    {
        [Serializable]
        public class DefGroupData
        {
            public DefInfoGroup group;
            public Color color = Color.white;
        }

        [SerializeField]
        private List<DefGroupData> DefData = new List<DefGroupData>();

        [Serializable]
        public class DataGroupData
        {
            public DataInfoGroup group;
            public Color color = Color.white;
        }

        [SerializeField]
        private List<DataGroupData> DataData = new List<DataGroupData>();

        private Dictionary<DataInfoGroup, DataGroupData> dataMap = new Dictionary<DataInfoGroup, DataGroupData>();
        private Dictionary<DefInfoGroup, DefGroupData> defMap = new Dictionary<DefInfoGroup, DefGroupData>();

        public ReadOnlyDictionary<DataInfoGroup, DataGroupData> DataInfo { get; private set; }
        public ReadOnlyDictionary<DefInfoGroup, DefGroupData> DefInfo { get; private set; }

        public void Init()
        {
            defMap.Clear();
            dataMap.Clear();

            foreach (var item in DefData)
            {
                defMap.Add(item.group, item);
            }

            foreach (var item in DataData)
            {
                dataMap.Add(item.group, item);
            }

            DataInfo = new ReadOnlyDictionary<DataInfoGroup, DataGroupData>(dataMap);
            DefInfo = new ReadOnlyDictionary<DefInfoGroup, DefGroupData>(defMap);
        }
    }
}
