﻿using Framework.Data;
using Framework.Internal;
using System;
using System.Collections.Generic;
using UnityEditor;

namespace Framework.Editor
{
    //[InitializeOnLoad]
    //public class FrameworkScriptablePostLaunchInitializer
    //{
    //    static FrameworkScriptablePostLaunchInitializer()
    //    {
    //        FrameworkDataValidator.InitializeOnLoad();
    //        EditorUtility.RequestScriptReload();
    //    }
    //}

    public class FrameworkDataValidator : AssetPostprocessor
    {
        [InitializeOnLoadMethod]
        public static void InitializeOnLoad()
        {
            
        }

        [MenuItem("Framework/ValidateData")]
        public static void Validate()
        {

        }
    }
}
