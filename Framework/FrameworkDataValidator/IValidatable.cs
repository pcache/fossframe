﻿using Framework;
using Framework.Internal;
using System;

namespace Framework
{
    public interface IValidatable
    {
        void ValidateData(ValidationResults results);
    }
}
