﻿using Framework;
using System;
using System.Collections.Generic;

namespace Framework.Internal
{
    public class ValidationResults
    {
        public class ValidationResult
        {
            public IValidatable Target { get; set; }
            public string Message { get; set; }
        }

        public IValidatable Current { get; set; }
        private List<ValidationResult> results = new List<ValidationResult>();

        public ValidationResults Assert(bool assertion, string message = null)
        {
            if(!assertion)
            {
                var result = new ValidationResult();

                result.Target = Current;
                result.Message = message;
                results.Add(result);
            }

            return this;
        }
    }
}
