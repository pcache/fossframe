﻿using Framework.Data;
using Framework.Internal;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Framework.Editor
{
    [InitializeOnLoad]
    public class FrameworkScriptablePostLaunchInitializer
    {
        static FrameworkScriptablePostLaunchInitializer()
        {
            FrameworkScriptablePostProcessor.InitializeOnLoad();
            EditorUtility.RequestScriptReload();
        }
    }

    public class FrameworkScriptablePostProcessor : AssetPostprocessor
    {
        [InitializeOnLoadMethod]
        public static void InitializeOnLoad()
        {
            string path = "Assets/_Data/Resources/DefDB.asset";
            var db = UnityEngine.Resources.Load("DefDB", typeof(DefDB)) as DefDB;

            if (db == null)
            {
                Debug.Log($"No DEF_DB found at {path} , creating new one.");

                // recreate def db.
                db = ScriptableObject.CreateInstance<DefDB>();

                if(!AssetDatabase.IsValidFolder("Assets/_Data"))
                    AssetDatabase.CreateFolder("Assets", "_Data");
                if(!AssetDatabase.IsValidFolder("Assets/_Data/Resources"))
                    AssetDatabase.CreateFolder("Assets/_Data", "Resources");

                AssetDatabase.CreateAsset(db, path);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            EditorCollectScriptables(db);
        }

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            var db = UnityEngine.Resources.Load("DefDB", typeof(DefDB)) as DefDB;
            IDefDBInternal idb = db;

            for (var index = 0; index < importedAssets.Length; index++)
            {
                var importedAsset = importedAssets[index];
                var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(importedAsset);
                FrameworkScriptable cs = asset as FrameworkScriptable;

                if (cs)
                {
                    if (!idb.Contains(cs) && !IsUniqueId(cs.GUID, db) || cs.GUID == Guid.Empty)
                    {
                        ((IFrameworkScriptableEditorInternal)cs).GenerateNewGUID();
                        EditorCollectScriptables(db);
                    }
                }
            }

            for (var index = 0; index < deletedAssets.Length; index++)
            {
                idb.CleanupNulls();
            }
        }

        public static void OnBuildCollectScriptables()
        {
            var db = UnityEngine.Resources.Load("DefDB", typeof(DefDB)) as DefDB;
            EditorCollectScriptables(db);
        }

        public static void EditorCollectScriptables(DefDB db)
        {
            IDefDBInternal idb = db;
            List<FrameworkScriptable> all = new List<FrameworkScriptable>();
            var guids = AssetDatabase.FindAssets("t:FrameworkScriptable");

            foreach (var item in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(item);
                var def = AssetDatabase.LoadAssetAtPath(path, typeof(FrameworkScriptable)) as FrameworkScriptable;

                all.Add(def);
            }

            idb.SetScriptables(all);
            EditorMapIds(db);
            EditorUtility.SetDirty(db);
        }

        private static void EditorMapIds(DefDB db)
        {
            IDefDBInternal idb = db;
            Dictionary<Guid, FrameworkScriptable> map = new Dictionary<Guid, FrameworkScriptable>();

            foreach (var s in db.Scriptables)
            {
                if (map.ContainsKey(s.GUID))
                {
                    continue;
                }
                map.Add(s.GUID, s);
            }

            idb.SetGuidScriptableMap(map);
        }

        private static bool IsUniqueId(Guid guid, DefDB db)
        {
            return !db.GuidScriptableMap.ContainsKey(guid);
        }
    }
}
